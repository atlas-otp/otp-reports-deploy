#!/bin/sh
set -e

rm *.js
rm *.css
rm *.map
wget -nv -i assets.lst
