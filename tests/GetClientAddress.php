<?php
	header("Content-Type: text/plain");

    if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
    } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        $ip = $_SERVER["HTTP_CLIENT_IP"];
    } else {
    	$ip =  $_SERVER["REMOTE_ADDR"];
    }

    echo @gethostbyaddr($ip)."\n";
