<!DOCTYPE html>

<!-- warning.html -->

<!--------------------------------------------------------------------------->
<!--                                                                       -->
<!-- WARNING, this file is generated, please edit the source and reconvert -->
<!--                                                                       -->
<!--                                                                       -->
<!--                                                                       -->
<!--------------------------------------------------------------------------->



<?php
error_reporting(E_ALL);
ini_set("display_errors", '1');
?>


<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>


<html lang="en">
    <head>
        <!-- header.html -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="OTP, Operation Task Planner">
<meta name="keywords" content="CERN, ATLAS, OTP">
<meta name="author" content="Mark Donszelmann">
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<title>
  ATLAS OTP &middot; OTP ATLAS
</title>

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../ico/atlas-144x144.png">
                               <link rel="shortcut icon" href="../ico/atlas.ico">

<!-- jQuery -->
<!-- NOTE: no Ajax in 'slim' version of jquery -->
<!-- NOTE: jquery 3.x did not work -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- jQuery TimeAgo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>

<!-- jQuery Resizable -->
<!--
<script src="../jquery-resizable-0.28/dist/jquery-resizable.min.js"></script>
-->

<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/cerulean/bootstrap.min.css">
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >

<!-- Bootstrap Select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

<!-- Bootstrap DateRangePicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>

<!-- Datatables, jquery then bootstrap, but NOTE removal below -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css" >
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
<!-- Removing container-fluid on class of dataTables_wrapper as it interferes with scrollResize -->
var DataTable = $.fn.dataTable;
$.extend( DataTable.ext.classes, {
    sWrapper:      "dataTables_wrapper dt-bootstrap4",
} );
</script>

<!-- Datatables Extensions -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.0.3/css/rowGroup.dataTables.min.css">
<script src="https://cdn.datatables.net/rowgroup/1.0.3/js/dataTables.rowGroup.min.js"></script>

<!-- Does not work with scrollResize
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/1.5.0/css/scroller.dataTables.min.css">
<script src="https://cdn.datatables.net/scroller/1.5.0/js/dataTables.scroller.min.js"></script>
-->

<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.dataTables.min.css">
<script src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>

<!-- Datatables Plugins -->
<script src="https://cdn.datatables.net/plug-ins/1.10.19/features/scrollResize/dataTables.scrollResize.min.js"></script>

<!-- Highcharts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/modules/exporting.js"></script>

<!-- Search JS -->
<!--
Below are possibly these ?
<script src="https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.0.1/lunr.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="http://stevenlevithan.com/assets/misc/date.format.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.10/URI.min.js" type="text/javascript"></script>
-->
<!--
<script src="../js/search.min.js" type="text/javascript"></script>
-->
<script src="../js/lunr.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/mustache.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/date.format.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/URI.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/jquery.lunr.search.js" type="text/javascript" charset="utf-8"></script>

<!-- ATLAS and OTP Stylesheets and JavaScript-->
<link rel="stylesheet" href="../css/atlas.css" >
<link rel="stylesheet" href="../css/otp.css" >
<script src="../js/otp.js" type="text/javascript"></script>




        <link rel="stylesheet" href="../css/otp-table.css" > <script type="text/javascript" class="init" src="../js/otp.js?random=<?php echo uniqid(); ?>"></script>

        <link rel="stylesheet" href="../css/otp-table.css" > <script type="text/javascript" class="init" src="../js/otp.js?random=<?php echo uniqid(); ?>"></script>

    </head>

    <body>
        
<div class="container-fluid">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="../">ATLAS OTP Reports</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Reports
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../reports/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/TaskScheduleP1.php">Task Schedule Point1</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/FundingAgency.php">Funding Agency</a>
            <a class="dropdown-item" href="../reports/FundingAgency.php?byInstitution">Funding Agency by Institution</a>
            <a class="dropdown-item" href="../reports/Institution.php">Institution</a>
            <a class="dropdown-item" href="../reports/InstitutionYear.php">Institution by Year</a>
            <a class="dropdown-item" href="../reports/Task.php">Task</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/MultiYear.php">Multi-Year</a>
            <a class="dropdown-item" href="../reports/MultiYear.php?byInstitution">Multi-Year by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/Commitment.php">Commitment</a>
            <a class="dropdown-item" href="../reports/CommitmentFunding.php">Commitment Funding</a>
            <a class="dropdown-item" href="../reports/CommitmentFunding.php?byInstitution">Commitment Funding by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/PersonalSchedule.php">Personal Schedule</a>
            <a class="dropdown-item" href="../reports/TaskSchedule.php">Task Schedule</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/Occupancy.php">Occupancy</a>
            <a class="dropdown-item" href="../reports/Personal.php">Personal</a>
            <a class="dropdown-item" href="../reports/ShiftsByHour.php">Shifts by Hour (SLIMOS)</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/TaskPersonnel.php">Task Personnel</a>
            <a class="dropdown-item" href="../reports/Allocation.php">Allocation</a>
            <a class="dropdown-item" href="../reports/Requirement.php">Requirement</a>
            <a class="dropdown-item" href="../reports/History.php">History</a>

          </div>
        </li>
<!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Plots
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../plots/index.php">Description*</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../plots/FTEbyActivity.php">FTEbyActivity</a>
            <a class="dropdown-item" href="../plots/FTEbySystem.php">FTEbySystem</a>
          </div>
        </li>
-->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Checks
          </a>
          <div class="dropdown-menu">
          <!-- placement exactly under menu
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
          -->
            <a class="dropdown-item" href="../checks/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../checks/CategoryUsage.php">Category Usage</a>
            <a class="dropdown-item" href="../checks/ConflictingShifts.php">Conflicting Shifts</a>
            <a class="dropdown-item" href="../checks/DoubleDesks.php">Double Desks</a>
            <a class="dropdown-item" href="../checks/OvervaluedShifts.php">Overvalued Shifts</a>
            <a class="dropdown-item" href="../checks/OnShift.php">On Shift</a>
          </div>
        </li>

<!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Tests
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../tests/GetClientAddress.php">Get Client Address</a>
            <a class="dropdown-item" href="../tests/PublicPhoneListRedirect.html">Public Phonelist Redirect</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../tests/BootstrapSelectTest.html">Bootstrap Select Test</a>
            <a class="dropdown-item" href="../tests/OtpBootstrapSelectTest.php">OTP Bootstrap Select Test</a>
            <a class="dropdown-item" href="../tests/ScrollTest.html">Scroll Test</a>
            <a class="dropdown-item" href="../tests/OtpScrollTest.php">OTP Scroll Test</a>

            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header btn-danger">Not Working</h6>
            <a class="dropdown-item" href="../tests/TrainingIndico.php">Training Indico</a>
            <a class="dropdown-item" href="../tests/UnicodeTest.php">Unicode Test</a>
          </div>
        </li>
-->
        <li class="nav-item">
          <a class="nav-link" href="../final_reports/index.php">Final Reports</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="https://otp-atlas.web.cern.ch">Website</a>
        </li>
      <!--
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      -->
      </ul>
    <!--
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    -->
    </div>
  </nav>

  <script type="text/javascript" class="init">
  $(document).ready(function() {

    dateFormat = "DD-MMM-YY";

    parseIntOrDate = function ( intOrDate ) {
      var m = moment( intOrDate, dateFormat, true );
      if (m.isValid()) {
        intOrDate = m.format( 'x' );
      }
      return parseInt(intOrDate);
    }

    $.fn.dataTableExt.oSort['nullable-asc'] = function(a,b) {
        if (a === null)
            return 1;
        else if (b === null)
            return -1;
        else
        {
            var ia = parseIntOrDate(a);
            var ib = parseIntOrDate(b);
            return (ia<ib) ? -1 : ((ia > ib) ? 1 : 0);
        }
    }

    $.fn.dataTableExt.oSort['nullable-desc'] = function(a,b) {
        if (a === null)
          return 1;
        else if (b === null)
          return -1;
        else
        {
            var ia = parseIntOrDate(a);
            var ib = parseIntOrDate(b);
            return (ia>ib) ? -1 : ((ia < ib) ? 1 : 0);
        }
    }
  } );
</script>

<div class="card">
  <!--
<div class="card-header">
  <div class="card-title">
    <span class="selectionName"></span>
  </div>
</div>
-->


  <form class="form-inline">
      <div class="form-group">
        <label class="sr-only" for="year">Year</label>
        <div class="input-group mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text">Year</div>
          </div>
          <select class="selectpicker" id="year">
            <option>2018</option>
            <option>2017</option>
            <option>2016</option>
            <option>2015</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="sr-only" for="category">Category</label>
        <div class="input-group mb-2 mr-sm-2">
          <div class="input-group-prepend">
            <div class="input-group-text">Category</div>
          </div>
          <select multiple="multiple" class="selectpicker" id="category">
            <option>Class 1</option>
            <option>Class 2</option>
            <option>Class 3</option>
            <option>Class 4</option>
          </select>
        </div>
    </div>
  </form>
</div>

<div class="card panel-default">
  <div class="card-header">
    <div class="card-title">
      <span class="reportName"></span>
    </div>
  </div>
</div>


<div class="row content">
	<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Name</th>
				<th>Position</th>
				<th>Office</th>
				<th>Age</th>
				<th>Start date</th>
				<th>Salary</th>
			</tr>
		</thead>

		<tfoot>
			<tr>
				<th>Name</th>
				<th>Position</th>
				<th>Office</th>
				<th>Age</th>
				<th>Start date</th>
				<th>Salary</th>
			</tr>
		</tfoot>

		<tbody>
			<tr>
				<td>Tiger Nixon</td>
				<td>System Architect</td>
				<td>Edinburgh</td>
				<td>61</td>
				<td>2011/04/25</td>
				<td>$320,800</td>
			</tr>
			<tr>
				<td>Garrett Winters</td>
				<td>Accountant</td>
				<td>Tokyo</td>
				<td>63</td>
				<td>2011/07/25</td>
				<td>$170,750</td>
			</tr>
			<tr>
				<td>Ashton Cox</td>
				<td>Junior Technical Author</td>
				<td>San Francisco</td>
				<td>66</td>
				<td>2009/01/12</td>
				<td>$86,000</td>
			</tr>
			<tr>
				<td>Cedric Kelly</td>
				<td>Senior Javascript Developer</td>
				<td>Edinburgh</td>
				<td>22</td>
				<td>2012/03/29</td>
				<td>$433,060</td>
			</tr>
			<tr>
				<td>Airi Satou</td>
				<td>Accountant</td>
				<td>Tokyo</td>
				<td>33</td>
				<td>2008/11/28</td>
				<td>$162,700</td>
			</tr>
			<tr>
				<td>Brielle Williamson</td>
				<td>Integration Specialist</td>
				<td>New York</td>
				<td>61</td>
				<td>2012/12/02</td>
				<td>$372,000</td>
			</tr>
			<tr>
				<td>Herrod Chandler</td>
				<td>Sales Assistant</td>
				<td>San Francisco</td>
				<td>59</td>
				<td>2012/08/06</td>
				<td>$137,500</td>
			</tr>
			<tr>
				<td>Rhona Davidson</td>
				<td>Integration Specialist</td>
				<td>Tokyo</td>
				<td>55</td>
				<td>2010/10/14</td>
				<td>$327,900</td>
			</tr>
			<tr>
				<td>Colleen Hurst</td>
				<td>Javascript Developer</td>
				<td>San Francisco</td>
				<td>39</td>
				<td>2009/09/15</td>
				<td>$205,500</td>
			</tr>
			<tr>
				<td>Sonya Frost</td>
				<td>Software Engineer</td>
				<td>Edinburgh</td>
				<td>23</td>
				<td>2008/12/13</td>
				<td>$103,600</td>
			</tr>
			<tr>
				<td>Jena Gaines</td>
				<td>Office Manager</td>
				<td>London</td>
				<td>30</td>
				<td>2008/12/19</td>
				<td>$90,560</td>
			</tr>
			<tr>
				<td>Quinn Flynn</td>
				<td>Support Lead</td>
				<td>Edinburgh</td>
				<td>22</td>
				<td>2013/03/03</td>
				<td>$342,000</td>
			</tr>
			<tr>
				<td>Charde Marshall</td>
				<td>Regional Director</td>
				<td>San Francisco</td>
				<td>36</td>
				<td>2008/10/16</td>
				<td>$470,600</td>
			</tr>
			<tr>
				<td>Haley Kennedy</td>
				<td>Senior Marketing Designer</td>
				<td>London</td>
				<td>43</td>
				<td>2012/12/18</td>
				<td>$313,500</td>
			</tr>
			<tr>
				<td>Tatyana Fitzpatrick</td>
				<td>Regional Director</td>
				<td>London</td>
				<td>19</td>
				<td>2010/03/17</td>
				<td>$385,750</td>
			</tr>
			<tr>
				<td>Michael Silva</td>
				<td>Marketing Designer</td>
				<td>London</td>
				<td>66</td>
				<td>2012/11/27</td>
				<td>$198,500</td>
			</tr>
			<tr>
				<td>Paul Byrd</td>
				<td>Chief Financial Officer (CFO)</td>
				<td>New York</td>
				<td>64</td>
				<td>2010/06/09</td>
				<td>$725,000</td>
			</tr>
			<tr>
				<td>Gloria Little</td>
				<td>Systems Administrator</td>
				<td>New York</td>
				<td>59</td>
				<td>2009/04/10</td>
				<td>$237,500</td>
			</tr>
			<tr>
				<td>Bradley Greer</td>
				<td>Software Engineer</td>
				<td>London</td>
				<td>41</td>
				<td>2012/10/13</td>
				<td>$132,000</td>
			</tr>
			<tr>
				<td>Dai Rios</td>
				<td>Personnel Lead</td>
				<td>Edinburgh</td>
				<td>35</td>
				<td>2012/09/26</td>
				<td>$217,500</td>
			</tr>
			<tr>
				<td>Jenette Caldwell</td>
				<td>Development Lead</td>
				<td>New York</td>
				<td>30</td>
				<td>2011/09/03</td>
				<td>$345,000</td>
			</tr>
			<tr>
				<td>Yuri Berry</td>
				<td>Chief Marketing Officer (CMO)</td>
				<td>New York</td>
				<td>40</td>
				<td>2009/06/25</td>
				<td>$675,000</td>
			</tr>
			<tr>
				<td>Caesar Vance</td>
				<td>Pre-Sales Support</td>
				<td>New York</td>
				<td>21</td>
				<td>2011/12/12</td>
				<td>$106,450</td>
			</tr>
			<tr>
				<td>Doris Wilder</td>
				<td>Sales Assistant</td>
				<td>Sidney</td>
				<td>23</td>
				<td>2010/09/20</td>
				<td>$85,600</td>
			</tr>
			<tr>
				<td>Angelica Ramos</td>
				<td>Chief Executive Officer (CEO)</td>
				<td>London</td>
				<td>47</td>
				<td>2009/10/09</td>
				<td>$1,200,000</td>
			</tr>
			<tr>
				<td>Gavin Joyce</td>
				<td>Developer</td>
				<td>Edinburgh</td>
				<td>42</td>
				<td>2010/12/22</td>
				<td>$92,575</td>
			</tr>
			<tr>
				<td>Jennifer Chang</td>
				<td>Regional Director</td>
				<td>Singapore</td>
				<td>28</td>
				<td>2010/11/14</td>
				<td>$357,650</td>
			</tr>
			<tr>
				<td>Brenden Wagner</td>
				<td>Software Engineer</td>
				<td>San Francisco</td>
				<td>28</td>
				<td>2011/06/07</td>
				<td>$206,850</td>
			</tr>
			<tr>
				<td>Fiona Green</td>
				<td>Chief Operating Officer (COO)</td>
				<td>San Francisco</td>
				<td>48</td>
				<td>2010/03/11</td>
				<td>$850,000</td>
			</tr>
			<tr>
				<td>Shou Itou</td>
				<td>Regional Marketing</td>
				<td>Tokyo</td>
				<td>20</td>
				<td>2011/08/14</td>
				<td>$163,000</td>
			</tr>
			<tr>
				<td>Michelle House</td>
				<td>Integration Specialist</td>
				<td>Sidney</td>
				<td>37</td>
				<td>2011/06/02</td>
				<td>$95,400</td>
			</tr>
			<tr>
				<td>Suki Burks</td>
				<td>Developer</td>
				<td>London</td>
				<td>53</td>
				<td>2009/10/22</td>
				<td>$114,500</td>
			</tr>
			<tr>
				<td>Prescott Bartlett</td>
				<td>Technical Author</td>
				<td>London</td>
				<td>27</td>
				<td>2011/05/07</td>
				<td>$145,000</td>
			</tr>
			<tr>
				<td>Gavin Cortez</td>
				<td>Team Leader</td>
				<td>San Francisco</td>
				<td>22</td>
				<td>2008/10/26</td>
				<td>$235,500</td>
			</tr>
			<tr>
				<td>Martena Mccray</td>
				<td>Post-Sales support</td>
				<td>Edinburgh</td>
				<td>46</td>
				<td>2011/03/09</td>
				<td>$324,050</td>
			</tr>
			<tr>
				<td>Unity Butler</td>
				<td>Marketing Designer</td>
				<td>San Francisco</td>
				<td>47</td>
				<td>2009/12/09</td>
				<td>$85,675</td>
			</tr>
			<tr>
				<td>Howard Hatfield</td>
				<td>Office Manager</td>
				<td>San Francisco</td>
				<td>51</td>
				<td>2008/12/16</td>
				<td>$164,500</td>
			</tr>
			<tr>
				<td>Hope Fuentes</td>
				<td>Secretary</td>
				<td>San Francisco</td>
				<td>41</td>
				<td>2010/02/12</td>
				<td>$109,850</td>
			</tr>
			<tr>
				<td>Vivian Harrell</td>
				<td>Financial Controller</td>
				<td>San Francisco</td>
				<td>62</td>
				<td>2009/02/14</td>
				<td>$452,500</td>
			</tr>
			<tr>
				<td>Timothy Mooney</td>
				<td>Office Manager</td>
				<td>London</td>
				<td>37</td>
				<td>2008/12/11</td>
				<td>$136,200</td>
			</tr>
			<tr>
				<td>Jackson Bradshaw</td>
				<td>Director</td>
				<td>New York</td>
				<td>65</td>
				<td>2008/09/26</td>
				<td>$645,750</td>
			</tr>
			<tr>
				<td>Olivia Liang</td>
				<td>Support Engineer</td>
				<td>Singapore</td>
				<td>64</td>
				<td>2011/02/03</td>
				<td>$234,500</td>
			</tr>
			<tr>
				<td>Bruno Nash</td>
				<td>Software Engineer</td>
				<td>London</td>
				<td>38</td>
				<td>2011/05/03</td>
				<td>$163,500</td>
			</tr>
			<tr>
				<td>Sakura Yamamoto</td>
				<td>Support Engineer</td>
				<td>Tokyo</td>
				<td>37</td>
				<td>2009/08/19</td>
				<td>$139,575</td>
			</tr>
			<tr>
				<td>Thor Walton</td>
				<td>Developer</td>
				<td>New York</td>
				<td>61</td>
				<td>2013/08/11</td>
				<td>$98,540</td>
			</tr>
			<tr>
				<td>Finn Camacho</td>
				<td>Support Engineer</td>
				<td>San Francisco</td>
				<td>47</td>
				<td>2009/07/07</td>
				<td>$87,500</td>
			</tr>
			<tr>
				<td>Serge Baldwin</td>
				<td>Data Coordinator</td>
				<td>Singapore</td>
				<td>64</td>
				<td>2012/04/09</td>
				<td>$138,575</td>
			</tr>
			<tr>
				<td>Zenaida Frank</td>
				<td>Software Engineer</td>
				<td>New York</td>
				<td>63</td>
				<td>2010/01/04</td>
				<td>$125,250</td>
			</tr>
			<tr>
				<td>Zorita Serrano</td>
				<td>Software Engineer</td>
				<td>San Francisco</td>
				<td>56</td>
				<td>2012/06/01</td>
				<td>$115,000</td>
			</tr>
			<tr>
				<td>Jennifer Acosta</td>
				<td>Junior Javascript Developer</td>
				<td>Edinburgh</td>
				<td>43</td>
				<td>2013/02/01</td>
				<td>$75,650</td>
			</tr>
			<tr>
				<td>Cara Stevens</td>
				<td>Sales Assistant</td>
				<td>New York</td>
				<td>46</td>
				<td>2011/12/06</td>
				<td>$145,600</td>
			</tr>
			<tr>
				<td>Hermione Butler</td>
				<td>Regional Director</td>
				<td>London</td>
				<td>47</td>
				<td>2011/03/21</td>
				<td>$356,250</td>
			</tr>
			<tr>
				<td>Lael Greer</td>
				<td>Systems Administrator</td>
				<td>London</td>
				<td>21</td>
				<td>2009/02/27</td>
				<td>$103,500</td>
			</tr>
			<tr>
				<td>Jonas Alexander</td>
				<td>Developer</td>
				<td>San Francisco</td>
				<td>30</td>
				<td>2010/07/14</td>
				<td>$86,500</td>
			</tr>
			<tr>
				<td>Shad Decker</td>
				<td>Regional Director</td>
				<td>Edinburgh</td>
				<td>51</td>
				<td>2008/11/13</td>
				<td>$183,000</td>
			</tr>
			<tr>
				<td>Michael Bruce</td>
				<td>Javascript Developer</td>
				<td>Singapore</td>
				<td>29</td>
				<td>2011/06/27</td>
				<td>$183,000</td>
			</tr>
			<tr>
				<td>Donna Snider</td>
				<td>Customer Support</td>
				<td>New York</td>
				<td>27</td>
				<td>2011/01/25</td>
				<td>$112,000</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="card">
  <div class="card-header">
  <div class="card-title">
    <span class="notesName">Notes</span>
  </div>
</div>


  <ul class="notes">
    <li>Before January 2023 Class 1 shifts are valued 1.31 for weekend and night shifts, 0.66 at other times.</li>
<li>From January 2023 Class 1 shifts are valued 1.80 for night shifts in the weekend, 1.20 for other weekend shifts and night shifts during the week and 0.60 at other times.</li>

  </ul>
</div>

<script type="text/javascript" class="init" src="OtpScrollTest.js?random=<?php echo uniqid(); ?>"></script>



  <div class="row footer navbar navbar-expand-lg navbar-dark bg-dark">
      <ul class="nav navbar-nav mr-auto">
          <li class="nav-item footer">
            <a class="nav-link" href="https://gitlab.cern.ch/atlas-otp/otp-reports/pipelines">
              Version: 5.6.1-4-g23423ae

            </a>
          </li>
          <?php if (str_contains($_SERVER['SERVER_NAME'], '-alpha')) { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-test.web.cern.ch">Goto Test Reports</a></li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas.web.cern.ch">Goto Production Reports</a></li>
          <?php } elseif (str_contains($_SERVER['SERVER_NAME'], '-test')) { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-alpha.web.cern.ch">Goto Staged Reports</a></li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas.web.cern.ch">Goto Production Reports</a></li>
          <?php } else { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-alpha.web.cern.ch">Goto Staged Reports</a></li>
          <?php } ?>
      </ul>

      <ul class="nav navbar-nav">
          <li class="nav-item footer">Copyright© 2010-2025 CERN</li>
          <li class="nav-item">
            <a class="nav-link mail" href="mailto:Mark.Donszelmann@cern.ch?subject=ATLAS OTP Reports">
              <i class="fa fa-envelope"></i>&nbsp;ATLAS OTP
            </a>
          </li>
      </ul>
  </div>
</div>


        <!-- footer.html -->


        
        
    </body>
</html>
