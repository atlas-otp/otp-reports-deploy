<?php

/**
 * @param array<string, string> $params
 */
function build_indico_request(string $path, array $params, string $api_key = null, string $secret_key = null, bool $only_public = false, bool $persistent = false): string {
    if($api_key) {
        $params['apikey'] = $api_key;
    }

    if($only_public) {
        $params['onlypublic'] = 'yes';
    }

    if($secret_key) {
        if(!$persistent) {
            $params['timestamp'] = time();
        }
        uksort($params, 'strcasecmp');
        $url = $path . '?' . http_build_query($params);
        $params['signature'] = hash_hmac('sha1', $url, $secret_key);
    }

    if(!$params) {
        return $path;
    }

    return $path . '?' . http_build_query($params);
}

// if(true) { // change to false if you want to include this file
    $API_KEY = '9e34bbad-b497-4d3c-b68b-2569390ef26d';
    $SECRET_KEY = '52ec982a-08fc-46d5-8dc0-62ad427957b1';
    // works
    $PATH = '/export/categ/1426-1998.html';
    // does not work (yet), seems CERN runs an older version of indico
    $PATH = '/export/event/489143/registrants.json';
    $PARAMS = array(
        'pretty' => 'yes',
        'no_cache' => 'yes'
    );
    echo 'https://indico.cern.ch' . build_indico_request($PATH, $PARAMS, $API_KEY, $SECRET_KEY) . "\n";
// }
