#!/bin/sh

dir="$(dirname "$(readlink -f $0)")"
output=$(php-cgi -f $dir/ConflictingShiftsData.php count)
count=$?
if [ $count -ne 0 ]
then
  echo "There are $count conflicting shifts in point1, check https://otp-atlas.web.cern.ch/otp-atlas/checks/ConflictingShifts.php" | ./otp-mail/otp-mail.py -s "Conflicting Shifts in Point1" -c duns@cern.ch atlas-otp-conflicting-shifts@cern.ch
fi
# echo $output
