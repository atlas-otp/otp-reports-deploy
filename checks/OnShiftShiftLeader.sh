#!/bin/sh

dir="$(dirname "$(readlink -f $0)")"
output=$(php-cgi -f $dir/OnShiftData.php not_ok task=531450 shadowTask=531609 trainingTask=532009)
not_ok=$?
if [ $not_ok -ne 0 ]
then
  echo "There are $not_ok Untrained Shift Leaders scheduled in point1, check https://otp-atlas.web.cern.ch/otp-atlas/checks/OnShift.php#year=2017&task=531450&shadowTask=531609&trainingTask=532009" | mailx -s "Untrained Shift Leaders in Point1" -c duns@cern.ch atlas-otp-conflicting-shifts@cern.ch
fi
#echo $output
