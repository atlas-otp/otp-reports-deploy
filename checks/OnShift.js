$(document).ready(function() {
  "use strict";
  otpInitBegin("OnShift");

  otpInitSelect("year");
  otpInitInput("task");
  otpInitInput("shadowTask");
  otpInitInput("minShadows");
  otpInitInput("trainingTask");
  otpInitInput("minTrainings");

  otpInitEnd();

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        if ("taskTitle" in json) {
          $('#taskTitle').html(json.taskTitle);
        }
        if ("shadowTaskTitle" in json) {
          $('#shadowTaskTitle').html(json.shadowTaskTitle);
        }
        if ("trainingTaskTitle" in json) {
          $('#trainingTaskTitle').html(json.trainingTaskTitle);
        }
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-sm-6:eq(0)'));
    },
    order: [[ 3, "asc" ], [ 5, "asc" ]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columnDefs: [
      {
        targets: 1,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? '<a href="mailto:'+row.EMAIL+'">'+data+'</a>' : data;
        }
      },
    ],
    columns: [
      { data: "FNAME" },
      { data: "LNAME" },
      { data: "EMAIL", sortable: false, visible: false, searchable: false },
      { data: "OK" },
      { data: "LAST_SHIFT", type: "nullable" },
      { data: "NEXT_SHIFT", type: "nullable" },
      { data: "DONE_SHIFTS", type: "nullable", className: "dt-right" },
      { data: "PLANNED_SHIFTS", type: "nullable", className: "dt-right" },
      { data: "TOTAL_SHIFTS", type: "nullable", className: "dt-right" },
      { data: "LAST_SHADOW", type: "nullable" },
      { data: "NEXT_SHADOW", type: "nullable" },
      { data: "TOTAL_SHADOWS", type: "nullable" },
      { data: "LAST_TRAINING", type: "nullable" },
      { data: "NEXT_TRAINING", type: "nullable" },
      { data: "TOTAL_TRAININGS", type: "nullable" }
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-sm-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-sm-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
          return intVal(a) + intVal(b);
      };

      // number of OKs
      var ok = [ 3 ];
      for (var i=0; i<ok.length; i++) {
        // Total over all pages
        var total_ok = api
            .column( ok[i], { search:'applied' } )
            .data()
            .reduce( function (a, b) {
                return a + (b == 'Yes' ? 1 : 0);
            }, 0 );

        // Update footer
        $( api.column( ok[i] ).footer() ).html(total_ok);
      }

      // create sums of all numeric columns
      var columns = [ 6, 7, 8, 11, 14 ];
      for (var j=0; j<columns.length; j++) {
        // Total over all pages
        var total = api
            .column( columns[j], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        $( api.column( columns[j] ).footer() ).html(total);
      }

      // count number of people
      var c = 0;
      api.column( 1, { search:'applied' } )
         .data()
         .reduce( function (a, b) {
              return c++;
         }, 0 );

      // Update footer
      $( api.column( 1 ).footer() ).html(c);
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  otpTable.push(table);
} );
