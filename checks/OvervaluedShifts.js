$(document).ready(function() {
  "use strict";
  var table = $('#table').DataTable( {
    ajax: {
      url: "./OvervaluedShiftsData.php",
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-sm-6:eq(0)'));
    },
    order: [ 1, "asc" ],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columns: [
      { data: "FNAME", className: "title" },
      { data: "LNAME", className: "title" },
      { data: "TASK_ID" },
      { data: "TASK" },
      { data: "MONTH_CODE" },
      { data: "LOAD" },
    ],
    footerCallback: function ( row, data, start, end, display ) {
//        console.log("Footer "+start+" "+end);
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-sm-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-sm-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());
    }
  } );
  otpTable.push(table);

} );
