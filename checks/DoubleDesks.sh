#!/bin/sh

dir="$(dirname "$(readlink -f $0)")"
output=$(php-cgi -f $dir/DoubleDesksData.php count)
count=$?
if [ $count -ne 0 ]
then
  echo "There are $count double desk shifts in point1, check https://otp-atlas.web.cern.ch/otp-atlas/checks/DoubleDesks.php" | mailx -s "Double Desk Shifts in Point1" -c duns@cern.ch atlas-otp-conflicting-shifts@cern.ch
fi
#echo $output
