$(document).ready(function() {
  "use strict";
  var table = $('#table').DataTable( {
    ajax: {
      url: "./CategoryUsageData.php",
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(180),
    lengthChange: false,
    buttons: otpGetExport(),
    order: [ 0, "asc" ],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columns: [
      { data: "ID" },
      { data: "SHORT_TITLE" },
      { data: "CATEGORY_CODE" },
    ],
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  otpTable.push(table);
} );
