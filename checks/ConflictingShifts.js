$(document).ready(function() {
  "use strict";
  var table = $('#table').DataTable( {
    ajax: {
      url: "./ConflictingShiftsData.php",
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-sm-6:eq(0)'));
    },
    columnDefs: [
      {
        targets: 5,
        render: function ( data, type, row, meta ) {
            return row.FIRST_HOUR_FROM+'-'+row.FIRST_HOUR_TO;
        }
      },
      {
        targets: 10,
        render: function ( data, type, row, meta ) {
            return row.HOUR_FROM+'-'+row.HOUR_TO;
        }
      },
    ],
    order: [ 3, "asc" ],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columns: [
      { data: "FNAME", className: "title" },
      { data: "LNAME", className: "title" },
      { data: "HOUR_DIFF" },

      { data: "FIRST_DT", type: "nullable" },
      { data: "FIRST_HOUR_FROM", visible: false },
      { data: "FIRST_HOUR_TO", visible: true },
      { data: "FIRST_TASK_ID" },
      { data: "FIRST_SHORT_TITLE" },

      { data: "DT", type: "nullable" },
      { data: "HOUR_FROM", visible: false },
      { data: "HOUR_TO", visible: true },
      { data: "TASK_ID" },
      { data: "SHORT_TITLE" }
    ],
    footerCallback: function ( row, data, start, end, display ) {
//        console.log("Footer "+start+" "+end);
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-sm-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-sm-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());
    }
  } );
  otpTable.push(table);

} );
