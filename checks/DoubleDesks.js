$(document).ready(function() {
  "use strict";
  var table = $('#table').DataTable( {
    ajax: {
      url: "./DoubleDesksData.php",
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-sm-6:eq(0)'));
    },
    order: [ 3, "asc" ],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columns: [
      { data: "FNAME" },
      { data: "LNAME" },
      { data: "SHORT_TITLE" },
      { data: "FIRST_SHIFT", type: "nullable" },
    ],
    footerCallback: function ( row, data, start, end, display ) {
//        console.log("Footer "+start+" "+end);
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-sm-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-sm-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());
    }
  } );
  otpTable.push(table);

} );
