$(document).ready(function() {
  "use strict";
  otpInitBegin("FTEbySystem");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('system', 'Systems');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd(updateCallback);

  var chart = $('#graph').highcharts({
    chart: {
      type: 'line'
    },

    title: {
      text: 'Total FTEs by Person'
    },

    subtitle: {
        text: ' '
    },

    xAxis: {
        title: {
            text: 'Person #'
        }
    },

    yAxis: {
        title: {
            text: 'Total FTEs'
        }
    },

    plotOptions: {
        series: {
            pointStart: 1,
            color: '#000000'
        }
    },

    series: [
    ],

    credits: {
      enabled: false
    },

    exporting: {
        enabled: true
    }
  });

  // otpChangeData();
});

function updateCallback() {
    $.getJSON(otpGetUrl(chart), function(json) {
       var chart = $('#graph').highcharts();

       var system = otpGetValueOrAll('system');
       var value = system;

       chart.title.update({ text: 'Total ' + otpUnit + ' by Person'});
       chart.subtitle.update({ text: value});
       chart.yAxis[0].update({ title: { text: otpUnit}});


       while(chart.series.length > 0) {
         chart.series[0].remove(false);
       }

       var points = [];
       for (var i = 0; i < json.data.length; i++) {
         // result of toFixed is string
         points[i] = Number(Number(json.data[i].FTE*otpFactor).toFixed(2));
       }
       var series = chart.addSeries({ name: value, id: 0 }, false);
       series.setData(points, true);

       // chart.redraw();
    });
}
