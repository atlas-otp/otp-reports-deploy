<!DOCTYPE html>

<!-- warning.html -->

<!--------------------------------------------------------------------------->
<!--                                                                       -->
<!-- WARNING, this file is generated, please edit the source and reconvert -->
<!--                                                                       -->
<!--                                                                       -->
<!--                                                                       -->
<!--------------------------------------------------------------------------->



<?php
error_reporting(E_ALL);
ini_set("display_errors", '1');
?>


<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>


<html lang="en">
    <head>
        <!-- header.html -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="OTP, Operation Task Planner">
<meta name="keywords" content="CERN, ATLAS, OTP">
<meta name="author" content="Mark Donszelmann">
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<title>
   &middot; OTP ATLAS
</title>

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../ico/atlas-144x144.png">
                               <link rel="shortcut icon" href="../ico/atlas.ico">

<!-- jQuery -->
<!-- NOTE: no Ajax in 'slim' version of jquery -->
<!-- NOTE: jquery 3.x did not work -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- jQuery TimeAgo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>

<!-- jQuery Resizable -->
<!--
<script src="../jquery-resizable-0.28/dist/jquery-resizable.min.js"></script>
-->

<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/cerulean/bootstrap.min.css">
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >

<!-- Bootstrap Select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

<!-- Bootstrap DateRangePicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>

<!-- Datatables, jquery then bootstrap, but NOTE removal below -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css" >
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
<!-- Removing container-fluid on class of dataTables_wrapper as it interferes with scrollResize -->
var DataTable = $.fn.dataTable;
$.extend( DataTable.ext.classes, {
    sWrapper:      "dataTables_wrapper dt-bootstrap4",
} );
</script>

<!-- Datatables Extensions -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.0.3/css/rowGroup.dataTables.min.css">
<script src="https://cdn.datatables.net/rowgroup/1.0.3/js/dataTables.rowGroup.min.js"></script>

<!-- Does not work with scrollResize
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/1.5.0/css/scroller.dataTables.min.css">
<script src="https://cdn.datatables.net/scroller/1.5.0/js/dataTables.scroller.min.js"></script>
-->

<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.dataTables.min.css">
<script src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>

<!-- Datatables Plugins -->
<script src="https://cdn.datatables.net/plug-ins/1.10.19/features/scrollResize/dataTables.scrollResize.min.js"></script>

<!-- Highcharts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/modules/exporting.js"></script>

<!-- Search JS -->
<!--
Below are possibly these ?
<script src="https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.0.1/lunr.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="http://stevenlevithan.com/assets/misc/date.format.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.10/URI.min.js" type="text/javascript"></script>
-->
<!--
<script src="../js/search.min.js" type="text/javascript"></script>
-->
<script src="../js/lunr.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/mustache.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/date.format.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/URI.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/jquery.lunr.search.js" type="text/javascript" charset="utf-8"></script>

<!-- ATLAS and OTP Stylesheets and JavaScript-->
<link rel="stylesheet" href="../css/atlas.css" >
<link rel="stylesheet" href="../css/otp.css" >
<script src="../js/otp.js" type="text/javascript"></script>




        
        <style>
  .table td, .table th {
    text-align: center;
    vertical-align: baseline;
  }
  .table-bordered td, .table-bordered th {
    border: 4px solid #dee2e6;
  }
</style>

    </head>

    <body>
        
<div class="container-fluid">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="../">ATLAS OTP Reports</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Reports
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../reports/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/TaskScheduleP1.php">Task Schedule Point1</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/FundingAgency.php">Funding Agency</a>
            <a class="dropdown-item" href="../reports/FundingAgency.php?byInstitution">Funding Agency by Institution</a>
            <a class="dropdown-item" href="../reports/Institution.php">Institution</a>
            <a class="dropdown-item" href="../reports/InstitutionYear.php">Institution by Year</a>
            <a class="dropdown-item" href="../reports/Task.php">Task</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/MultiYear.php">Multi-Year</a>
            <a class="dropdown-item" href="../reports/MultiYear.php?byInstitution">Multi-Year by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/Commitment.php">Commitment</a>
            <a class="dropdown-item" href="../reports/CommitmentFunding.php">Commitment Funding</a>
            <a class="dropdown-item" href="../reports/CommitmentFunding.php?byInstitution">Commitment Funding by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/PersonalSchedule.php">Personal Schedule</a>
            <a class="dropdown-item" href="../reports/TaskSchedule.php">Task Schedule</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/Occupancy.php">Occupancy</a>
            <a class="dropdown-item" href="../reports/Personal.php">Personal</a>
            <a class="dropdown-item" href="../reports/ShiftsByHour.php">Shifts by Hour (SLIMOS)</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/TaskPersonnel.php">Task Personnel</a>
            <a class="dropdown-item" href="../reports/Allocation.php">Allocation</a>
            <a class="dropdown-item" href="../reports/Requirement.php">Requirement</a>
            <a class="dropdown-item" href="../reports/History.php">History</a>

          </div>
        </li>
<!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Plots
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../plots/index.php">Description*</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../plots/FTEbyActivity.php">FTEbyActivity</a>
            <a class="dropdown-item" href="../plots/FTEbySystem.php">FTEbySystem</a>
          </div>
        </li>
-->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Checks
          </a>
          <div class="dropdown-menu">
          <!-- placement exactly under menu
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
          -->
            <a class="dropdown-item" href="../checks/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../checks/CategoryUsage.php">Category Usage</a>
            <a class="dropdown-item" href="../checks/ConflictingShifts.php">Conflicting Shifts</a>
            <a class="dropdown-item" href="../checks/DoubleDesks.php">Double Desks</a>
            <a class="dropdown-item" href="../checks/OvervaluedShifts.php">Overvalued Shifts</a>
            <a class="dropdown-item" href="../checks/OnShift.php">On Shift</a>
          </div>
        </li>

<!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Tests
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../tests/GetClientAddress.php">Get Client Address</a>
            <a class="dropdown-item" href="../tests/PublicPhoneListRedirect.html">Public Phonelist Redirect</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../tests/BootstrapSelectTest.html">Bootstrap Select Test</a>
            <a class="dropdown-item" href="../tests/OtpBootstrapSelectTest.php">OTP Bootstrap Select Test</a>
            <a class="dropdown-item" href="../tests/ScrollTest.html">Scroll Test</a>
            <a class="dropdown-item" href="../tests/OtpScrollTest.php">OTP Scroll Test</a>

            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header btn-danger">Not Working</h6>
            <a class="dropdown-item" href="../tests/TrainingIndico.php">Training Indico</a>
            <a class="dropdown-item" href="../tests/UnicodeTest.php">Unicode Test</a>
          </div>
        </li>
-->
        <li class="nav-item">
          <a class="nav-link" href="../final_reports/index.php">Final Reports</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="https://otp-atlas.web.cern.ch">Website</a>
        </li>
      <!--
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      -->
      </ul>
    <!--
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    -->
    </div>
  </nav>

  <div class="card panel-default">
  <div class="card-header">
    <div class="card-title">
      <span class="reportName">Funding Agency - Final Reports</span>
    </div>
  </div>
</div>

<table class="table table-striped table-bordered">
  <tr>
    <th>Year</th>
    <th>Class 1</th>
    <th>Class 2</th>
    <th>Class 3</th>
    <th>Class 4</th>
    <th>Multi-Year</th>
  </tr>

  <tr>
    <td scope="row">2023</td>
    <td colspan="2">
      <a href="../reports/Institution.php?byTask#year=2023&category=Class 1,Class 2" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2023&category=Class 1,Class 2" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2023&category=Class 1,Class 2" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2023&category=Class 1,Class 2" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/Institution.php?byTask#year=2023&category=Class 3" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2023&category=Class 3" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2023&category=Class 3" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2023&category=Class 3" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/FundingAgency.php#year=2023&category=Class 4" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2023&category=Class 4" class="btn btn-info">FA by Institution</a>
    </td>
    <td>
      <a href="../reports/MultiYear.php?byInstitution" class="btn btn-info">FA by Institution</a>
    </td>
  </tr>

  <tr>
    <td scope="row">2022</td>
    <td colspan="2">
      <a href="../reports/Institution.php?byTask#year=2022&category=Class 1,Class 2" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2022&category=Class 1,Class 2" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2022&category=Class 1,Class 2" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2022&category=Class 1,Class 2" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/Institution.php?byTask#year=2022&category=Class 3" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2022&category=Class 3" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2022&category=Class 3" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2022&category=Class 3" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/FundingAgency.php#year=2022&category=Class 4" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2022&category=Class 4" class="btn btn-info">FA by Institution</a>
    </td>
    <td>
      <a href="../reports/MultiYear.php?byInstitution" class="btn btn-info">FA by Institution</a>
    </td>
  </tr>

  <tr>
    <td scope="row">2021</td>
    <td colspan="2">
      <a href="../reports/Institution.php?byTask#year=2021&category=Class 1,Class 2" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2021&category=Class 1,Class 2" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2021&category=Class 1,Class 2" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2021&category=Class 1,Class 2" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/Institution.php?byTask#year=2021&category=Class 3" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2021&category=Class 3" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2021&category=Class 3" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2021&category=Class 3" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/FundingAgency.php#year=2021&category=Class 4" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2021&category=Class 4" class="btn btn-info">FA by Institution</a>
    </td>
    <td>
      <a href="../reports/MultiYear.php?byInstitution" class="btn btn-info">FA by Institution</a>
    </td>
  </tr>

  <tr>
    <td scope="row">2020</td>
    <td colspan="2">
      <a href="../reports/Institution.php?byTask#year=2020&category=Class 1,Class 2" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2020&category=Class 1,Class 2" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2020&category=Class 1,Class 2" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2020&category=Class 1,Class 2" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/Institution.php?byTask#year=2020&category=Class 3" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2020&category=Class 3" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2020&category=Class 3" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2020&category=Class 3" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/FundingAgency.php#year=2020&category=Class 4" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2020&category=Class 4" class="btn btn-info">FA by Institution</a>
    </td>
    <td>
      <a href="../reports/MultiYear.php?byInstitution" class="btn btn-info">FA by Institution</a>
    </td>
  </tr>

  <tr>
    <td scope="row">2019</td>
    <td colspan="2">
      <a href="../reports/Institution.php?byTask#year=2019&category=Class 1,Class 2" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2019&category=Class 1,Class 2" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2019&category=Class 1,Class 2" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2019&category=Class 1,Class 2" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/Institution.php?byTask#year=2019&category=Class 3" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2019&category=Class 3" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2019&category=Class 3" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2019&category=Class 3" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/FundingAgency.php#year=2019&category=Class 4" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2019&category=Class 4" class="btn btn-info">FA by Institution</a>
    </td>
    <td>
      <a href="../reports/MultiYear.php?byInstitution" class="btn btn-info">FA by Institution</a>
    </td>
  </tr>

  <tr>
    <td scope="row">2018</td>
    <td colspan="2">
      <a href="../reports/Institution.php?byTask#year=2018&category=Class 1,Class 2" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2018&category=Class 1,Class 2" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2018&category=Class 1,Class 2" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2018&category=Class 1,Class 2" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/Institution.php?byTask#year=2018&category=Class 3" class="btn btn-info">Institution</a>
      <a href="../reports/FundingAgency.php#year=2018&category=Class 3" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2018&category=Class 3" class="btn btn-info">FA by Institution</a>
      <a href="../reports/Task.php#year=2018&category=Class 3" class="btn btn-info">Task</a>
    </td>
    <td>
      <a href="../reports/FundingAgency.php#year=2018&category=Class 4" class="btn btn-info">Funding Agency</a>
      <a href="../reports/FundingAgency.php?byInstitution#year=2018&category=Class 4" class="btn btn-info">FA by Institution</a>
    </td>
    <td>
      <a href="../reports/MultiYear.php?byInstitution" class="btn btn-info">FA by Institution</a>
    </td>
  </tr>

  <tr>
    <td scope="row">2017</td>
    <td colspan="2">
      <a href="https://otp-data.web.cern.ch/final_reports/2017/Funding_Agency_Report_Class_1_and_2_2017.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2017/Funding_Agency_Report_Class_1_and_2_2017.xls" class="btn btn-success">Excel</a>
    </td>
    <td>
      <a href="https://otp-data.web.cern.ch/final_reports/2017/Funding_Agency_Report_Class_3_2017.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2017/Funding_Agency_Report_Class_3_2017.xls" class="btn btn-success">Excel</a>
    </td>
    <td>
      <a href="https://otp-data.web.cern.ch/final_reports/2017/Funding_Agency_Report_Class_4_2017.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2017/Funding_Agency_Report_Class_4_2017.xls" class="btn btn-success">Excel</a>
    </td>
    <td>
      <a href="https://otp-data.web.cern.ch/final_reports/2017/Multi_Year_Report_2017.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2017/Multi_Year_Report_2017.xls" class="btn btn-success">Excel</a>
    </td>
  </tr>

  <tr>
    <td scope="row">2016</td>
    <td colspan="2">
      <a href="https://otp-data.web.cern.ch/final_reports/2016/Funding_Agency_Report_Class_1_and_2_2016.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2016/Funding_Agency_Report_Class_1_and_2_2016.xls" class="btn btn-success">Excel</a>
    </td>
    <td>
      <a href="https://otp-data.web.cern.ch/final_reports/2016/Funding_Agency_Report_Class_3_2016.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2016/Funding_Agency_Report_Class_3_2016.xls" class="btn btn-success">Excel</a>
    </td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2016/Funding_Agency_Report_Class_4_2016.pdf" class="btn btn-info">PDF</a></td>
    <td></td>
  </tr>

  <tr>
    <td scope="row">2015</td>
    <td colspan="2">
      <a href="https://otp-data.web.cern.ch/final_reports/2015/Funding_Agency_Report_Class_1_and_2_2015.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2015/Funding_Agency_Report_Class_1_and_2_2015.xls" class="btn btn-success">Excel</a>
    </td>
    <td>
      <a href="https://otp-data.web.cern.ch/final_reports/2015/Funding_Agency_Report_Class_3_2015.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2015/Funding_Agency_Report_Class_3_2015.xls" class="btn btn-success">Excel</a>
    </td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2015/Funding_Agency_Report_Class_4_2015.pdf" class="btn btn-info">PDF</a></td>
    <td></td>
  </tr>

  <tr>
    <td scope="row">2014</td>
    <td colspan="3">
      <a href="https://otp-data.web.cern.ch/final_reports/2014/Funding_Agency_Report_Class_1_and_2_and_3_2014.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2014/Funding_Agency_Report_Class_1_and_2_and_3_2014.xls" class="btn btn-success">Excel</a>
    </td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2014/Funding_Agency_Report_Class_4_2014.pdf" class="btn btn-info">PDF</a></td>
    <td></td>
  </tr>

  <tr>
    <td scope="row">2013</td>
    <td></td>
    <td colspan="2">
      <a href="https://otp-data.web.cern.ch/final_reports/2013/Funding_Agency_Report_Class_2_and_3_2013.pdf" class="btn btn-info">PDF</a>
      <a href="https://otp-data.web.cern.ch/final_reports/2013/Funding_Agency_Report_Class_2_and_3_2013.xls" class="btn btn-success">Excel</a>
    </td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2013/Funding_Agency_Report_Class_4_2013.pdf" class="btn btn-info">PDF</a></td>
    <th>Summary</th>
  </tr>

  <tr>
    <td scope="row">2012</td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2012/Funding_Agency_Report_Class_1_2012.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2012/Funding_Agency_Report_Class_2_2012.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2012/Funding_Agency_Report_Class_3_2012.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2012/Funding_Agency_Report_Class_4_2012.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2012/Funding_Agency_Report_Summary_2012.pdf" class="btn btn-info">PDF</a></td>
  </tr>

  <tr>
    <td scope="row">2011</td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2011/Funding_Agency_Report_Class_1_2011.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2011/Funding_Agency_Report_Class_2_2011.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2011/Funding_Agency_Report_Class_3_2011.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2011/Funding_Agency_Report_Class_4_2011.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2011/Funding_Agency_Report_Summary_2011.pdf" class="btn btn-info">PDF</a></td>
  </tr>

  <tr>
    <td scope="row">2010</td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2010/Funding_Agency_Report_Class_1_2010.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2010/Funding_Agency_Report_Class_2_2010.pdf" class="btn btn-info">PDF</a></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2010/Funding_Agency_Report_Class_3_2010.pdf" class="btn btn-info">PDF</a></td>
    <td></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2010/Funding_Agency_Report_Summary_2010.pdf" class="btn btn-info">PDF</a></td>
  </tr>

  <tr>
    <td scope="row">2009</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td><a href="https://otp-data.web.cern.ch/final_reports/2009/Funding_Agency_Report_Summary_2009.pdf" class="btn btn-info">PDF</a></td>
  </tr>

</table>


  <div class="row footer navbar navbar-expand-lg navbar-dark bg-dark">
      <ul class="nav navbar-nav mr-auto">
          <li class="nav-item footer">
            <a class="nav-link" href="https://gitlab.cern.ch/atlas-otp/otp-reports/pipelines">
              Version: 5.6.1-4-g23423ae

            </a>
          </li>
          <?php if (str_contains($_SERVER['SERVER_NAME'], '-alpha')) { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-test.web.cern.ch">Goto Test Reports</a></li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas.web.cern.ch">Goto Production Reports</a></li>
          <?php } elseif (str_contains($_SERVER['SERVER_NAME'], '-test')) { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-alpha.web.cern.ch">Goto Staged Reports</a></li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas.web.cern.ch">Goto Production Reports</a></li>
          <?php } else { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-alpha.web.cern.ch">Goto Staged Reports</a></li>
          <?php } ?>
      </ul>

      <ul class="nav navbar-nav">
          <li class="nav-item footer">Copyright© 2010-2025 CERN</li>
          <li class="nav-item">
            <a class="nav-link mail" href="mailto:Mark.Donszelmann@cern.ch?subject=ATLAS OTP Reports">
              <i class="fa fa-envelope"></i>&nbsp;ATLAS OTP
            </a>
          </li>
      </ul>
  </div>
</div>


        <!-- footer.html -->


        
        
    </body>
</html>
