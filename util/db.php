<?php

global $db;

$sql_details = array(
	"type" => NULL,         // Database type: "Mysql", "Postgres", "Sqlserver", "Sqlite" or "Oracle"
	"user" => "",   		// Database user name
	"pass" => "",           // Database password (TBS)
	"host" => "",     		// Database host
	"port" => "",           // Database connection port (can be left empty for default)
	"db"   => "",           // Database name (dev: devdb19; test: int8r; prod: atlr)
	"dsn"  => "",           // PHP DSN extra information. Set as `charset=utf8mb4` if you are using MySQL
	"pdoAttr" => array()   	// PHP PDO attributes array. See the PHP documentation for all options
);

// define("DATATABLES", true);

require_once '../config.php';
include ("../editor/lib/DataTables.php");

use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate,
    DataTables\Editor\ValidateOptions,
    DataTables\Database,
       DataTables\Database\Query,
       DataTables\Database\Result;

echo "<DL>\n";
echo "<DT>type</DT>\n";
echo "<DD>".$sql_details["type"]."</DD>\n";
echo "<DT>user</DT>\n";
echo "<DD>".$sql_details["user"]."</DD>\n";
echo "<DT>db</DT>\n";
echo "<DD>".$sql_details["db"]."</DD>\n";
echo "<DT>host</DT>\n";
echo "<DD>".$sql_details["host"]."</DD>\n";
echo "<DT>port</DT>\n";
echo "<DD>".$sql_details["port"]."</DD>\n";
echo "<DT>dsn</DT>\n";
echo "<DD>".$sql_details["dsn"]."</DD>\n";
echo "</DL>\n";

$sql = 'SELECT * FROM PUB_SYSTEM_NODE';

$result = $db->sql( $sql );
echo "<table border='1'>\n";
while ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
    echo "<tr>\n";
    foreach ($row as $item) {
        echo "    <td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
    }
    echo "</tr>\n";
}
echo "</table>\n";
