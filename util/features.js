$(document).ready(function() {
    "use strict";

    var table = $('#table').DataTable({
      "ajax": {
        "url": "./features_data.php",
        "type": "POST",
        "dataSrc": ""
      },
      "paging": false,
      "info": false,
      // "scrollY": calcDataTableHeight(),
      "scrollCollapse": true,
      "columns": [
        { "data": "flag" },
        { "data": "local" },
        { "data": "staging-dev" },
        { "data": "staging-test" },
        { "data": "staging-alpha" },
        { "data": "staging-beta" },
        { "data": "production" },
      ],
      "columnDefs": [
        {
          "targets": [1,2,3,4,5,6],
          "render": function ( data, type, row, meta ) {
            if (type == "sort") {
              return data;
            }
            return data ? "&#x2713;" : "";
          }
        },
      ]
    });
});
