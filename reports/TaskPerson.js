$(document).ready(function() {
  "use strict";
  otpInitBegin("TaskPerson");

  otpInitDateRange();
  otpInitInput('task');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colTID = 0;
  var colTask = 1;
  var colPID = 2;
  var colLastName = 3;
  var colFirstName = 4;
  var colAlloc = 5;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete: function(settings, json) {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: [colPID],
        visible: false,
        searchable: false,
      },
      {
        targets: [colAlloc],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num"
      },
    ],
    order: [[ colTID, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      { data: "TID",
        title: "Task Id",
        className: "title",
      },
      { data: "TASK",
        title: "Task",
        className: "title",
      },
      { data: "PID",
        title: "Person Id",
        className: "title",
      },
      { data: "LAST_NAME",
        title: "Last Name",
        className: "title",
      },
      { data: "FIRST_NAME",
        title: "First Name",
        className: "title",
      },
      { data: "FTE",
      },
    ],
    footerCallback: function ( row, data, start, end, display ) {
//        console.log("Footer "+start+" "+end);
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // count number of Tasks
      var tid = 0;
      var fa = api
          .column( colTID, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            tid++;
            return a;
          }, 0 );
      $( api.column( colTID ).footer() ).html(tid);

      var suma = api
          .column( colAlloc, { search:'applied' } )
          .data()
          .reduce( function (a, b) {
              return doubleValue(a) + doubleValue(b);
          }, 0 );
      $( api.column( colAlloc ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
});
