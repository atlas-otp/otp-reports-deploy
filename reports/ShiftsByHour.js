$(document).ready(function() {
  "use strict";
  otpInitBegin("ShiftsByHour");

  otpInitSelect('year', 'Years');
  // otpInitInput("task");
  otpInitSelect('category', 'Classes');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colPersonId = 0;
  var colFirstName = 1;
  var colLastName = 2;
  var colFrom = 3;
  var colTo = 4;
  var colTotal = 17;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function (settings, json) {
      // console.log("Complete");
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: [5,6,7,8,9,10,11,12,13,14,15,16],
        render: function ( data, type, row, meta ) {
          var d = (data*otpMonthFactor).toFixed(1);
          return d !== 0 ? d : "";
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [colTotal],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(1);
        },
        className: "text-right",
        type: "num"
      },
    ],
    order: [[ colLastName, "asc"], [ colFirstName, "asc"], [ colFrom, "asc"], [ colTo, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columns: [
      { data: "PID",
        title: 'Person ID',
        className: "title",
        visible: false,
        searchable: false,
        sortable: false,
      },
      { data: "FNAME",
        title: 'First Name',
        className: "title",
      },
      { data: "LNAME",
        title: 'Last Name',
        className: "title",
      },
      { data: "HOUR_FROM",
        title: 'From',
        className: "title",
      },
      { data: "HOUR_TO",
        title: 'To',
        className: "title",
      },

      { data: "JAN",
        title: "Jan",
        sortable: false,
      },
      { data: "FEB",
        title: "Feb",
        sortable: false,
      },
      { data: "MAR",
        title: "Mar",
        sortable: false,
      },
      { data: "APR",
        title: "Apr",
        sortable: false,
      },
      { data: "MAY",
        title: "May",
        sortable: false,
      },
      { data: "JUN",
        title: "Jun",
        sortable: false,
      },
      { data: "JUL",
        title: "Jul",
        sortable: false,
      },
      { data: "AUG",
        title: "Aug",
        sortable: false,
      },
      { data: "SEP",
        title: "Sep",
        sortable: false,
      },
      { data: "OCT",
        title: "Oct",
        sortable: false,
      },
      { data: "NOV",
        title: "Nov",
        sortable: false,
      },
      { data: "DEC",
        title: "Dec",
        sortable: false,
      },

      { data: "SUMA",
        title: "Total",
        sortable: false,
      },
    ],
    rowGroup: {
      startRender: null,
      endRender: function ( rows, group ) {
        var html = '';
        html += '<td colspan="3"></td>';
        html += '<td>Sum:</td>';
        var columns = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC','SUMA'];
        for (var i = 0; i < columns.length; i++) {
          var total = rows.data().pluck(columns[i]).reduce(otpSum, 0);

          let factor = i < 12 ? otpMonthFactor : otpFactor;
          html += '<td class="text-right">' + (total*factor).toFixed(1) + '</td>';
        }
        return $('<tr/>').append(html);
      },
      dataSrc: 'PID'
    },
    headerCallback: function ( row, data, start, end, display ) {
    },
    footerCallback: function ( row, data, start, end, display ) {
      // console.log('Footer');
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      // create sums of all numeric month columns
      var points = [];
      var columns = [ 5,6,7,8,9,10,11,12,13,14,15,16 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( otpSum, 0 );

        // Update footer
        total = (total*otpMonthFactor).toFixed(1);
        points[i] = Number(total);
        $( api.column( columns[i] ).footer() ).html(total);
      }

      var suma = api
          .column( colTotal, { search:'applied' } )
          .data()
          .reduce( otpSum, 0 );
      $( api.column( colTotal ).footer() ).html((suma*otpFactor).toFixed(1));
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header) {
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  // otpChangeData();
  // table.cells().invalidate().draw();

  otpTable.push(table);
} );
