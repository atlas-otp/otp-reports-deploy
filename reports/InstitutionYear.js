$(document).ready(function() {
  "use strict";
  otpInitBegin("InstitutionYear");

  otpInitSelect('category', 'Classes');
  otpInitSelect('funding', 'Funding Agencies');
  otpInitSelect('institution', 'Institutions');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  // Colors from: http://graphicdesign.stackexchange.com/questions/3682/where-can-i-find-a-large-palette-set-of-contrasting-colors-for-coloring-many-d
  /*
  var chart = $('#graph').highcharts({
      chart: {
          colors: [
            '#ff0000', '#e4e400', '#00ff00', '#00ffff', '#b0b0ff', '#ff00ff',
            '#870000', '#878700', '#008700', '#008787', '#4949ff', '#870087',
            '#550000', '#545400', '#005500', '#005555', '#0000ff', '#550055',
            '#b00000', '#baba00', '#00b000', '#00b0b0', '#8484ff', '#b000b0',
          ],
          type: 'column'
      },
      xAxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
          plotLines: [{
              value: 0,
              width: 1,
              color: '#808080'
          }]
      },
      plotOptions: {
          column: {
              stacking: 'normal',
              lineColor: '#666666',
              lineWidth: 1,
              marker: {
                  lineWidth: 1,
                  lineColor: '#666666'
              }
          }
      },
      series: [
      ]
  });
*/
  var colI = 0;
  var colFirstName = 1;
  var colLastName = 2;
  var colActivity = 3;
  var colSystem = 4;
  var colTask = 5;
  var colTaskID = 6;
  var colAlloc = 21;

  var byTask = window.location.search.includes('byTask');

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function (settings, json) {
      // console.log("Complete");
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
/*
      var chart = $('#graph').highcharts();
      chart.title.update({ text: 'Allocations '+otpGetValue('funding') });
      chart.yAxis[0].setTitle({ text: 'Allocations ['+otpUnit+']' });
      chart.redraw();
*/
    },
    columnDefs: [
      {
        targets: [7,8,9,10,11,12,13,14,15,16,17,18,19,20,21, 22],
        render: function ( data, type, row, meta ) {
          var d = (data*otpFactor).toFixed(otpDetailPrecision);
          return d !== 0 ? d : "";
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [colAlloc],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num"
      },
    ],
    orderFixed: [[ colI, "asc" ]],
    order: [[ colLastName, "asc"], [ colActivity, "asc"], [ colSystem, "asc"], [ colTask, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columns: [
      { data: "INAME",
        title: 'Institutions',
        className: "title",
        sortable: false,
      },
      { data: "FNAME",
        title: 'First Name',
        className: "title",
      },
      { data: "LNAME",
        title: 'Last Name',
        className: "title",
      },
      { data: "ACTIVITY",
        title: 'Activity',
        className: "title",
        visible: byTask,
        searchable: byTask
      },
      { data: "SYSTEM",
        title: 'System',
        className: "title",
        visible: byTask,
        searchable: byTask
      },
      { data: "TASK",
        title: 'Task',
        className: "title",
        visible: byTask,
        searchable: byTask
      },
      { data: "TASK_ID",
        title: 'Task ID',
        className: "title",
        visible: byTask,
        searchable: byTask
      },

      { data: "Y2010",
        title: "2010",
        sortable: false,
      },
      { data: "Y2011",
        title: "2011",
        sortable: false,
      },
      { data: "Y2012",
        title: "2012",
        sortable: false,
      },
      { data: "Y2013",
        title: "2013",
        sortable: false,
      },
      { data: "Y2014",
        title: "2014",
        sortable: false,
      },
      { data: "Y2015",
        title: "2015",
        sortable: false,
      },
      { data: "Y2016",
        title: "2016",
        sortable: false,
      },
      { data: "Y2017",
        title: "2017",
        sortable: false,
      },
      { data: "Y2018",
        title: "2018",
        sortable: false,
      },
      { data: "Y2019",
        title: "2019",
        sortable: false,
      },
      { data: "Y2020",
        title: "2020",
        sortable: false,
      },
      { data: "Y2021",
        title: "2021",
        sortable: false,
      },
      { data: "Y2022",
        title: "2022",
        sortable: false,
      },
      { data: "Y2023",
        title: "2023",
        sortable: false,
      },
      { data: "Y2024",
        title: "2024",
        sortable: false,
      },
      { data: "SUMA",
        title: "Alloc",
        sortable: false,
      },
    ],
    rowGroup: {
      endRender: function ( rows, group ) {
        // console.log('EndRender ' + group);
        // FIXME should move
        var doubleValue = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // FIXME should move
        var sum = function (a, b) {
          return doubleValue(a) + doubleValue(b);
        };

        var html = '';
        var colspan = byTask ? 6 : 2;
        html += '<td colspan="' + colspan + '">' + group + '<span class="rowCount-grid"><b>' + ' (' + rows.count() + ')' + '</b></span>' + '</td>';
        html += '<td>Sum:</td>';
        var columns = ['Y2010','Y2011','Y2012','Y2013','Y2014','Y2015','Y2016','Y2017','Y2018','Y2019','Y2020','Y2021','Y2022','Y2023','Y2024','SUMA'];
        var points = [];
        for (var i = 0; i < columns.length; i++) {
          var total = rows.data().pluck(columns[i]).reduce(sum, 0);

          html += '<td class="text-right">' + (total*otpFactor).toFixed(otpSumPrecision) + '</td>';

          if (i < 14) {
            points[i] = Number(total);
          }
        }
/*
        var chart = $('#graph').highcharts();
        var series = chart.addSeries({ name: group, id: group }, false);
        series.setData(points, false);
        series.update({ 'tooltip': { valueSuffix: ' '+otpUnit }}, false);
*/
        return $('<tr/>').append(html);
      },
      dataSrc: 'INAME'
    },
    headerCallback: function ( row, data, start, end, display ) {
      // console.log('Header');
/*
      var chart = $('#graph').highcharts();
      while(chart.series.length > 0) {
        chart.series[0].remove(false);
      }
*/
    },
    footerCallback: function ( row, data, start, end, display ) {
      // console.log('Footer');
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // create sums of all numeric month columns
      var points = [];
      var columns = [ 7,8,9,10,11,12,13,14,15,16,17,18,19,20,21, 22 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        total = (total*otpFactor).toFixed(otpSumPrecision);
        points[i] = Number(total);
        $( api.column( columns[i] ).footer() ).html(total);
      }

      var suma = api
          .column( colAlloc, { search:'applied' } )
          .data()
          .reduce( function (a, b) {
              return doubleValue(a) + doubleValue(b);
          }, 0 );
      $( api.column( colAlloc ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header) {
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
} );
