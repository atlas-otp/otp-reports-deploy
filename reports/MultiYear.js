$(document).ready(function() {
  "use strict";
  otpInitBegin("MultiYear");

  otpInitSelect('category', 'Classes');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colFA = 0;
  var colI = 1;

  var byInstitution = window.location.search.includes('byInstitution');

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        // Fix chinese clusters which until end of 2016 where merged into one, and after 2016 are two separate clusters.
        // Totals and percentages are recalculated.
        // var columns = ['2015', '2016'];
        // for (var col=0; col < columns.length; col++) {
        //   var column = columns[col];
        //   var chinese_cluster_value = 0;
        //   var chinese_cluster_row = -1;
        //   var value;
        //   for (var row=0; row < json.data.length; row++) {
        //     if (json.data[row].INAME == "China cluster") {
        //       chinese_cluster_row = row;
        //     }
        //     if (json.data[row].INAME == "China IHEP-NJU-THU cluster") {
        //       value = Number(json.data[row]['SUMA_' + column]);
        //       chinese_cluster_value += value;
        //       json.data[row].SUMA_TOTAL = Number(json.data[row].SUMA_TOTAL) - value;
        //       json.data[row].PERC_TOTAL = Number(json.data[row].SUMA_TOTAL) * 100 / Number(json.data[row].SUMR_TOTAL);
        //       json.data[row]['SUMA_' + column] = 0;
        //     }
        //     if (json.data[row].INAME == "China USTC-SDU-SJTU cluster") {
        //       value = Number(json.data[row]['SUMA_' + column]);
        //       chinese_cluster_value += value;
        //       json.data[row].SUMA_TOTAL = Number(json.data[row].SUMA_TOTAL) - value;
        //       json.data[row].PERC_TOTAL = Number(json.data[row].SUMA_TOTAL) * 100 / Number(json.data[row].SUMR_TOTAL);
        //       json.data[row]['SUMA_' + column] = 0;
        //     }
        //   }
        //   if (chinese_cluster_row >= 0) {
        //     json.data[chinese_cluster_row]['SUMA_' + column] = chinese_cluster_value;
        //     json.data[chinese_cluster_row]['PERC_' + column] = Number(json.data[chinese_cluster_row]['SUMA_' + column]) * 100 / Number(json.data[chinese_cluster_row]['SUMR_' + column]);
        //     json.data[chinese_cluster_row].SUMA_TOTAL = Number(json.data[chinese_cluster_row].SUMA_TOTAL) + chinese_cluster_value;
        //     json.data[chinese_cluster_row].PERC_TOTAL = Number(json.data[chinese_cluster_row].SUMA_TOTAL) * 100 / Number(json.data[chinese_cluster_row].SUMR_TOTAL);
        //   }
        // }
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete: function (settings, json) {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: [2, 3, 5, 6, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [4, 7, 10, 13, 16, 19, 22],
        className: "text-right",
        type: "num",
        render: function (data, type, row, meta) {
          return (type == "display") ? data < 0 ? "N/A" : Number(data).toFixed(otpPercentagePrecision) : Number(data);
        },
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          $(td).addClass(otpGetColorClass(cellData));
        },
      },
    ],
    order: [[ colFA, "asc" ], [ colI, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      { data: "FNAME",
        title: 'Funding Agency',
        className: "title"
      },
      { data: "INAME",
        title: 'Institution',
        className: "title",
        visible: byInstitution,
        searchable: byInstitution
      },

      { data: "SUMA_TOTAL",
      },
      { data: "SUMR_TOTAL",
      },
      { data: "PERC_TOTAL",
      },

      { data: "SUMA_2019",
      },
      { data: "SUMR_2019",
      },
      { data: "PERC_2019",
      },

      { data: "SUMA_2020",
      },
      { data: "SUMR_2020",
      },
      { data: "PERC_2020",
      },

      { data: "SUMA_2021",
      },
      { data: "SUMR_2021",
      },
      { data: "PERC_2021",
      },

      { data: "SUMA_2022",
      },
      { data: "SUMR_2022",
      },
      { data: "PERC_2022",
      },

      { data: "SUMA_2023",
      },
      { data: "SUMR_2023",
      },
      { data: "PERC_2023",
      },

      { data: "SUMA_2024",
      },
      { data: "SUMR_2024",
      },
      { data: "PERC_2024",
      }
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // count number of FAs and Is
      var inst = 0;
      var fa = api
          .column( colFA, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            inst++;
            return a;
          }, 0 );
      $( api.column( colFA ).footer() ).html(fa ? fa.size : 0);
      $( api.column( colI ).footer() ).html(inst);

      // create sums of all numeric columns
      var columns = [ 2,5,8,11,14,17,20 ];
      for (var i=0; i<columns.length; i++) {
        var suma = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        var sumr = api
            .column( columns[i]+1, { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        var perc = sumr > 0 ? suma * 100 / sumr : 0;

        // Update footer
        $( api.column( columns[i]   ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));
        $( api.column( columns[i]+1 ).footer() ).html((sumr*otpFactor).toFixed(otpSumPrecision));
        $( api.column( columns[i]+2 ).footer() ).html((perc).toFixed(otpPercentagePrecision));
        $( api.column( columns[i]+2 ).footer() ).addClass(otpGetColorClass(perc));
      }
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);

} );
