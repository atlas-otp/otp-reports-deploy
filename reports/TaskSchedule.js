$(document).ready(function() {
  "use strict";
  otpInitBegin("TaskSchedule");

  // otpInitSelectYear();
  otpInitDateRange();
  otpInitInput("task");
  otpInitSelect("name", "name", true);

  otpInitEnd();

  var colYWN = 0;
  var colTask = 1;
  var colTID = 2;
  var colCLS = 3;
  var colHF = 4;
  var colHT = 5;
  var colDESC = 6;
  var colBOW = 7;
  var colMR = 8;
  var colMA = 15;
  var colMP = 22;

  function dateFormat(date) {
    var month;
    // FIXME to be done with moment
    switch(date.getMonth()) {
      case 0: month="Jan"; break;
      case 1: month="Feb"; break;
      case 2: month="Mar"; break;
      case 3: month="Apr"; break;
      case 4: month="May"; break;
      case 5: month="Jun"; break;
      case 6: month="Jul"; break;
      case 7: month="Aug"; break;
      case 8: month="Sep"; break;
      case 9: month="Oct"; break;
      case 10: month="Nov"; break;
      case 11: month="Dec"; break;
      default:
        month = "none"; break;
    }
    return date.getDate()+'-'+month+'-'+date.getFullYear();
  }

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
    },
    orderFixed: [[ colYWN, "asc" ]],
    order: [[ colTask, "asc" ]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columnDefs: [
      {
        targets: [colTID, colCLS, colHF, colHT, colBOW, 15,16,17,18,19,20,21, 22,23,24,25,26,27,28],
        sortable: false,
        searchable: false,
        visible: false
      },
      {
        targets: [8,9,10,11,12,13,14],
        render: function ( data, type, row, meta ) {
            // var PID = 0
            var LAST_NAME = 1;
            var FIRST_NAME = 2;
            var CREDIT = 3;
            var INSTITUTE = 4;

            var MONDAY = 0;
            var TUESDAY = 1;
            var WEDNESDAY = 2;
            var THURSDAY = 3;
            var FRIDAY = 4;
            var SATURDAY = 5;
            var SUNDAY = 6;

            var CLASS1 = 'Class 1';

            var FACTOR_UP = 1.31;
            var FACTOR_DOWN = 0.66;

            var days = [];
            days[MONDAY] = "MON";
            days[TUESDAY] = "TUE";
            days[WEDNESDAY] = "WED";
            days[THURSDAY] = "THU";
            days[FRIDAY] = "FRI";
            days[SATURDAY] = "SAT";
            days[SUNDAY] = "SUN";

            var day_index = meta.col-8;
            var req = row[days[day_index]+"_R"];
            var alloc = row[days[day_index]+"_A"];

            var category = row.CLS;
            var hour_from = row.HOUR_FROM;
            if (category == CLASS1) {
              if ((day_index == SATURDAY) || (day_index == SUNDAY) || (hour_from == 23)) {
                req /= FACTOR_UP;
                alloc /= FACTOR_UP;
              } else {
                req /= FACTOR_DOWN;
                alloc /= FACTOR_DOWN;
              }
            }

            var who = row[days[day_index]+"_P"];
            var persons = who === null ? [''] : who.split(';');
            var diff = req - alloc;
            var clz = "percentage badge " + (diff > 0 ? "badge-danger" : diff < 0 ? "badge-primary" : "badge-success");
            var html = '<div class="d-flex flex-column">';
            for (var i = 0; i < persons.length; i++) {
              var values = persons[i].split(':');
              if ((req > 0) && (values.length >= 5)) {
                var full_name = values[FIRST_NAME] + ' ' + values[LAST_NAME];
                var short_first_name = values[FIRST_NAME].charAt(0) + '. ' + values[LAST_NAME];
                var short_last_name = values[FIRST_NAME].split(" ", 1)[0] + ' ' + values[LAST_NAME].charAt(0) + '.';
                var name = otpGetValue('name') == "Full Name" ? full_name : otpGetValue('name') == "F. Last" ? short_first_name : short_last_name;
                var institute = values[INSTITUTE];

                html += '<div class="' + clz + '" title="' + full_name + (institute != '' ? ' - ' + institute : '') + '">';
                // if (category == CLASS1) {
                //   values[CREDIT] /= ((day_index == SATURDAY) || (day_index == SUNDAY) || (hour_from == 23)) ? FACTOR_UP : FACTOR_DOWN;
                // }

                // credit is requirement if no allocation
                var credit = alloc > 0 ? values[CREDIT] : req;
                credit = (credit != 100) && (credit != 0) ? ' (' + Number(credit/100).toFixed(2) +')' : '';

                // covered
                var percentage = alloc*100/req;
                percentage = (percentage != 100) && (percentage != 0) ? Number(percentage).toFixed(0) + ' % ' : '';

                // console.log(persons[i]+" "+req+" "+alloc+" "+diff+" "+percentage+" "+credit + " " + (alloc*100/req));
                name = alloc > 0 ? name : 'Unallocated';

                html +=  percentage + name + credit;
                html += '</div>';
              }
            }
            html += '</div>';
            return type === 'display' ? html : data;
        },
        sortable: false, searchable: false
      },
    ],
    columns: [
      { data: "YEAR_WEEK_NUMBER",
        className: "title",
        sortable: false,
        visible: false,
        searchable: false,
        render: function ( data, type, row, meta ) {
          return type === 'display' ? parseInt(data.toString().slice(-2)) : data;
        }
      },
      { data: "TASK",
        render: function ( data, type, row, meta ) {
          return type === 'display' ? '<a href="https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#Ma0_Task_panel(N'+row.TID+')">'+data+'</a>' : data;
        }
      },
      { data: "TID",
        visible: false },
      { data: "CLS" },
      { data: "HOUR_FROM" },
      { data: "HOUR_TO" },
      { data: "DESCRIPTION",
        render: function ( data, type, row, meta ) {
          return type === 'display' ? row.HOUR_FROM+'-'+row.HOUR_TO+' '+data : data;
        }
      },
      { data: "BEGIN_OF_WEEK",
        type: "nullable" },

      { data: "MON_R" },
      { data: "TUE_R" },
      { data: "WED_R" },
      { data: "THU_R" },
      { data: "FRI_R" },
      { data: "SAT_R" },
      { data: "SUN_R" },

      { data: "MON_A" },
      { data: "TUE_A" },
      { data: "WED_A" },
      { data: "THU_A" },
      { data: "FRI_A" },
      { data: "SAT_A" },
      { data: "SUN_A" },

      { data: "MON_P" },
      { data: "TUE_P" },
      { data: "WED_P" },
      { data: "THU_P" },
      { data: "FRI_P" },
      { data: "SAT_P" },
      { data: "SUN_P" },
    ],
    rowGroup: {
      startRender: function ( rows, group ) {
        var data = rows.data();

        // once rowspan works, should disable the rendering of the weeknumber cells, can do the same for Task
        //  rowspan="' + (data.length + 1) + '"
        group = parseInt(group.toString().slice(-2));
        var html = '<td class="title" style="color: black;" colspan="2">Week: ' + group + '</td>';
        var begin_of_week = new Date(data[0].BEGIN_OF_WEEK);
        for (var i = 0; i < 7; i++) {
          html += '<td><span class="date">'+dateFormat(begin_of_week)+'</span></td>';
          begin_of_week.setDate(begin_of_week.getDate() + 1);
        }
        return $('<tr/>').append(html);
      },
      // endRender: function ( rows, group ) {
      // },
      dataSrc: 'YEAR_WEEK_NUMBER'
    }
  });


  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  otpTable.push(table);
});
