<!DOCTYPE html>

<!-- warning.html -->

<!--------------------------------------------------------------------------->
<!--                                                                       -->
<!-- WARNING, this file is generated, please edit the source and reconvert -->
<!--                                                                       -->
<!--                                                                       -->
<!--                                                                       -->
<!--------------------------------------------------------------------------->



<?php
error_reporting(E_ALL);
ini_set("display_errors", '1');
?>


<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>


<html lang="en">
    <head>
        <!-- header.html -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="OTP, Operation Task Planner">
<meta name="keywords" content="CERN, ATLAS, OTP">
<meta name="author" content="Mark Donszelmann">
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<title>
   &middot; OTP ATLAS
</title>

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../ico/atlas-144x144.png">
                               <link rel="shortcut icon" href="../ico/atlas.ico">

<!-- jQuery -->
<!-- NOTE: no Ajax in 'slim' version of jquery -->
<!-- NOTE: jquery 3.x did not work -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- jQuery TimeAgo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>

<!-- jQuery Resizable -->
<!--
<script src="../jquery-resizable-0.28/dist/jquery-resizable.min.js"></script>
-->

<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/cerulean/bootstrap.min.css">
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >

<!-- Bootstrap Select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

<!-- Bootstrap DateRangePicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>

<!-- Datatables, jquery then bootstrap, but NOTE removal below -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css" >
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
<!-- Removing container-fluid on class of dataTables_wrapper as it interferes with scrollResize -->
var DataTable = $.fn.dataTable;
$.extend( DataTable.ext.classes, {
    sWrapper:      "dataTables_wrapper dt-bootstrap4",
} );
</script>

<!-- Datatables Extensions -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.0.3/css/rowGroup.dataTables.min.css">
<script src="https://cdn.datatables.net/rowgroup/1.0.3/js/dataTables.rowGroup.min.js"></script>

<!-- Does not work with scrollResize
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/1.5.0/css/scroller.dataTables.min.css">
<script src="https://cdn.datatables.net/scroller/1.5.0/js/dataTables.scroller.min.js"></script>
-->

<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.dataTables.min.css">
<script src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>

<!-- Datatables Plugins -->
<script src="https://cdn.datatables.net/plug-ins/1.10.19/features/scrollResize/dataTables.scrollResize.min.js"></script>

<!-- Highcharts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/modules/exporting.js"></script>

<!-- Search JS -->
<!--
Below are possibly these ?
<script src="https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.0.1/lunr.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="http://stevenlevithan.com/assets/misc/date.format.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.10/URI.min.js" type="text/javascript"></script>
-->
<!--
<script src="../js/search.min.js" type="text/javascript"></script>
-->
<script src="../js/lunr.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/mustache.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/date.format.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/URI.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/jquery.lunr.search.js" type="text/javascript" charset="utf-8"></script>

<!-- ATLAS and OTP Stylesheets and JavaScript-->
<link rel="stylesheet" href="../css/atlas.css" >
<link rel="stylesheet" href="../css/otp.css" >
<script src="../js/otp.js" type="text/javascript"></script>




        
        
    </head>

    <body>
        
<div class="container">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="../">ATLAS OTP Reports</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Reports
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../reports/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/TaskScheduleP1.php">Task Schedule Point1</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/FundingAgency.php">Funding Agency</a>
            <a class="dropdown-item" href="../reports/FundingAgency.php?byInstitution">Funding Agency by Institution</a>
            <a class="dropdown-item" href="../reports/Institution.php">Institution</a>
            <a class="dropdown-item" href="../reports/InstitutionYear.php">Institution by Year</a>
            <a class="dropdown-item" href="../reports/Task.php">Task</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/MultiYear.php">Multi-Year</a>
            <a class="dropdown-item" href="../reports/MultiYear.php?byInstitution">Multi-Year by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/Commitment.php">Commitment</a>
            <a class="dropdown-item" href="../reports/CommitmentFunding.php">Commitment Funding</a>
            <a class="dropdown-item" href="../reports/CommitmentFunding.php?byInstitution">Commitment Funding by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/PersonalSchedule.php">Personal Schedule</a>
            <a class="dropdown-item" href="../reports/TaskSchedule.php">Task Schedule</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/Occupancy.php">Occupancy</a>
            <a class="dropdown-item" href="../reports/Personal.php">Personal</a>
            <a class="dropdown-item" href="../reports/ShiftsByHour.php">Shifts by Hour (SLIMOS)</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/TaskPersonnel.php">Task Personnel</a>
            <a class="dropdown-item" href="../reports/Allocation.php">Allocation</a>
            <a class="dropdown-item" href="../reports/Requirement.php">Requirement</a>
            <a class="dropdown-item" href="../reports/History.php">History</a>

          </div>
        </li>
<!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Plots
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../plots/index.php">Description*</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../plots/FTEbyActivity.php">FTEbyActivity</a>
            <a class="dropdown-item" href="../plots/FTEbySystem.php">FTEbySystem</a>
          </div>
        </li>
-->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Checks
          </a>
          <div class="dropdown-menu">
          <!-- placement exactly under menu
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
          -->
            <a class="dropdown-item" href="../checks/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../checks/CategoryUsage.php">Category Usage</a>
            <a class="dropdown-item" href="../checks/ConflictingShifts.php">Conflicting Shifts</a>
            <a class="dropdown-item" href="../checks/DoubleDesks.php">Double Desks</a>
            <a class="dropdown-item" href="../checks/OvervaluedShifts.php">Overvalued Shifts</a>
            <a class="dropdown-item" href="../checks/OnShift.php">On Shift</a>
          </div>
        </li>

<!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Tests
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../tests/GetClientAddress.php">Get Client Address</a>
            <a class="dropdown-item" href="../tests/PublicPhoneListRedirect.html">Public Phonelist Redirect</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../tests/BootstrapSelectTest.html">Bootstrap Select Test</a>
            <a class="dropdown-item" href="../tests/OtpBootstrapSelectTest.php">OTP Bootstrap Select Test</a>
            <a class="dropdown-item" href="../tests/ScrollTest.html">Scroll Test</a>
            <a class="dropdown-item" href="../tests/OtpScrollTest.php">OTP Scroll Test</a>

            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header btn-danger">Not Working</h6>
            <a class="dropdown-item" href="../tests/TrainingIndico.php">Training Indico</a>
            <a class="dropdown-item" href="../tests/UnicodeTest.php">Unicode Test</a>
          </div>
        </li>
-->
        <li class="nav-item">
          <a class="nav-link" href="../final_reports/index.php">Final Reports</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="https://otp-atlas.web.cern.ch">Website</a>
        </li>
      <!--
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      -->
      </ul>
    <!--
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    -->
    </div>
  </nav>

  <h3 id="otp-reports">OTP Reports</h3>

<p>The following OTP Reports are available (alphabetical order):</p>

<!---
-
| [Shift by Hour Report](ShiftByHour.php) |  Showing shifts by the hour for a task. |

| [Commitment Task Report](CommitmentTask.php)                                    | Showing commitments and allocations over the past 2 years by Task.                           |

-->

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Reports</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left"><a href="Allocation.php">Allocation</a></td>
      <td style="text-align: left">Showing allocations by person and task.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Appointment.php">Appointment</a></td>
      <td style="text-align: left">Showing Appointment by person and task.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Commitment.php">Commitment</a></td>
      <td style="text-align: left">Showing commitments and allocations over the past 2 years by Task/Requirement.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="CommitmentFunding.php">Commitment Funding</a></td>
      <td style="text-align: left">Showing commitments and allocations over the past 2 years by Funding Agency.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="CommitmentFunding.php?byInstitution">Commitment Funding by Institution</a></td>
      <td style="text-align: left">Showing commitments and allocations over the past 2 years by Funding Agency and Institution.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="FundingAgency.php">Funding Agency</a></td>
      <td style="text-align: left">Showing FTE or shifts by month and Funding Agency.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="FundingAgency.php?byInstitution">Funding Agency by Institution</a></td>
      <td style="text-align: left">Showing FTE or shifts by month and Funding Agency and Institution.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="FundingAgencyOverview.php">Funding Agency Overview</a></td>
      <td style="text-align: left">Shows FTE for past few years and summary by Funding Agency.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="History.php">History</a></td>
      <td style="text-align: left">Showing history on booking and un-booking.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Institution.php?byTask">Institution Task</a></td>
      <td style="text-align: left">Showing FTE or shifts by month and individual people and tasks.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Institution.php">Institution</a></td>
      <td style="text-align: left">Showing FTE or shifts by month and individual people.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="InstitutionYear.php?byTask">Institution Year Task</a></td>
      <td style="text-align: left">Showing FTE or shifts by year and individual people and tasks.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="InstitutionYear.php">Institution Year</a></td>
      <td style="text-align: left">Showing FTE or shifts by year and individual people.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="InstitutionOverview.php">Institution Overview</a></td>
      <td style="text-align: left">Shows FTE for past few years and summary by Institution</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="MultiYear.php">Multi Year</a></td>
      <td style="text-align: left">Showing FTE or shifts by multiple years and Funding Agency.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="MultiYear.php?byInstitution">Multi Year by Institution</a></td>
      <td style="text-align: left">Showing FTE or shifts by multiple years and Funding Agency and Institution.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Occupancy.php">Occupancy</a></td>
      <td style="text-align: left">Showing business of individual people.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Occupancy.php?byInstitute">Occupancy by Institute</a></td>
      <td style="text-align: left">Showing business of individual people by real institute.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Personal.php">Personal</a></td>
      <td style="text-align: left">Showing tasks for individual people.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="PersonalSchedule.php">Personal Schedule</a></td>
      <td style="text-align: left">Showing tasks for a person by days and weeks.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Predictions.php">Predictions</a></td>
      <td style="text-align: left">Showing predictions (Upgrade Construction only) by Task/Requirement.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Requirement.php">Requirement</a></td>
      <td style="text-align: left">Showing requirements and allocations by task.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="ShiftsByHour.php">Shift by Hour</a></td>
      <td style="text-align: left">Showing shifts by hour for SLIMOS</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="Task.php">Task</a></td>
      <td style="text-align: left">Showing FTE or shifts by month and Task.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="TaskPerson.php">Task Person</a></td>
      <td style="text-align: left">Showing FTE or shifts by Task and Person.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="TaskPersonnel.php">Task Personnel</a></td>
      <td style="text-align: left">Showing FTE or shifts by month by Task/Subtask and Personnel by Insitiution.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="TaskPersonnelNoInst.php">Task Personnel No Institutions</a></td>
      <td style="text-align: left">Showing FTE or shifts by month by Task/Subtask and Personnel without Institution.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="TaskSchedule.php">Task Schedule</a></td>
      <td style="text-align: left">Showing task requirements and allocations by days and weeks.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="TaskScheduleP1.php">Task Schedule Point1</a></td>
      <td style="text-align: left">Showing task requirements and allocations by days and weeks for shifts in Point1.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="TaskYear.php">Task Year</a></td>
      <td style="text-align: left">Showing FTE or shifts by year and Task/Requirement.</td>
    </tr>
  </tbody>
</table>

<p>If you have any suggestions to add more reports or to improve the existing reports, please contact <a href="mailto:Mark.Donszelmann@cern.ch?subject=ATLAS OTP Website">Mark Dönszelmann</a></p>

<p>The reports above use the following technologies:</p>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th style="text-align: left">Name</th>
      <th style="text-align: left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align: left"><a href="https://otp-atlas.web.cern.ch/docs/API.php">OTP Public API</a></td>
      <td style="text-align: left">To access the data in OTP.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="http://www.php.net">PHP</a></td>
      <td style="text-align: left">To handle the server side lookups of data.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="https://jekyllrb.com">jekyll</a></td>
      <td style="text-align: left">To build the website.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="https://getbootstrap.com">Bootstrap</a></td>
      <td style="text-align: left">To style the website.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="https://en.wikipedia.org/wiki/JavaScript">JavaScript</a></td>
      <td style="text-align: left">To handle data in the browser.</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="https://jquery.com">jQuery</a></td>
      <td style="text-align: left">To simplify JavaScript</td>
    </tr>
    <tr>
      <td style="text-align: left"><a href="https://datatables.net">DataTables</a></td>
      <td style="text-align: left">To display data in tables.</td>
    </tr>
  </tbody>
</table>


  <div class="row footer navbar navbar-expand-lg navbar-dark bg-dark">
      <ul class="nav navbar-nav mr-auto">
          <li class="nav-item footer">
            <a class="nav-link" href="https://gitlab.cern.ch/atlas-otp/otp-reports/pipelines">
              Version: 5.6.1-4-g23423ae

            </a>
          </li>
          <?php if (str_contains($_SERVER['SERVER_NAME'], '-alpha')) { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-test.web.cern.ch">Goto Test Reports</a></li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas.web.cern.ch">Goto Production Reports</a></li>
          <?php } elseif (str_contains($_SERVER['SERVER_NAME'], '-test')) { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-alpha.web.cern.ch">Goto Staged Reports</a></li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas.web.cern.ch">Goto Production Reports</a></li>
          <?php } else { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-alpha.web.cern.ch">Goto Staged Reports</a></li>
          <?php } ?>
      </ul>

      <ul class="nav navbar-nav">
          <li class="nav-item footer">Copyright© 2010-2025 CERN</li>
          <li class="nav-item">
            <a class="nav-link mail" href="mailto:Mark.Donszelmann@cern.ch?subject=ATLAS OTP Reports">
              <i class="fa fa-envelope"></i>&nbsp;ATLAS OTP
            </a>
          </li>
      </ul>
  </div>
</div>


        <!-- footer.html -->


        
        
    </body>
</html>
