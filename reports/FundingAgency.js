$(document).ready(function() {
  "use strict";
  otpInitBegin("FundingAgency");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('funding', 'Funding Agencies');
  otpInitSelect('institution', 'Institutions');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colFA = 0;
  var colI = 1;
  var colAlloc = 14;
  var colReq = 15;
  var colEst = 16;
  var colPerc = 17;

  var byInstitution = window.location.search.includes('byInstitution');

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete: function (settings, json) {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: [2,3,4,5,6,7,8,9,10,11,12,13],
        render: function ( data, type, row, meta ) {
          var d = (data*otpMonthFactor).toFixed(otpDetailPrecision);
          return d !== 0 ? d : "";
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [colAlloc],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [colEst],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num",
        createdCell: function (td, cellData, rowData, row, col) {
          $(td).addClass('estimate');
        },
      },
      {
        targets: [colReq],
        className: "text-right",
        type: "num",
        render: function (data, type, row, meta) {
          // ESTIMATE
          // var r = (otpGetEstimate(row.ESTR, row.SUMR) * otpFactor).toFixed(otpSumPrecision);
          var r = (row.SUMR * otpFactor).toFixed(otpSumPrecision);
          return r;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          // ESTIMATE
          // if (otpIsEstimate(rowData.ESTR, rowData.SUMR)) $(td).addClass('estimate');
        },
      },
      {
        targets: [colPerc],
        className: "text-right",
        type: "num-fmt",
        render: function (data, type, row, meta) {
          // ESTIMATE
          // var sumr = otpGetEstimate(row.ESTR, row.SUMR);
          var sumr = row.SUMR;
          return sumr > 0 ? (row.SUMA*100/sumr).toFixed(otpPercentagePrecision) + "%" : "N/A";
        },
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          // ESTIMATE
          // if (otpIsEstimate(rowData.ESTR, rowData.SUMR)) $(td).addClass('estimate');

          // ESTIMATE
          // var sumr = otpGetEstimate(rowData.ESTR, rowData.SUMR);
          var sumr = rowData.SUMR;
          var p = sumr > 0 ? (rowData.SUMA*100/sumr).toFixed(otpPercentagePrecision) : "N/A";
          $(td).addClass(otpGetColorClass(p));
        },
      },
    ],
    order: [[ colFA, "asc" ], [ colI, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      { data: "FNAME",
        title: 'Funding Agency',
        className: "title"
      },
      { data: "INAME",
        title: 'Institution',
        className: "title",
        visible: byInstitution,
        searchable: byInstitution
      },
      { data: "JAN",
        title: "Jan"
      },
      { data: "FEB",
        title: "Feb"
      },
      { data: "MAR",
        title: "Mar"
      },
      { data: "APR",
        title: "Apr"
      },
      { data: "MAY",
        title: "May"
      },
      { data: "JUN",
        title: "Jun"
      },
      { data: "JUL",
        title: "Jul"
      },
      { data: "AUG",
        title: "Aug"
      },
      { data: "SEP",
        title: "Sep"
      },
      { data: "OCT",
        title: "Oct"
      },
      { data: "NOV",
        title: "Nov"
      },
      { data: "DEC",
        title: "Dec"
      },
      { data: "SUMA",
        title: "Alloc"
      },
      { data: "SUMR",
        title: "Req"
      },
      { data: "ESTR",
        title: "Est",
        // ESTIMATE
        // visible: false,
        // searchable: false,
      },
      { data: "SUMR",
        title: "Alloc/Req"
      },
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // count number of FAs and Is
      var inst = 0;
      var fa = api
          .column( colFA, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            inst++;
            return a;
          }, 0 );
      $( api.column( colFA ).footer() ).html(fa ? fa.size : 0);
      $( api.column( colI ).footer() ).html(inst);

      // create sums of all numeric month columns
      var columns = [ 2,3,4,5,6,7,8,9,10,11,12,13 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        total = (total*otpMonthFactor).toFixed(otpSumPrecision);
        $( api.column( columns[i] ).footer() ).html(total);
      }

      var suma = api
          .column( colAlloc, { search:'applied' } )
          .data()
          .reduce( sum, 0 );
      $( api.column( colAlloc ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));

      var sumr = api
          .column( colReq, { search:'applied' } )
          .data()
          .reduce( sum, 0 );

      var estr = api
          .column( colEst, { search:'applied' } )
          .data()
          .reduce( sum, 0 );

      // ESTIMATE
      // var req = otpGetEstimate(estr, sumr);
      var req = sumr;
      $( api.column( colReq ).footer() ).html((req*otpFactor).toFixed(otpSumPrecision));
      $( api.column( colEst ).footer() ).html((estr*otpFactor).toFixed(otpSumPrecision));

      // update total percentage
      var colReqFooter = $( api.column( colReq ).footer() );
      var colEstFooter = $( api.column( colEst ).footer() );
      var colPercFooter = $( api.column( colPerc ).footer() );
      colReqFooter.removeClass();
      colPercFooter.removeClass();

      colReqFooter.addClass('text-right');
      colPercFooter.addClass('text-right');

      var p = req > 0 ? (suma * 100 / req).toFixed(otpPercentagePrecision) + "%" : "N/A";
      colPercFooter.html(p);
      colPercFooter.addClass(otpGetColorClass(p));

      // ESTIMATE
      // if (otpIsEstimate(estr, sumr)) {
      //   colReqFooter.addClass('estimate');
      //   colPercFooter.addClass('estimate');
      // }
      colEstFooter.addClass('estimate');
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });
  otpTable.push(table);

} );
