$(document).ready(function() {
  "use strict";
  otpInitBegin("Requirement");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        if ("task" in json) {
          $('#task').html(json.task);
        }
        if ("shadowTask" in json) {
          $('#shadowTask').html(json.shadowTask);
        }
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
    },
    order: [[ 7, "desc" ]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columnDefs: [
      {
        targets: 3,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? '<a href="https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#Ma0_Task_panel(N'+row.TASK_ID+')">'+data+'</a>' : data;
        }
      },
      { targets: 4,
        visible: false,
        searchable: false
      },
      { targets: [ 5, 6, 7],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(2);
        },
        className: "text-right",
        type: "num"
      }
    ],
    columns: [
      { data: "ACTIVITY" },
      { data: "WBS" },
      { data: "SYSTEM" },
      { data: "TASK" },
      { data: "TASK_ID", sortable: false },
      { data: "REQUIRED_FTE" },
      { data: "ALLOCATED_FTE" },
      { data: "DIFF_FTE" }
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function( a, b) {
        return intVal(a) + intVal(b);
      };

      // create sums of all numeric columns
      var columns = [ 5, 6, 7 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        $( api.column( columns[i] ).footer() ).html((total*otpFactor).toFixed(2));
      }

      // count number of tasks
      var c = 0;
      api.column( 1, { search:'applied' }  )
         .data()
         .reduce( function (a, b) {
              return c++;
         }, 0 );

      // Update footer
      $( api.column( 1 ).footer() ).html(c);
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  otpTable.push(table);
} );
