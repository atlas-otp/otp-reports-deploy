<?php

function otpDateId(string $dateString): string {
  $dateString = str_replace('"','',$dateString);
  $dateString = str_replace("'",'',$dateString);
  $date = date_create_from_format('d-M-y', $dateString);
  if (!$date) {
    $date = date_create_from_format('d-M-Y', $dateString);
  }
  if (!$date) {
    $date = date_create_from_format('d-m-y', $dateString);
  }
  if (!$date) {
    $date = date_create_from_format('d-m-Y', $dateString);
  }
  if (!$date) {
    return "197010101";
  }
  $quarter = floor((date_format($date, 'm')-1)/3) + 1;
  return date_format($date, 'Y'.$quarter.'md');
}

$date = date_create_from_format('j-M-Y', '15-Feb-2009');
echo date_format($date, 'Y-m-d');
echo '</br>';
echo otpDateId('06-Feb-2019');
echo '</br>';
echo otpDateId("'31-Mar-2019'");
echo '</br>';
echo otpDateId("'01-Apr-19'");
?>
