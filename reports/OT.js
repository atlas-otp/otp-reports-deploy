$(document).ready(function() {
  "use strict";
  otpInitBegin("OT");

  otpInitEnd();

  var colFA = 0;
  var colI = 1;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete: function (settings, json) {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18],
        render: function ( data, type, row, meta ) {
          return data !== 0 ? data : "";
        },
        className: "text-right",
        type: "num"
      },
    ],
    order: [[ colFA, "asc" ], [ colI, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      { data: "FNAME",
        title: 'Funding Agency',
        className: "title"
      },
      { data: "INAME",
        title: 'Institution',
        className: "title"
      },
      { data: "2009",
        title: "2009"
      },
      { data: "2010",
        title: "2010"
      },
      { data: "2011",
        title: "2011"
      },
      { data: "2012",
        title: "2012"
      },
      { data: "2013",
        title: "2013"
      },
      { data: "2014",
        title: "2014"
      },
      { data: "2015",
        title: "2015"
      },
      { data: "2016",
        title: "2016"
      },
      { data: "2017",
        title: "2017"
      },
      { data: "2018",
        title: "2018"
      },
      { data: "2019",
        title: "2019"
      },
      { data: "2020",
        title: "2020"
      },
      { data: "2021",
        title: "2021"
      },
      { data: "2022",
        title: "2022"
      },
      { data: "2023",
        title: "2023"
      },
      { data: "2024",
        title: "2024"
      },
      { data: "2025",
        title: "2025"
      },
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // count number of FAs and Is
      var inst = 0;
      var fa = api
          .column( colFA, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            inst++;
            return a;
          }, 0 );
      $( api.column( colFA ).footer() ).html(fa ? fa.size : 0);
      $( api.column( colI ).footer() ).html(inst);

      // create sums of all numeric month columns
      var columns = [ 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        total = (total).toFixed(otpSumPrecision);
        $( api.column( columns[i] ).footer() ).html(total);
      }
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });
  otpTable.push(table);

} );
