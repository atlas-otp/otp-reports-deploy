$(document).ready(function() {
  "use strict";
  otpInitBegin("Institution");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('funding', 'Funding Agencies');
  otpInitSelect('institution', 'Institutions');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);
  // otpInitCheck('show_months', true);

  otpInitEnd();

  // Colors from: http://graphicdesign.stackexchange.com/questions/3682/where-can-i-find-a-large-palette-set-of-contrasting-colors-for-coloring-many-d
  /*
  var chart = $('#graph').highcharts({
      chart: {
          colors: [
            '#ff0000', '#e4e400', '#00ff00', '#00ffff', '#b0b0ff', '#ff00ff',
            '#870000', '#878700', '#008700', '#008787', '#4949ff', '#870087',
            '#550000', '#545400', '#005500', '#005555', '#0000ff', '#550055',
            '#b00000', '#baba00', '#00b000', '#00b0b0', '#8484ff', '#b000b0',
          ],
          type: 'column'
      },
      xAxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
          plotLines: [{
              value: 0,
              width: 1,
              color: '#808080'
          }]
      },
      plotOptions: {
          column: {
              stacking: 'normal',
              lineColor: '#666666',
              lineWidth: 1,
              marker: {
                  lineWidth: 1,
                  lineColor: '#666666'
              }
          }
      },
      series: [
      ]
  });
*/
  var colI = 0;
  var colFirstName = 1;
  var colLastName = 2;
  var colActivity = 3;
  var colSystem = 4;
  var colTask = 5;
  var colAlloc = 18;

  var byTask = window.location.search.includes('byTask');

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function (settings, json) {
      // console.log("Complete");
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
/*
      var chart = $('#graph').highcharts();
      chart.title.update({ text: 'Allocations '+otpGetValue('funding') });
      chart.yAxis[0].setTitle({ text: 'Allocations ['+otpUnit+']' });
      chart.redraw();
*/
    },
    columnDefs: [
      {
        targets: [6,7,8,9,10,11,12,13,14,15,16,17],
        render: function ( data, type, row, meta ) {
          var d = (data*otpMonthFactor).toFixed(otpDetailPrecision);
          return d !== 0 ? d : "";
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [colAlloc],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num"
      },
    ],
    orderFixed: [[ colI, "asc" ]],
    order: [[ colLastName, "asc"], [ colActivity, "asc"], [ colSystem, "asc"], [ colTask, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columns: [
      { data: "INAME",
        title: 'Institutions',
        className: "title",
        sortable: false,
      },
      { data: "FNAME",
        title: 'First Name',
        className: "title",
      },
      { data: "LNAME",
        title: 'Last Name',
        className: "title",
      },
      { data: "ACTIVITY",
        title: 'Activity',
        className: "title",
        visible: byTask,
        searchable: byTask
      },
      { data: "SYSTEM",
        title: 'System',
        className: "title",
        visible: byTask,
        searchable: byTask
      },
      { data: "TASK",
        title: 'Task',
        className: "title",
        visible: byTask,
        searchable: byTask
      },

      { data: "JAN",
        title: "Jan",
        sortable: false,
      },
      { data: "FEB",
        title: "Feb",
        sortable: false,
      },
      { data: "MAR",
        title: "Mar",
        sortable: false,
      },
      { data: "APR",
        title: "Apr",
        sortable: false,
      },
      { data: "MAY",
        title: "May",
        sortable: false,
      },
      { data: "JUN",
        title: "Jun",
        sortable: false,
      },
      { data: "JUL",
        title: "Jul",
        sortable: false,
      },
      { data: "AUG",
        title: "Aug",
        sortable: false,
      },
      { data: "SEP",
        title: "Sep",
        sortable: false,
      },
      { data: "OCT",
        title: "Oct",
        sortable: false,
      },
      { data: "NOV",
        title: "Nov",
        sortable: false,
      },
      { data: "DEC",
        title: "Dec",
        sortable: false,
      },
      { data: "SUMA",
        title: "Alloc",
        sortable: false,
      },
    ],
    rowGroup: {
      endRender: function ( rows, group ) {
        // console.log('EndRender ' + group);
        // FIXME should move
        var doubleValue = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // FIXME should move
        var sum = function (a, b) {
          return doubleValue(a) + doubleValue(b);
        };

        var html = '';
        var colspan = byTask ? 5 : 2;
        html += '<td colspan="' + colspan + '">' + group + '<span class="rowCount-grid"><b>' + ' (' + rows.count() + ')' + '</b></span>' + '</td>';
        html += '<td>Sum:</td>';
        var columns = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC','SUMA'];
        var points = [];
        for (var i = 0; i < columns.length; i++) {
          var total = rows.data().pluck(columns[i]).reduce(sum, 0);

          let factor = i < 12 ? otpMonthFactor : otpFactor;
          html += '<td class="text-right">' + (total*factor).toFixed(otpSumPrecision) + '</td>';

          if (i < 12) {
            points[i] = Number(total);
          }
        }
/*
        var chart = $('#graph').highcharts();
        var series = chart.addSeries({ name: group, id: group }, false);
        series.setData(points, false);
        series.update({ 'tooltip': { valueSuffix: ' '+otpUnit }}, false);
*/
        return $('<tr/>').append(html);
      },
      dataSrc: 'INAME'
    },
    headerCallback: function ( row, data, start, end, display ) {
      // console.log('Header');
/*
      var chart = $('#graph').highcharts();
      while(chart.series.length > 0) {
        chart.series[0].remove(false);
      }
*/
    },
    footerCallback: function ( row, data, start, end, display ) {
      // console.log('Footer');
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // create sums of all numeric month columns
      var points = [];
      var columns = [ 6,7,8,9,10,11,12,13,14,15,16,17 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        total = (total*otpMonthFactor).toFixed(otpSumPrecision);
        points[i] = Number(total);
        $( api.column( columns[i] ).footer() ).html(total);
      }

      var suma = api
          .column( colAlloc, { search:'applied' } )
          .data()
          .reduce( function (a, b) {
              return doubleValue(a) + doubleValue(b);
          }, 0 );
      $( api.column( colAlloc ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header) {
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
} );
