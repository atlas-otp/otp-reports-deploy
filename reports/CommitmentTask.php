<!DOCTYPE html>

<!-- warning.html -->

<!--------------------------------------------------------------------------->
<!--                                                                       -->
<!-- WARNING, this file is generated, please edit the source and reconvert -->
<!--                                                                       -->
<!--                                                                       -->
<!--                                                                       -->
<!--------------------------------------------------------------------------->



<?php
error_reporting(E_ALL);
ini_set("display_errors", '1');
?>


<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>


<html lang="en">
    <head>
        <!-- header.html -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="OTP, Operation Task Planner">
<meta name="keywords" content="CERN, ATLAS, OTP">
<meta name="author" content="Mark Donszelmann">
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">

<title>
  ATLAS OTP &middot; OTP ATLAS
</title>

<!-- Favicons -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../ico/atlas-144x144.png">
                               <link rel="shortcut icon" href="../ico/atlas.ico">

<!-- jQuery -->
<!-- NOTE: no Ajax in 'slim' version of jquery -->
<!-- NOTE: jquery 3.x did not work -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- jQuery TimeAgo -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.3/jquery.timeago.min.js"></script>

<!-- jQuery Resizable -->
<!--
<script src="../jquery-resizable-0.28/dist/jquery-resizable.min.js"></script>
-->

<!-- Bootstrap -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.2.1/cerulean/bootstrap.min.css">
<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css">
-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >

<!-- Bootstrap Select -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.5/css/bootstrap-select.min.css">

<!-- Bootstrap DateRangePicker -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.3/daterangepicker.min.js"></script>

<!-- Datatables, jquery then bootstrap, but NOTE removal below -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/dataTables.bootstrap4.min.css" >
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
<!-- Removing container-fluid on class of dataTables_wrapper as it interferes with scrollResize -->
var DataTable = $.fn.dataTable;
$.extend( DataTable.ext.classes, {
    sWrapper:      "dataTables_wrapper dt-bootstrap4",
} );
</script>

<!-- Datatables Extensions -->
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.bootstrap4.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.5/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.37/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/rowgroup/1.0.3/css/rowGroup.dataTables.min.css">
<script src="https://cdn.datatables.net/rowgroup/1.0.3/js/dataTables.rowGroup.min.js"></script>

<!-- Does not work with scrollResize
<link rel="stylesheet" href="https://cdn.datatables.net/scroller/1.5.0/css/scroller.dataTables.min.css">
<script src="https://cdn.datatables.net/scroller/1.5.0/js/dataTables.scroller.min.js"></script>
-->

<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.4/css/fixedHeader.dataTables.min.css">
<script src="https://cdn.datatables.net/fixedheader/3.1.4/js/dataTables.fixedHeader.min.js"></script>

<!-- Datatables Plugins -->
<script src="https://cdn.datatables.net/plug-ins/1.10.19/features/scrollResize/dataTables.scrollResize.min.js"></script>

<!-- Highcharts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.2.0/modules/exporting.js"></script>

<!-- Search JS -->
<!--
Below are possibly these ?
<script src="https://cdnjs.cloudflare.com/ajax/libs/lunr.js/2.0.1/lunr.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="http://stevenlevithan.com/assets/misc/date.format.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.10/URI.min.js" type="text/javascript"></script>
-->
<!--
<script src="../js/search.min.js" type="text/javascript"></script>
-->
<script src="../js/lunr.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/mustache.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/date.format.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/URI.js" type="text/javascript" charset="utf-8"></script>
<script src="../js/jquery.lunr.search.js" type="text/javascript" charset="utf-8"></script>

<!-- ATLAS and OTP Stylesheets and JavaScript-->
<link rel="stylesheet" href="../css/atlas.css" >
<link rel="stylesheet" href="../css/otp.css" >
<script src="../js/otp.js" type="text/javascript"></script>




        <link rel="stylesheet" href="../css/otp-table.css" > <script type="text/javascript" class="init" src="../js/otp.js?random=<?php echo uniqid(); ?>"></script>

        
    </head>

    <body>
        
<div class="container-fluid">

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="../">ATLAS OTP Reports</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Reports
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../reports/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/TaskScheduleP1.php">Task Schedule Point1</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/FundingAgency.php">Funding Agency</a>
            <a class="dropdown-item" href="../reports/FundingAgency.php?byInstitution">Funding Agency by Institution</a>
            <a class="dropdown-item" href="../reports/Institution.php">Institution</a>
            <a class="dropdown-item" href="../reports/InstitutionYear.php">Institution by Year</a>
            <a class="dropdown-item" href="../reports/Task.php">Task</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/MultiYear.php">Multi-Year</a>
            <a class="dropdown-item" href="../reports/MultiYear.php?byInstitution">Multi-Year by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/Commitment.php">Commitment</a>
            <a class="dropdown-item" href="../reports/CommitmentFunding.php">Commitment Funding</a>
            <a class="dropdown-item" href="../reports/CommitmentFunding.php?byInstitution">Commitment Funding by Institution</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/PersonalSchedule.php">Personal Schedule</a>
            <a class="dropdown-item" href="../reports/TaskSchedule.php">Task Schedule</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/Occupancy.php">Occupancy</a>
            <a class="dropdown-item" href="../reports/Personal.php">Personal</a>
            <a class="dropdown-item" href="../reports/ShiftsByHour.php">Shifts by Hour (SLIMOS)</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../reports/TaskPersonnel.php">Task Personnel</a>
            <a class="dropdown-item" href="../reports/Allocation.php">Allocation</a>
            <a class="dropdown-item" href="../reports/Requirement.php">Requirement</a>
            <a class="dropdown-item" href="../reports/History.php">History</a>

          </div>
        </li>
<!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Plots
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../plots/index.php">Description*</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../plots/FTEbyActivity.php">FTEbyActivity</a>
            <a class="dropdown-item" href="../plots/FTEbySystem.php">FTEbySystem</a>
          </div>
        </li>
-->
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Checks
          </a>
          <div class="dropdown-menu">
          <!-- placement exactly under menu
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
          -->
            <a class="dropdown-item" href="../checks/index.php">Description</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../checks/CategoryUsage.php">Category Usage</a>
            <a class="dropdown-item" href="../checks/ConflictingShifts.php">Conflicting Shifts</a>
            <a class="dropdown-item" href="../checks/DoubleDesks.php">Double Desks</a>
            <a class="dropdown-item" href="../checks/OvervaluedShifts.php">Overvalued Shifts</a>
            <a class="dropdown-item" href="../checks/OnShift.php">On Shift</a>
          </div>
        </li>

<!--
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle disabled" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            Tests
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="../tests/GetClientAddress.php">Get Client Address</a>
            <a class="dropdown-item" href="../tests/PublicPhoneListRedirect.html">Public Phonelist Redirect</a>

            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../tests/BootstrapSelectTest.html">Bootstrap Select Test</a>
            <a class="dropdown-item" href="../tests/OtpBootstrapSelectTest.php">OTP Bootstrap Select Test</a>
            <a class="dropdown-item" href="../tests/ScrollTest.html">Scroll Test</a>
            <a class="dropdown-item" href="../tests/OtpScrollTest.php">OTP Scroll Test</a>

            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header btn-danger">Not Working</h6>
            <a class="dropdown-item" href="../tests/TrainingIndico.php">Training Indico</a>
            <a class="dropdown-item" href="../tests/UnicodeTest.php">Unicode Test</a>
          </div>
        </li>
-->
        <li class="nav-item">
          <a class="nav-link" href="../final_reports/index.php">Final Reports</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="https://otp-atlas.web.cern.ch">Website</a>
        </li>
      <!--
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li>
      -->
      </ul>
    <!--
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    -->
    </div>
  </nav>

  <script type="text/javascript" class="init">
  $(document).ready(function() {

    dateFormat = "DD-MMM-YY";

    parseIntOrDate = function ( intOrDate ) {
      var m = moment( intOrDate, dateFormat, true );
      if (m.isValid()) {
        intOrDate = m.format( 'x' );
      }
      return parseInt(intOrDate);
    }

    $.fn.dataTableExt.oSort['nullable-asc'] = function(a,b) {
        if (a === null)
            return 1;
        else if (b === null)
            return -1;
        else
        {
            var ia = parseIntOrDate(a);
            var ib = parseIntOrDate(b);
            return (ia<ib) ? -1 : ((ia > ib) ? 1 : 0);
        }
    }

    $.fn.dataTableExt.oSort['nullable-desc'] = function(a,b) {
        if (a === null)
          return 1;
        else if (b === null)
          return -1;
        else
        {
            var ia = parseIntOrDate(a);
            var ib = parseIntOrDate(b);
            return (ia>ib) ? -1 : ((ia < ib) ? 1 : 0);
        }
    }
  } );
</script>

<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>

<?php

// NOTE: defaults need to be set when including this file
$defaultYear = date("Y");   // current year
// $defaultStart = "01-Jan-" . date('y');  // start of current year
$defaultStart = date('d-M-Y');
$defaultEnd = "31-Dec-" . date('Y'); // end of current year
$defaultWeeks = "" != "" ? "" : 4;  /** @phpstan-ignore-line */
$defaultCategory = "Class 3" != "" ? "Class 3" : "Class 1";  /** @phpstan-ignore-line */
$defaultFunding = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultInstitution = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultSystem = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultActivity = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultType = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultRecognition = "" != "" ? "" : "Duty";  /** @phpstan-ignore-line */
$defaultTask = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultPerson = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultShadowTask = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultMinShadows = "" != "" ? "" : 1;  /** @phpstan-ignore-line */
$defaultTrainingTask = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultMinTrainings = "" != "" ? "" : 1;  /** @phpstan-ignore-line */
$defaultRequirement = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultName = "" != "" ? "" : 'First L.';  /** @phpstan-ignore-line */

$otpDebug = false;
$otpConnection = null;
$otpDatabase = '';
$otpStatementId = null;
$otpSql = '';
$otpShowSql = false;

function otpSetup(): void {
  global $otpDebug, $otpShowSql;

  if (isset($_REQUEST['debug'])) {
    $otpDebug = true;
  } else {
    $otpDebug = false;
  }

  if (isset($_REQUEST['sql'])) {
    $otpShowSql = true;
  } else {
    $otpShowSql = false;
  }

  error_reporting(E_ALL);
  ini_set('display_errors', '1');

  date_default_timezone_set("Europe/Zurich");
}

function otpGetRequestValue(string $key, string $defaultValue): string {
  // NOTE: Using isset() rather than empty(), to make sure 'category=' results in '' rather than 'All'
  return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $defaultValue;
}

function otpDate(string $dateString): DateTime|false {
  $dateString = str_replace('"','',$dateString);
  $dateString = str_replace("'",'',$dateString);
  $date = date_create_from_format('d-M-y', $dateString);    // 1-Dec-20 1-December-20 01-Dec-20 01-December-20
  if (!$date) {
    $date = date_create_from_format('d-M-Y', $dateString);  // 1-Dec-2020 1-December-2020 01-Dec-2020 01-December-2020
  }
  if (!$date) {
    $date = date_create_from_format('d-m-y', $dateString);  // 1-1-20 1-01-20 1-12-20
  }
  if (!$date) {
    $date = date_create_from_format('d-m-Y', $dateString);  // 1-1-2020 1-01-2020 1-12-2020
  }
  return $date;
}

function otpDateId(string $dateString): string {
  $date = otpDate($dateString);

  if (!$date) {
    return "197010101";
  }
  $quarter = floor((date_format($date, 'm')-1)/3) + 1;
  return date_format($date, 'Y'.$quarter.'md');
}

function otpDateString(string $dateString): string {
  $date = otpDate($dateString);

  if (!$date) {
    return "'1970-01-01'";
  }
  return "'" . date_format($date, 'Y-m-d') . "'";
}

function otpDateDt(string $dateString, bool $isMySql): string {
  $date = otpDateString($dateString);
  return $isMySql ? "str_to_date($date, '%Y-%m-%d')" : "to_date($date, 'YYYY-MM-DD')";
}

function otpParams(): void {
  global $defaultYear, $defaultStart, $defaultEnd, $defaultWeeks;
  global $defaultCategory, $defaultSystem, $defaultActivity;
  global $defaultType, $defaultRecognition;
  global $defaultFunding, $defaultInstitution;
  global $defaultPerson;
  global $defaultTask, $defaultShadowTask, $defaultTrainingTask;
  global $defaultMinShadows, $defaultMinTrainings;
  global $defaultRequirement;
  global $defaultName;

  global $year, $start, $end, $weeks, $now;
  global $startDt, $endDt, $nowDt;
  global $startId, $endId, $nowId;
  global $category, $system, $activity;
  global $type, $recognition;
  global $funding, $institution;
  global $person;
  global $task, $shadowTask, $trainingTask;
  global $minShadows, $minTrainings;
  global $requirement;
  global $count, $not_ok;

  global $sql_details, $isMySql;

  $isMySql = $sql_details['type'] == 'Mysql';

  // December
  global $DEC;
  $DEC = $isMySql ? '`DEC`' : '"DEC"';
  // NVL
  global $NVL;
  $NVL = $isMySql ? 'IFNULL' : 'NVL';
  // LOAD
  global $LOAD;
  $LOAD = $isMySql ? '`LOAD`' : '"LOAD"';
  // SYSTEM
  global $SYSTEM;
  $SYSTEM = $isMySql ? '`SYSTEM`' : '"SYSTEM"';
  // DESCRIPTION
  global $DESCRIPTION;
  $DESCRIPTION = $isMySql ? '`DESCRIPTION`' : '"DESCRIPTION"';
  // LISTAGG
  global $LISTAGG;
  $LISTAGG = $isMySql ? 'GROUP_CONCAT' : 'LISTAGG';

  $dateFormat = 'd-M-Y';
  $now = date($dateFormat);
  $weekInSeconds = 60 * 60 * 24 * 7;

  $year = otpGetRequestValue('year', $defaultYear);
  $defaultStart = "01-Jan-" . $year; // start of current year
  $defaultEnd = "01-Jan-" . (((int)$year)+1); // start of next year
  $start = otpGetRequestValue('start', $defaultStart);
  $end = otpGetRequestValue('end', $defaultEnd);
  $weeks = otpGetRequestValue('weeks', $defaultWeeks);
  $category = otpGetRequestValue('category', $defaultCategory);
  $funding = otpGetRequestValue('funding', $defaultFunding);
  $institution = otpGetRequestValue('institution', $defaultInstitution);
  $system = otpGetRequestValue('system', $defaultSystem);
  $activity = otpGetRequestValue('activity', $defaultActivity);
  $type = otpGetRequestValue('type', $defaultType);
  $recognition = otpGetRequestValue('recognition', $defaultRecognition);
  $person = otpGetRequestValue('person', $defaultPerson);
  $task = otpGetRequestValue('task', $defaultTask);
  $shadowTask = otpGetRequestValue('shadowTask', $defaultShadowTask);
  $minShadows = otpGetRequestValue('minShadows', $defaultMinShadows);
  $trainingTask = otpGetRequestValue('trainingTask', $defaultTrainingTask);
  $minTrainings = otpGetRequestValue('minTrainings', $defaultMinTrainings);
  $requirement = otpGetRequestValue('requirement', $defaultRequirement);
  $name = otpGetRequestValue('name', $defaultName);

  // FLAG
  if (isset($_REQUEST['commingWeeks'])) {
    $start = $now;
    $end = date($dateFormat, strtotime("now") + (((int)$weeks) * $weekInSeconds));
  }

  // date_id
  $startId = otpDateId($start);
  $endId = otpDateId($end);
  $nowId = otpDateId($now);

  // convert to DB
  $startDt = otpDateDt($start, $isMySql);
  $endDt = otpDateDt($end, $isMySql);
  $nowDt = otpDateDt($now, $isMySql);
}

include( "config.php" );
include ("editor/lib/DataTables.php");

use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate,
    DataTables\Editor\ValidateOptions,
    DataTables\Database,
       DataTables\Database\Query,
       DataTables\Database\Result;

function otpConnect(): void {
  global $sql_details, $db, $person;

  // Get logged in Person
  if ($person == 0) {
    if (in_array($_SERVER['SERVER_NAME'], ['localhost', 'otp-next.localhost', 'otp-www.localhost'])) {
      $user = "DUNS";
    } else {
      // Old SSO
      $user = getenv("OIDC_CLAIM_cern_upn");
      if (!$user) {
          // compatible with OKD4 SSO
          $user = $_SERVER['HTTP_X_FORWARDED_USER'];
      }

      if ($user) {
          $user = strtoupper($user);
      } else {
          $user = "";
      }
    }

    $sql = "select ID  from PUB_PERSON where USERNAME = '" . $user . "'";

    $result = $db->sql( $sql );

    if ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
      $person = $row['ID'];
    }
  }

}

function otpSqlStart(): void {
  global $otpSql, $db, $otpResult;

  $otpResult = $db->sql( $otpSql );
}

function otpToJson(): void {
  global $otpResult, $db, $otpSql;
  global $isMySql;

  global $year, $start, $end, $weeks, $now;
  global $startDt, $endDt, $nowDt;
  global $startId, $endId, $nowId;
  global $category, $system, $activity;
  global $type, $recognition;
  global $funding, $institution;
  global $person;
  global $task, $shadowTask, $trainingTask;
  global $minShadows, $minTrainings;
  global $requirement;
  global $name;
  global $count, $not_ok;

  // NOTE: php will have undefined values as not all times these are available
  echo "  \"year\": ", $year, ",\n";
  echo "  \"start\": \"", $start, "\",\n";
  echo "  \"start_dt\": \"", $startDt, "\",\n";
  echo "  \"start_id\": \"", $startId, "\",\n";
  echo "  \"end\": \"", $end, "\",\n";
  echo "  \"end_dt\": \"", $endDt, "\",\n";
  echo "  \"end_id\": \"", $endId, "\",\n";
  echo "  \"weeks\": ", $weeks, ",\n";
  echo "  \"now\": \"", $now, "\",\n";
  echo "  \"now_dt\": \"", $nowDt, "\",\n";
  echo "  \"now_id\": \"", $nowId, "\",\n";
  echo "  \"category\": \"", $category, "\",\n";
  echo "  \"system\": \"", $system, "\",\n";
  echo "  \"activity\": \"", $activity, "\",\n";
  echo "  \"type\": \"", $type, "\",\n";
  echo "  \"recognition\": \"", $recognition, "\",\n";
  echo "  \"funding\": \"", $funding, "\",\n";
  echo "  \"institution\": \"", $institution, "\",\n";
  echo "  \"task\": \"", $task, "\",\n";
  echo "  \"person\": \"", $person, "\",\n";
  echo "  \"shadowTask\": \"", $shadowTask, "\",\n";
  echo "  \"minShadows\": \"", $minShadows, "\",\n";
  echo "  \"trainingTask\": \"", $trainingTask, "\",\n";
  echo "  \"minTrainings\": \"", $minTrainings, "\",\n";
  echo "  \"requirement\": \"", $requirement, "\",\n";
  echo "  \"name\": \"", $name, "\",\n";
  echo "  \"is_mysql\": \"", $isMySql, "\",\n";
  // echo "  \"version\": \"", exec('git describe --tags --always'), "\",\n";
  echo "  \"data\": [\n";

  $first = true;

  $count = 0;
  $not_ok = 0;

  while ($row = $otpResult->fetch($fetchType=\PDO::FETCH_ASSOC)) {
    // see http://php.net/manual/en/function.json-encode.php, UTF-8-encode, otherwise we will loose names with accents
    array_walk_recursive($row, function(&$val) {
      $val = utf8_encode($val);
    });

    echo ($first ? "    " : ",\n    ").json_encode($row);
    $first = false;
    $count++;
    if (array_key_exists('OK', $row) && ($row['OK'] != 'Yes')) {
      $not_ok++;
    }
  }

  echo "\n  ],\n";
  echo "  \"count\": ", $count, ",\n";
  echo "  \"not_ok\": ", $not_ok, "\n";
}

function otpSqlEnd(): void {
}

function otpClose(): void {
  global $count, $not_ok;

  if (isset($_REQUEST['count'])) {
    exit($count);
  }
  if (isset($_REQUEST['not_ok'])) {
    exit($not_ok);
  }
}

function otpHeader(): void {
  global $otpDatabase, $otpShowSql;

  if ($otpShowSql) {
    header("Content-Type: text/plain");
  } else {
    header("Content-Type: application/json");
    header("Oracle-DB: ".$otpDatabase);
  }
}

function otpShowSql(): void {
  global $otpSql;

  echo $otpSql.";\n";
}

function otpQuote(string $string): string {
  // split by comma (separator) except if followed by whitespace (e.g. Yerevan,NRC KI, Protvino,NIKHEF => Yerevan; NRC KI, Protvino; NIKHEF), issue #118
  $string = "'" . preg_replace('/,(?!\s)/m', "','", $string) . "'";
  return $string;
}

function getLabel(string $name): string {
  if ($name == 'funding') return "Funding Agency";
  if ($name == 'name') return "Display Name";

  return ucwords($name);
}

function getOptions(string $name, string $sql = ''): void {
  global $db;

  if ($name == 'category') {
    echo "<option>Class 1</option>\n";
    echo "<option>Class 2</option>\n";
    echo "<option>Class 3</option>\n";
    echo "<option>Class 4</option>\n";
    echo "<option>Upgrade Construction</option>\n";
    return;
  }

  if ($name == 'year') {
    for ($year = 2025; $year >= 2010; $year--) {
      echo "<option>$year</option>\n";
    }
    return;
  }

  if ($name == 'type') {
    echo "<option>Expert</option>\n";
    echo "<option>Shifter</option>\n";
    return;
  }

  if ($name == 'recognition') {
    echo "<option>Duty</option>\n";
    echo "<option>Personal Duty</option>\n";
    echo "<option>Contract</option>\n";
    echo "<option>Not Operational Task</option>\n";
    return;
  }

  if ($name == 'unit') {
    echo "<option>Auto Units</option>\n";
    echo "<option>Shifts</option>\n";
    echo "<option>FTEs</option>\n";
    return;
  }

  if ($name == 'name') {
    echo "<option>Full Name</option>\n";
    echo "<option>F. Last</option>\n";
    echo "<option>First L.</option>\n";
    return;
  }

  $result = $db->sql( $sql );

  echo '  "'.$name.'": [';
  $oldGroup = "";
  $group = "";
  while ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
    $group = array_key_exists('GRP', $row) ? trim($row['GRP']) : "";
    if ($group != $oldGroup) {
      if ($oldGroup != "") {
        echo "</optgroup>\n";
      }
      echo "<optgroup label=\"".$group."\">\n";
      $oldGroup = $group;
    }
    // Inlined commas will have a space behind them, so they can be distinguished from comma separators
    $value = $row['NAME']; // str_replace(',','|',$row['NAME']);
    if (array_key_exists('ID', $row)) {
      echo "<option data-institution-id=\"".trim($row['ID'])."\">".trim($value)."</option>\n";
    } else {
      echo "<option>".trim($value)."</option>\n";
    }
  }
  if ($oldGroup != "") {
    echo "</optgroup>\n";
  }
}

function getButton(string $name): void {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
  echo "<button type=\"button\" class=\"btn btn-primary mb-2 mr-sm-2\" id=\"$name\">$label</button>\n";
  echo "</div>\n";
}

function getSingleSelect(string $name, string $sql = ''): void {
  getSelect($name, $sql, false);
}

function getSelect(string $name, string $sql = '', bool $multiple=true, bool $all=false): void {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\">$label</span>\n";
      echo "</div>\n";
      echo "<select ".($multiple ? "multiple=\"multiple\"" : "")." class=\"selectpicker\" id=\"$name\" data-width=\"fit\" title=\"Choose...\">\n";
        if ($all) {
          echo "<option disabled=\"disabled\">All</option>\n";
        }
        getOptions($name, $sql);
      echo "</select>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function getInput(string $name): void {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\" id=\"$name-label\">$label</span>\n";
      echo "</div>\n";
      echo "<input class=\"form-control\" id=\"$name\" placeholder=\"$label\"/>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function getCheck(string $name, bool $state): void {
  $label = getLabel($name);
  echo "<div class=\"form-check\">\n";
    echo "<input class=\"form-check-input\" type=\"checkbox\" value=\"" . ($state ? "true" : "false") . "\" id=\"$name\"" . ($state ? " checked=\"checked\"" : "") . ">\n";
    echo "<label class=\"form-check-label\" for=\"$name\">$label</label>\n";
  echo "</div>\n";
}

function otpReset(): void {
  getButton("Reset");
}

function otpSelectDateRange(): void {
  global $start, $end;
  $name = "dates";
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\" id=\"$name-label\">$label</span>\n";
      echo "</div>\n";
      echo "<input class=\"form-control\" id=\"$name\" type=\"text\" name=\"daterange\" value=\"$start - $end\"/>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function otpSelectTask(): void {
  getInput("task");
}

function otpSelectPerson(): void {
  getInput("person");
}

function otpSelectShadowTask(): void {
  getInput("shadowTask");
}

function otpSelectMinShadows(): void {
  getInput("minShadows");
}

function otpSelectTrainingTask(): void {
  getInput("trainingTask");
}

function otpSelectMinTrainings(): void {
  getInput("minTrainings");
}

function otpSelectRequirement(): void {
  getInput("requirement");
}


function otpSelectCategory(bool $single = false, bool $all = false): void {
  // select CODE as NAME from DOMAIN_LISTS where TYP_CODE='Category' and ALLOW_SELECT='Y' order by ORDER_IN_TYPE
  getSelect('category', '', !$single, $all);
}

function otpSelectYear(bool $single = false, bool $all = false): void {
  getSelect('year', '', !$single, $all);
}

function otpSelectSystem(bool $single = false, bool $all = false): void {
  getSelect('system', "select TITLE as NAME from PUB_SYSTEM_NODE order by TITLE", !$single, $all);
}

function otpSelectActivity(bool $single = false, bool $all = false): void {
  getSelect('activity', "select TITLE as NAME from PUB_WBS_NODE where PARENT_ID is null order by TITLE", !$single, $all);
}

function otpSelectType(bool $single = false, bool $all = false): void {
  getSelect('type', '', !$single, $all);
}

function otpSelectRecognition(bool $single = false, bool $all = false): void {
  getSelect('recognition', '', !$single, $all);
}

function otpSelectUnit(bool $single = false, bool $all = false): void {
  getSelect('unit', '', !$single, $all);
}

function otpSelectFunding(bool $single = false, bool $all = false): void {
  getSelect('funding', "select distinct F.NAME as NAME from PUB_FUNDING_AGENCY F join PUB_INSTITUTE I on I.FUNDA_ID = F.ID order by F.NAME", !$single, $all);
}

function otpSelectInstitution(bool $single = false, bool $all = false): void {
  getSelect('institution', "select F.NAME as GRP, ONAME as NAME, I.ID as ID from PUB_INSTITUTE I join PUB_FUNDING_AGENCY F on F.ID = I.FUNDA_ID  where I.ID < 2000 or I.ID >= 3000 order by F.NAME, ONAME", !$single, $all);
}

function otpSelectInstitutionAndICs(bool $single = false, bool $all = false): void {
  getSelect('institution', "select F.NAME as GRP, ONAME as NAME, I.ID as ID from PUB_INSTITUTE I join PUB_FUNDING_AGENCY F on F.ID = I.FUNDA_ID order by F.NAME, ONAME", !$single, $all);
}

function otpSelectName(): void {
  getSelect("name", '', false, false);
}

function otpShowMonths(bool $state = false): void {
  getCheck("show_months", $state);
}


global $otpResult, $db, $otpSql;
global $isMySql;

global $year, $start, $end, $weeks, $now;
global $startDt, $endDt, $nowDt;
global $startId, $endId, $nowId;
global $category, $system, $activity;
global $type, $recognition;
global $funding, $institution;
global $person;
global $task, $shadowTask, $trainingTask;
global $minShadows, $minTrainings;
global $requirement;

?>

<script type="text/javascript">
  function otpGetDefault(name) {
    "use strict";
    switch(name) {
      case 'category': return "<?php echo $defaultCategory; ?>";
      case 'year': return "<?php echo $defaultYear; ?>";
      case 'start': return "<?php echo $defaultStart; ?>";
      case 'end': return "<?php echo $defaultEnd; ?>";
      case 'system': return "<?php echo $defaultSystem; ?>";
      case 'activity': return "<?php echo $defaultActivity; ?>";
      case 'type': return "<?php echo $defaultType; ?>";
      case 'recognition': return "<?php echo $defaultRecognition; ?>";
      case 'funding': return "<?php echo $defaultFunding; ?>";
      case 'institution': return "<?php echo $defaultInstitution; ?>";
      case 'unit': return "Auto Units";
      case 'task': return "<?php echo $defaultTask; ?>";
      case 'person': return "<?php echo $defaultPerson; ?>";
      case 'shadowTask': return "<?php echo $defaultShadowTask; ?>";
      case 'minShadows': return "<?php echo $defaultMinShadows; ?>";
      case 'trainingTask': return "<?php echo $defaultTrainingTask; ?>";
      case 'minTrainings': return "<?php echo $defaultMinTrainings; ?>";
      case 'requirement': return "<?php echo $defaultRequirement; ?>";
      case 'name': return "<?php echo $defaultName; ?>";
      case 'show_months': return "<?php echo "false"; ?>";
      default:
        return '';
    }
  }
</script>

<?php
otpSetup();
otpConnect();
?>

<div class="card">
  <!--
<div class="card-header">
  <div class="card-title">
    <span class="selectionName"></span>
  </div>
</div>
-->


  <form class="form-inline">
    <?php
      otpSelectYear(true);
      otpSelectCategory();
      otpSelectFunding();
      otpSelectInstitutionAndICs();
      otpSelectSystem();
      otpSelectActivity();
      otpSelectUnit(true);
    ?>
  </form>
</div>

<div class="card panel-default">
  <div class="card-header">
    <div class="card-title">
      <span class="reportName"></span>
    </div>
  </div>
</div>


<div class="row content">
  <table id="table" class="table table-striped table-condenced table-hover table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th class="title">System</th>
        <th class="title">Activity</th>
        <th class="title">Task Id</th>
        <th class="title">Task</th>

        <th class="title">Funding Agency</th>
        <th class="title">Institution Id</th>
        <th class="title">Institution</th>
        <th class="title">Name</th>
        <th class="title">Description</th>

        <th class="title">Committed [<span class="unit"></span>]</th>
        <th class="title">Allocated [<span class="unit"></span>]</th>
        <th class="title">Task Requirement [<span class="unit"></span>]</th>
        <th class="title">Committed Fraction of Requirement [%]</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th class="title" style="text-align:right">Total Commitments:</th>
        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"></th>

        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"><span class="unit"></span>:</th>

        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"></th>
        <th class="title" style="text-align:right"></th>
      </tr>
    </tfoot>
  </table>
</div>

<div class="card">
  <div class="card-header">
  <div class="card-title">
    <span class="notesName">Notes</span>
  </div>
</div>


  <ul class="notes">
    <li>Before January 2023 Class 1 shifts are valued 1.31 for weekend and night shifts, 0.66 at other times.</li>
<li>From January 2023 Class 1 shifts are valued 1.80 for night shifts in the weekend, 1.20 for other weekend shifts and night shifts during the week and 0.60 at other times.</li>

    <li>Requirements are counted only once in the sum, they may be listed for both Institutes and IC Clusters.</li>
    <li>To print table use PDF or Print button rather the menu of the browser.</li>
  </ul>
</div>

<script type="text/javascript" class="init" src="CommitmentTask.js"></script>


<?php
otpClose();
?>



  <div class="row footer navbar navbar-expand-lg navbar-dark bg-dark">
      <ul class="nav navbar-nav mr-auto">
          <li class="nav-item footer">
            <a class="nav-link" href="https://gitlab.cern.ch/atlas-otp/otp-reports/pipelines">
              Version: 5.6.1-4-g23423ae

            </a>
          </li>
          <?php if (str_contains($_SERVER['SERVER_NAME'], '-alpha')) { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-test.web.cern.ch">Goto Test Reports</a></li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas.web.cern.ch">Goto Production Reports</a></li>
          <?php } elseif (str_contains($_SERVER['SERVER_NAME'], '-test')) { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-alpha.web.cern.ch">Goto Staged Reports</a></li>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas.web.cern.ch">Goto Production Reports</a></li>
          <?php } else { ?>
          <li class="nav-item footer"><a class="nav-link" href="https://otp-reports-atlas-alpha.web.cern.ch">Goto Staged Reports</a></li>
          <?php } ?>
      </ul>

      <ul class="nav navbar-nav">
          <li class="nav-item footer">Copyright© 2010-2025 CERN</li>
          <li class="nav-item">
            <a class="nav-link mail" href="mailto:Mark.Donszelmann@cern.ch?subject=ATLAS OTP Reports">
              <i class="fa fa-envelope"></i>&nbsp;ATLAS OTP
            </a>
          </li>
      </ul>
  </div>
</div>


        <!-- footer.html -->


        
        
    </body>
</html>
