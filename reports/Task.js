$(document).ready(function() {
  "use strict";
  otpInitBegin("Task");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
//    otpInitSelect('funding', 'Funding Agencies');
//    otpInitSelect('institution', 'Institutions');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colSystem = 0;
  var colActivity = 1;
  var colTID = 2;
  var colTask = 3;
  var colAlloc = 28;
  var colReq = 29;
  var colPerc = 30;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete: function(settings, json) {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: [4, 6,8,10,12,14,16,18,20,22,24,26],
        visible: false,
        searchable: false,
      },
      {
        targets: [5,7,9,11,13,15,17,19,21,23,25,27],
        render: function ( data, type, row, meta ) {
          var d = (data*otpMonthFactor).toFixed(otpDetailPrecision);
          return d !== 0 ? d : "";
        },
        className: "text-right",
        type: "num",
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          // NOTE: rowData is an Object, NOT an Array
          var aIndex = '';
          var rIndex = '';
          switch(col) {
            case 5:
              aIndex = 'JAN_A';
              rIndex = 'JAN_R';
              break;
            case 7:
              aIndex = 'FEB_A';
              rIndex = 'FEB_R';
              break;
            case 9:
              aIndex = 'MAR_A';
              rIndex = 'MAR_R';
              break;
            case 11:
              aIndex = 'APR_A';
              rIndex = 'APR_R';
              break;
            case 13:
              aIndex = 'MAY_A';
              rIndex = 'MAY_R';
              break;
            case 15:
              aIndex = 'JUN_A';
              rIndex = 'JUN_R';
              break;
            case 17:
              aIndex = 'JUL_A';
              rIndex = 'JUL_R';
              break;
            case 19:
              aIndex = 'AUG_A';
              rIndex = 'AUG_R';
              break;
            case 21:
              aIndex = 'SEP_A';
              rIndex = 'SEP_R';
              break;
            case 23:
              aIndex = 'OCT_A';
              rIndex = 'OCT_R';
              break;
            case 25:
              aIndex = 'NOV_A';
              rIndex = 'NOV_R';
              break;
            case 27:
              aIndex = 'DEC_A';
              rIndex = 'DEC_R';
              break;
            default:
              aIndex = col;
              rIndex = col;
              break;
          }
          var a = rowData[aIndex];
          var r = rowData[rIndex];
          if (r > 0) {
            $(td).addClass(otpGetColorClass(a*100/r));
          }
        },
      },
      {
        targets: [colAlloc, colReq],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [colPerc],
        className: "text-right",
        type: "num-fmt",
        render: function (data, type, row, meta) {
          var sumr = row.SUMR;
          return sumr > 0 ? (row.SUMA*100/sumr).toFixed(otpPercentagePrecision) + "%" : "N/A";
        },
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          var sumr = rowData.SUMR;
          var p = sumr > 0 ? (rowData.SUMA*100/sumr).toFixed(otpPercentagePrecision) : "N/A";
          $(td).addClass(otpGetColorClass(p));
        },
      },
    ],
    order: [[ colSystem, "asc" ], [ colActivity, "asc"], [ colTID, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      { data: "SYSTEM",
        title: 'System',
        className: "title",
      },
      { data: "ACTIVITY",
        title: 'Activity',
        className: "title",
      },
      { data: "TID",
        title: "Task Id",
        className: "title",
      },
      { data: "TASK",
        title: "Task",
        className: "title",
      },
      { data: "JAN_R" },
      { data: "JAN_A",
        title: "Jan"
      },
      { data: "FEB_R" },
      { data: "FEB_A",
        title: "Feb"
      },
      { data: "MAR_R" },
      { data: "MAR_A",
        title: "Mar"
      },
      { data: "APR_R" },
      { data: "APR_A",
        title: "Apr"
      },
      { data: "MAY_R" },
      { data: "MAY_A",
        title: "May"
      },
      { data: "JUN_R" },
      { data: "JUN_A",
        title: "Jun"
      },
      { data: "JUL_R" },
      { data: "JUL_A",
        title: "Jul"
      },
      { data: "AUG_R" },
      { data: "AUG_A",
        title: "Aug"
      },
      { data: "SEP_R" },
      { data: "SEP_A",
        title: "Sep"
      },
      { data: "OCT_R" },
      { data: "OCT_A",
        title: "Oct"
      },
      { data: "NOV_R" },
      { data: "NOV_A",
        title: "Nov"
      },
      { data: "DEC_R" },
      { data: "DEC_A",
        title: "Dec"
      },
      { data: "SUMA",
        title: "Alloc"
      },
      { data: "SUMR",
        title: "Req"
      },
      { data: "SUMR",
        title: "Alloc/Req"
      },
    ],
    footerCallback: function ( row, data, start, end, display ) {
//        console.log("Footer "+start+" "+end);
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // count number of Tasks
      var tid = 0;
      var fa = api
          .column( colTID, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            tid++;
            return a;
          }, 0 );
      $( api.column( colTID ).footer() ).html(tid);

      // create sums of all numeric month columns
      var columns = [ 5,7,9,11,13,15,17,19,21,23,25,27 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        total = (total*otpMonthFactor).toFixed(otpSumPrecision);
        $( api.column( columns[i] ).footer() ).html(total);
      }

      var suma = api
          .column( colAlloc, { search:'applied' } )
          .data()
          .reduce( function (a, b) {
              return doubleValue(a) + doubleValue(b);
          }, 0 );
      $( api.column( colAlloc ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));

      var sumr = api
          .column( colReq, { search:'applied' } )
          .data()
          .reduce( function (a, b) {
              return doubleValue(a) + doubleValue(b);
          }, 0 );

      $( api.column( colReq ).footer() ).html((sumr*otpFactor).toFixed(otpSumPrecision));

      // update total percentage
      var p = sumr > 0 ? (suma * 100 / sumr).toFixed(otpPercentagePrecision) + "%" : "N/A";
      var footer = $( api.column( colPerc ).footer() );
      footer.html(p);
      footer.addClass(otpGetColorClass(p));
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
});
