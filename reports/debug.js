$(document).ready(function() {
    "use strict";

    var table = $('#table').DataTable({
      "ajax": {
        "url": "./debug.json",
        "type": "POST",
        "dataSrc": ""
      },
      "paging": false,
      "info": false,
      // "scrollY": calcDataTableHeight(),
      "scrollCollapse": true,
      "columns": [
        { "data": "id" },
        { "data": "name" },
        { "data": "report" },
        { "data": "data" },
        { "data": "sql" },
        { "data": "speed" },
      ],
      "columnDefs": [
        {
          "targets": [2,3,4],
          "render": function ( data, type, row, meta ) {
            if (type == "sort") {
              return data;
            }
            return data == undefined || data == "" ? "" : "<a href=\"" + data + "\">" + row.name + "</a>";
          }
        },
      ]
    });

    otpTable.push(table);
  });
