$(document).ready(function() {
  "use strict";
  otpInitBegin("History");

  otpInitDateRange();
  otpInitInput("task");
  otpInitInput("requirement");

  otpInitEnd();

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        if ("task" in json) {
          $('#task').html(json.task);
        }
        if ("taskId" in json) {
          $('#taskId').html(json.taskId);
        }
        if ("requirement" in json) {
          $('#requirement').html(json.requirement);
        }
        if ("requirementId" in json) {
          $('#requirementId').html(json.requirementId);
        }
        return json.data;
      }
    },
    language: otpGetLanguage(30),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
    },
    order: [[ 1, "asc"], [ 4, "asc" ]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columnDefs: [
      {
        targets: 7,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? row.FNAME+' '+data : data;
        }
      },
      {
        targets: 8,
        render: function ( data, type, row, meta ) {
            var a = data.split(' ').map(Number);
            var z = '';
            var clz = row.TDELETE === 'Y' ? "badge badge-danger" : "badge badge-info";
            for (var i = 0; i < a.length; i++) {
              // Keep spacing and minus for csv export
              z += a[i] > 0 ? '<span class="'+clz+'">'+(row.TDELETE === 'Y' ? ' -' : ' ')+(i+1)+'</span>' : ' ';
            }
            return type === 'display' ? z : data;
        }
      },
      {
        targets: 9,
        render: function ( data, type, row, meta ) {
            var y = data.substring(0,4);
            var m = data.substring(4,7);
            var month = [];
            month[0] = "January";
            month[1] = "February";
            month[2] = "March";
            month[3] = "April";
            month[4] = "May";
            month[5] = "June";
            month[6] = "July";
            month[7] = "August";
            month[8] = "September";
            month[9] = "October";
            month[10] = "November";
            month[11] = "December";
            var n = month[Number(m)-1];
            return type === 'display' || type === 'filter' ? n + ' ' + y : data;
        }
      },
      {
        targets: 12,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? row.HOUR_FROM+'-'+row.HOUR_TO+' '+data : data;
        }
      },
    ],
    columns: [
      { data: "TSTART", type: "nullable" },

      { data: "TWORKID" },
      { data: "AUTHOR_FNAME", visible: false },
      { data: "AUTHOR_LNAME", visible: false },

      { data: "TID" },
      { data: "TDELETE", visible: false, searchable: false },

      { data: "FNAME", visible: false },
      { data: "LNAME" },

      { data: "LOAD", searchable: false, sortable: false },
      { data: "MONTH_CODE" },
      { data: "HOUR_FROM", visible: false, searchable: false },
      { data: "HOUR_TO", visible: false, searchable: false },
      { data: "DESCRIPTION", searchable: false },
    ],
    orderFixed: {
      post: [[ 1, 'asc' ], [ 4, 'asc' ]]
    },
    drawCallback: function ( settings ) {
        var api = this.api();
        var rows = api.rows( {search:'applied'} ).nodes();
        var data = api.rows( {search:'applied'} ).data();
        var last = null;
        var count = 0;

        api.column(1, {search:'applied'} ).data().each( function ( tid, i ) {
            if ( last !== tid ) {
                $(rows).eq( i ).before(
                    '<tr class="group"><td colspan="7"><b>Transaction: '+tid+' by '+
                    data[i].AUTHOR_FNAME+' '+data[i].AUTHOR_LNAME+
                    '</b></td></tr>'
                );

                count++;
                last = tid;
            }
        });

        // Update footer
        $( api.column( 1 ).footer() ).html(count);
    },
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
          return intVal(a) + intVal(b);
      };

      // create sums of all numeric columns
      var columns = [ ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        $( api.column( columns[i] ).footer() ).html(total);
      }

      // count number of ids
      var c = 0;
      api.column( 1, { search:'applied' } )
         .data()
         .reduce( function (a, b) {
              return c++;
         }, 0 );

      // Update footer
      $( api.column( 4 ).footer() ).html(c);
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  otpTable.push(table);
} );
