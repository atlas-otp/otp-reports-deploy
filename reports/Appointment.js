$(document).ready(function() {
  "use strict";
  otpInitBegin("Appointment");

  otpInitSelect('year', 'Years');

  otpInitEnd();

  var institutes;
  var fteData;

  var colTaskId = 0;
  var colTask = 1;

  var colPersonId = 2;
  var colLastName = 3;
  var colFirstName = 4;

  // var colReq = 5;
  var colAlloc = 5;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(10),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
    },
    order: [[ colTaskId, "asc" ], [ colLastName, "asc"], [ colFirstName, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columnDefs: [
      {
        targets: colTaskId,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? '<a href="https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#Ma0_Task_panel(N'+row.TASK_ID+')">'+data+'</a>' : data;
        }
      },
      { targets: [ colAlloc ],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(2);
        },
        className: "text-right",
        type: "num"
      },
    ],
    columns: [
      { data: "TASK_ID" },
      { data: "TASK" },
      { data: "PERSON_ID" },
      { data: "LNAME" },
      { data: "FNAME" },
      // { data: "REQUIRED_FTE"},
      { data: "ALLOCATED_FTE"}
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function( a, b) {
        return intVal(a) + intVal(b);
      };

      // Sum Allocations
      // var total_required = api
      //     .cells( function ( index, data, node ) {
      //         return true;
      //     }, colAlloc, { search:'applied' } )
      //     .data()
      //     .reduce( sum, 0 );
      // $( api.column( colReq ).footer() ).html(Number(total_required*otpFactor).toFixed(2));

      var total_allocated = api
          .cells( function ( index, data, node ) {
              return true;
          }, colAlloc, { search:'applied' } )
          .data()
          .reduce( sum, 0 );
      $( api.column( colAlloc ).footer() ).html(Number(total_allocated*otpFactor).toFixed(2));

      // count number of tasks
      var c = 0;
      api.column( colTask, { search:'applied' }  )
         .data()
         .reduce( function (a, b) {
              return c++;
         }, 0 );

      // Update footer
      $( api.column( colTask ).footer() ).html(c);
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  otpTable.push(table);
} );
