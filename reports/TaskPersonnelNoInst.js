// NOTE: same as TaskPersonnel but without Institutes
$(document).ready(function() {
  "use strict";
  otpInitBegin("TaskPersonnelNoInst");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colGroup = 0;
  var colTID = 1;
  var colTask = 2;
  var colRID = 3;
  var colSubtask = 4;
  var colPID = 5;
  var colLastName = 6;
  var colFirstName = 7;
  var colFTE = 20;
  var colFTElastYear = 21;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete: function(settings, json) {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: [8,9,10,11,12,13,14,15,16,17,18,19],
        render: function ( data, type, row, meta ) {
          var d = (data*otpMonthFactor).toFixed(otpDetailPrecision);
          return d !== 0 ? d : "";
        },
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          if (rowData.PID == 0) {
            $(td).addClass("title");
          }
        },
        className: "text-right",
        type: "num",
      },
      {
        targets: [colFTE, colFTElastYear],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          if (rowData.PID == 0) {
            $(td).addClass("title");
          }
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [0, 1, 2, 3, 4, 5, 6, 7],
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          if (rowData.PID == 0) {
            if (col == 9) {
              $(td).html("Requirement:");
            }
            $(td).removeClass("title");
          }
        },
      }
    ],
    order: [[ colGroup, "asc" ], [ colTID, "asc"], [ colRID, "asc"], [ colLastName, "asc"], [ colFirstName, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      { data: "GRP",
        title: "Task Group",
        className: "title",
        visible: true,
      },
      { data: "TID",
        title: "Task ID",
        className: "title",
        visible: true,
      },
      { data: "TASK",
        title: "Task Name",
        className: "title",
      },
      { data: "RID",
        title: "Subtask ID",
        className: "title",
        visible: true,
      },
      { data: "REQ",
        title: "Subtask Name",
        className: "title",
      },
      { data: "PID",
        title: "Person ID",
        className: "title",
        visible: true,
      },
      { data: "LNAME",
        title: "Last Name",
        className: "title",
        render: function ( data, type, row, meta ) {
          // sort Requirements as last
          return (type == 'sort') && (row.PID == 0) ? "Zzzzzzzz" : data;
        },
      },
      { data: "FNAME",
        title: "First Name",
        className: "title",
      },
      { data: "JAN",
        title: "Jan"
      },
      { data: "FEB",
        title: "Feb"
      },
      { data: "MAR",
        title: "Mar"
      },
      { data: "APR",
        title: "Apr"
      },
      { data: "MAY",
        title: "May"
      },
      { data: "JUN",
        title: "Jun"
      },
      { data: "JUL",
        title: "Jul"
      },
      { data: "AUG",
        title: "Aug"
      },
      { data: "SEP",
        title: "Sep"
      },
      { data: "OCT",
        title: "Oct"
      },
      { data: "NOV",
        title: "Nov"
      },
      { data: "DEC",
        title: "Dec"
      },
      { data: "FTE",
        title: "Alloc"
      },
      { data: "FTE_PREV",
        title: "Prev. Year"
      },
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // console.log("Footer "+start+" "+end);
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      var tid = api
          .column( colTID, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            return a;
          }, 0 );
      $( api.column( colTID ).footer() ).html(tid.size);

      var rid = api
          .column( colRID, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            return a;
          }, 0 );
      $( api.column( colRID ).footer() ).html(rid.size);

      var pid = api
          .column( colPID, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            return a;
          }, 0 );
      $( api.column( colPID ).footer() ).html(pid.size);

      var column_add = function (a, b) {
        // if (i == 0) {
        //     console.log(a);
        //     console.log(b);
        // }

        if (Array.isArray(a)) {
            // second b has data, a is now ColPID
            var sum = 0.0;
            for (var idx=0; idx<a.length; idx++) {
                if (parseInt(a[idx]) != 0) {
                    sum += doubleValue(b[idx]);
                }
            }
            return sum;
        } else {
            // first b, ColPID is delivered, data not yet
            return b;
        }
      }

      // create sums of all numeric month columns
      var columns = [ 8,9,10,11,12,13,14,15,16,17,18,19 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
          .columns( [ colPID, columns[i] ], { search:'applied' } )
          .data()
          .reduce(column_add, 0);

        // Update footer
        total = (total*otpMonthFactor).toFixed(otpSumPrecision);
        $( api.column( columns[i] ).footer() ).html(total);
      }

      var suma = api
          .columns( [ colPID, colFTE ], { search:'applied' } )
          .data()
          .reduce(column_add, 0);
      $( api.column( colFTE ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));

      var sumap = api
          .columns( [ colPID, colFTElastYear], { search:'applied' } )
          .data()
          .reduce(column_add, 0);
  $( api.column( colFTElastYear ).footer() ).html((sumap*otpFactor).toFixed(otpSumPrecision));
    }
  } );

  // fix to make sure "Loading..." message re-appears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
});
