$(document).ready(function() {
  "use strict";
  otpInitBegin("CommitmentFunding");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colFA = 0;
  var colI = 1;
  var colIID = 2;
  var colLNAME = 3;

  var colAlloc = 4;
  var colReq = 5;
  var colPerc = 6;

  var colIc = 7;
  var colIcSrc = 8;
  var colIcSum = 9;

  var colIcReq = 10;

  var colAllocIc = 11;
  var colReqIc= 12;

  var byInstitution = window.location.search.includes('byInstitution');

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        var rows = {};
        // Find rows for each ID
        for (var r1=0; r1 < json.data.length; r1++) {
          rows[json.data[r1].IID] = r1;
        }

        // evenly spread the IC Commitments over its institutes
        for (var r2=0; r2 < json.data.length; r2++) {
          if (json.data[r2].INAME != null && json.data[r2].INAME.startsWith("IC: ")) {
            var ids = json.data[r2].LNAME.split(",");
            var val = json.data[r2].COMMITTED_FTE / ids.length;
            var iname = '';
            for (var i=0; i<ids.length; i++) {
              var iid = parseInt(ids[i]);
              if (iid in rows) {
                json.data[rows[iid]].COMMITTED_FTE = Number(json.data[rows[iid]].COMMITTED_FTE) + val;
                json.data[rows[iid]].COMMITTED_FTE_SUM = Number(json.data[rows[iid]].COMMITTED_FTE_SUM) + val;
                iname += (iname != '' ? ", " : "") + json.data[rows[iid]].INAME;
              } else {
                console.log("IID " + iid + " no longer exists");
              }
            }
            // set the IC commitments in the sum column to zero to avoid double counting
            json.data[r2].COMMITTED_FTE_SUM = 0;
            json.data[r2].INAME += ": " + iname;
          }
        }
        return json.data;
      }
    },
    language: otpGetLanguage(30),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete: function (settings, json) {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: [colI],
        createdCell: function (td, cellData, rowData, row, col) {
          if ((cellData != null) && (cellData.startsWith("IC: "))) {
            $(td).addClass("ic");
          }
        },
      },
      {
        targets: [colAlloc, colReq, colIc, colIcSrc, colIcSum],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num"
      },
      {
        targets: [colPerc],
        className: "text-right",
        type: "num",
        render: function (data, type, row, meta) {
          var result = otpPercentage(row.ALLOCATED_FTE, row.REQUIRED_FTE);
          return (type == "display") ? result < 0 ? "N/A" : result.toFixed(otpPercentagePrecision) : result;
        },
        createdCell: function (td, cellData, rowData, row, col) {
          // NOTE: not called for PDF or Excel
          var result = otpPercentage(rowData.ALLOCATED_FTE, rowData.REQUIRED_FTE);
          $(td).addClass(otpGetColorClass(result));
        },
      },
      {
        targets: [colIcReq],
        className: "text-right",
        type: "num",
        render: function (data, type, row, meta) {
          var result = otpPercentage(row.COMMITTED_FTE, row.REQUIRED_FTE);
          result = row.INAME != null && row.INAME.startsWith("IC: ") ? -1 : result;
          return (type == "display") ? result < 0 ? "N/A" : result.toFixed(otpPercentagePrecision) : result;
        },
      },
      {
        targets: [colAllocIc],
        className: "text-right",
        type: "num",
        render: function (data, type, row, meta) {
          var result = otpPercentage(row.ALLOCATED_FTE, row.COMMITTED_FTE);
          result = row.INAME != null && row.INAME.startsWith("IC: ") ? -1 : result;
          return (type == "display") ? result < 0 ? "N/A" : result.toFixed(otpPercentagePrecision) : result;
        },
      },
      {
        targets: [colReqIc],
        className: "text-right",
        type: "num",
        render: function (data, type, row, meta) {
          var result = otpPercentage(row.REQUIRED_FTE, row.COMMITTED_FTE);
          result = row.INAME != null && row.INAME.startsWith("IC: ") ? -1 : result;
          return (type == "display") ? result < 0 ? "N/A" : result.toFixed(otpPercentagePrecision) : result;
        },
      }
    ],
    order: [[ colFA, "asc" ], [ colI, "asc"]],
    paging: false,
    info: false,
    scrollY: 100,
    scrollX: true,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      { data: "FNAME",
        title: 'Funding Agency',
        className: "title"
      },
      { data: "INAME",
        title: 'Institution',
        className: "title",
        visible: byInstitution,
        searchable: byInstitution
      },
      { data: "IID",
        visible: false,
        searchable: false,
      },
      { data: "LNAME",
        visible: false,
        searchable: false,
      },

      { data: "ALLOCATED_FTE",
      },
      { data: "REQUIRED_FTE",
      },
      { data: null,
      },

      { data: "COMMITTED_FTE",
      },
      { data: "COMMITTED_FTE_SRC",
        visible: false,
        searchable: false,
      },
      { data: "COMMITTED_FTE_SUM",
        visible: false,
        searchable: false,
      },

      { data: null,
      },

      { data: null,
      },
      { data: null,
      },
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // count number of FAs and Is
      var inst = 0;
      var fa = api
          .column( colFA, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            inst++;
            return a;
          }, 0 );
      $( api.column( colFA ).footer() ).html(fa ? fa.size : 0);
      $( api.column( colI ).footer() ).html(inst);

      // create sums of all numeric columns
      var suma = api
          .column( colAlloc, { search:'applied' } )
          .data()
          .reduce( sum, 0 );

      var sumr = api
          .column( colReq, { search:'applied' } )
          .data()
          .reduce( sum, 0 );

      var perc = otpPercentage(suma, sumr);

      var ic = api
          .column( colIcSum, { search:'applied' } )
          .data()
          .reduce( sum, 0 );

      var ic_src = api
          .column( colIcSrc, { search:'applied' } )
          .data()
          .reduce( sum, 0 );

      var ic_sum = api
          .column( colIcSum, { search:'applied' } )
          .data()
          .reduce( sum, 0 );

      var ic_req_perc = otpPercentage(ic_sum, sumr);

      var alloc_ic_perc = otpPercentage(suma, ic_sum);
      var req_ic_perc = otpPercentage(sumr, ic_sum);

      // Update footer
      $( api.column( colAlloc ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));
      $( api.column( colReq ).footer() ).html((sumr*otpFactor).toFixed(otpSumPrecision));
      $( api.column( colPerc ).footer() ).html((perc).toFixed(otpPercentagePrecision));
      $( api.column( colPerc ).footer() ).addClass(otpGetColorClass(perc));

      $( api.column( colIc ).footer() ).html((ic_sum*otpFactor).toFixed(otpSumPrecision));
      $( api.column( colIcSrc ).footer() ).html((ic_src*otpFactor).toFixed(otpSumPrecision));
      $( api.column( colIcSum ).footer() ).html((ic_sum*otpFactor).toFixed(otpSumPrecision));

      $( api.column( colIcReq ).footer() ).html((ic_req_perc).toFixed(otpPercentagePrecision));
      // $( api.column( colIcReq ).footer() ).addClass(otpGetColorClass(ic_req_perc));

      $( api.column( colAllocIc ).footer() ).html((alloc_ic_perc).toFixed(otpPercentagePrecision));
      // $( api.column( colAllocIc ).footer() ).addClass(otpGetColorClass(alloc_ic_perc));
      $( api.column( colReqIc ).footer() ).html((req_ic_perc).toFixed(otpPercentagePrecision));
      // $( api.column( colReqIc ).footer() ).addClass(otpGetColorClass(req_ic_perc));
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
} );
