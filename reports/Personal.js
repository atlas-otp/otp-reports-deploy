$(document).ready(function() {
  "use strict";
  otpInitBegin("Personal");

  otpInitSelect('year', 'Years');
  otpInitInput('person');
  otpInitSelect('category', 'Classes');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colLastName = 0;
  var colFirstName = 1;
  var colEmail = 2;
  var colCategory = 3;
  var colTID = 4;
  var colTask = 5;
  var colRID = 6;
  var colRequirement = 7;
  var colAlloc = 8;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        $("#person").val(json.id);
        $("#person-label").html('<b>' + json.first_name + " " + json.last_name + '</b>');
        return json.data;
      }
    },
    language: otpGetLanguage(),
    buttons: otpGetExport(),
    initComplete: function (settings, json) {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
    },
    columnDefs: [
      {
        targets: colLastName,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? '<a href="mailto:'+row.EMAIL+'">'+data+'</a>' : data;
        }
      },
      {
        targets: colTask,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? '<a href="https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#Ma0_Task_panel(N'+row.TID+')">'+data+'</a>' : data;
        }
      },
      {
        targets: [colAlloc],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(otpSumPrecision);
        },
        className: "text-right",
        type: "num"
      },
    ],
    order: [[colAlloc, "desc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    // deferRender: true,
    // scroller: true,
    scrollCollapse: true,
    scrollResize: true,
    lengthChange: false,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      // Sorting is too slow (> 5000 records)
      { data: "LNAME", sortable: false, searchable: false },
      { data: "FNAME", sortable: false, searchable: false },
      { data: "EMAIL", sortable: false, visible: false, searchable: false },
      { data: "CATEGORY_CODE", searchable: false },
      { data: "TID", sortable: false, visible: false, searchable: false },
      { data: "TASK", searchable: false },
      { data: "RID", sortable: false, visible: false, searchable: false },
      { data: "REQUIREMENT", searchable: false },
      { data: "AFTE", searchable: false },
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // create sums of all numeric month columns
      var columns = [ colAlloc ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        total = (total*otpFactor).toFixed(otpSumPrecision);
        $( api.column( columns[i] ).footer() ).html(total);
      }
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
} );
