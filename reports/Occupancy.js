$(document).ready(function() {
  "use strict";
  otpInitBegin("Occupancy");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('funding', 'Funding Agencies');
  otpInitSelect('institution', 'Institutions');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colPID = 0;
  var colLastName = 1;
  var colFirstName = 2;
  var colEmail = 3;
  var colInstitution = 4;
  var colInstitute = 5;
  var colSum = 6;

  var byInstitute = window.location.search.includes('byInstitute');

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(40),
    buttons: otpGetExport(),
    initComplete: function (settings, json) {
      table.buttons().container().appendTo('#table_wrapper .col-md-6:eq(0)');
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      // FIXME #98
      // $('<span id="search_filter" class="badge badge-warning">Filtered</span>').prependTo('#table_filter');
      // $('<span id="staging" class="badge badge-danger">Staging</span>').prependTo('#table_filter');
      // $('<span id="branches" class="badge badge-danger">Branch</span>').prependTo('#table_filter');
    },
    columnDefs: [
      {
        targets: colLastName,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? '<a href="mailto:'+row.EMAIL+'">'+data+'</a>' : data;
        }
      },
      {
        targets: [colSum],
        render: function ( data, type, row, meta ) {
          var year = otpGetValue('year');
          var category = otpGetValueOrAll('category');
          var otpType = otpGetValueOrAll('type');
          var recognition = otpGetValueOrAll('recognition');
          var unit = otpGetValueOrAll('unit');
          var pid = row.PID;
          var href = './Personal.php#year='+year+'&category='+category+'&type='+otpType+'&recognition='+recognition+'&unit='+unit+'&person='+pid;
          var warn = data > 1.2;
          return type === 'display' ? (warn ? '<b>' : '') + '<a href="' + href + '">' + (data*otpFactor).toFixed(otpSumPrecision) + '</a>' + (warn ? '</b>' : '') : data;
        },
        className: "text-right",
        type: "num"
      },
    ],
    order: [[ colSum, "desc" ], [colLastName, "asc"]],
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    paging: false,
    deferRender: true,
    scroller: false,
    scrollResize: true,
    lengthChange: false,
    search: {
      search: otpLoad("Search")
    },
    columns: [
      // Sorting is too slow (> 5000 records)
      { data: "PID", sortable: true, visible: true, searchable: true },
      { data: "LNAME" },
      { data: "FNAME", sortable: false, searchable: false },
      { data: "EMAIL", sortable: false, visible: false, searchable: false },
      { data: "INAME", sortable: true, visible: byInstitute, searchable: byInstitute },
      { data: "RINAME", sortable: true, visible: byInstitute, searchable: byInstitute },
      { data: "SFTE", searchable: false },
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      var persons = 0;
      api.column( colLastName, { search:'applied' }  )
         .data()
         .reduce( function (a, b) {
            persons++;
            return a;
          }, 0 );
      $( api.column( colLastName ).footer() ).html(persons);

      // create sums of all numeric month columns
      var columns = [ colSum ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        total = (total*otpFactor).toFixed(otpSumPrecision);
        $( api.column( columns[i] ).footer() ).html(total);
      }
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header){
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
} );
