<?php
date_default_timezone_set("Europe/Zurich");
$dateformat = "d-m-y H:i:s";
$utcformat = "D, d M Y H:i:s T";
$updatetime = 15*60;

set_include_path(get_include_path() . PATH_SEPARATOR . '../');
?>

<?php

// NOTE: defaults need to be set when including this file
$defaultYear = date("Y");   // current year
// $defaultStart = "01-Jan-" . date('y');  // start of current year
$defaultStart = date('d-M-Y');
$defaultEnd = "31-Dec-" . date('Y'); // end of current year
$defaultWeeks = "" != "" ? "" : 4;  /** @phpstan-ignore-line */
$defaultCategory = "" != "" ? "" : "Class 1";  /** @phpstan-ignore-line */
$defaultFunding = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultInstitution = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultSystem = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultActivity = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultType = "" != "" ? "" : "All";  /** @phpstan-ignore-line */
$defaultRecognition = "" != "" ? "" : "Duty";  /** @phpstan-ignore-line */
$defaultTask = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultPerson = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultShadowTask = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultMinShadows = "" != "" ? "" : 1;  /** @phpstan-ignore-line */
$defaultTrainingTask = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultMinTrainings = "" != "" ? "" : 1;  /** @phpstan-ignore-line */
$defaultRequirement = "" != "" ? "" : 0;  /** @phpstan-ignore-line */
$defaultName = "" != "" ? "" : 'First L.';  /** @phpstan-ignore-line */

$otpDebug = false;
$otpConnection = null;
$otpDatabase = '';
$otpStatementId = null;
$otpSql = '';
$otpShowSql = false;

function otpSetup(): void {
  global $otpDebug, $otpShowSql;

  if (isset($_REQUEST['debug'])) {
    $otpDebug = true;
  } else {
    $otpDebug = false;
  }

  if (isset($_REQUEST['sql'])) {
    $otpShowSql = true;
  } else {
    $otpShowSql = false;
  }

  error_reporting(E_ALL);
  ini_set('display_errors', '1');

  date_default_timezone_set("Europe/Zurich");
}

function otpGetRequestValue(string $key, string $defaultValue): string {
  // NOTE: Using isset() rather than empty(), to make sure 'category=' results in '' rather than 'All'
  return isset($_REQUEST[$key]) ? $_REQUEST[$key] : $defaultValue;
}

function otpDate(string $dateString): DateTime|false {
  $dateString = str_replace('"','',$dateString);
  $dateString = str_replace("'",'',$dateString);
  $date = date_create_from_format('d-M-y', $dateString);    // 1-Dec-20 1-December-20 01-Dec-20 01-December-20
  if (!$date) {
    $date = date_create_from_format('d-M-Y', $dateString);  // 1-Dec-2020 1-December-2020 01-Dec-2020 01-December-2020
  }
  if (!$date) {
    $date = date_create_from_format('d-m-y', $dateString);  // 1-1-20 1-01-20 1-12-20
  }
  if (!$date) {
    $date = date_create_from_format('d-m-Y', $dateString);  // 1-1-2020 1-01-2020 1-12-2020
  }
  return $date;
}

function otpDateId(string $dateString): string {
  $date = otpDate($dateString);

  if (!$date) {
    return "197010101";
  }
  $quarter = floor((date_format($date, 'm')-1)/3) + 1;
  return date_format($date, 'Y'.$quarter.'md');
}

function otpDateString(string $dateString): string {
  $date = otpDate($dateString);

  if (!$date) {
    return "'1970-01-01'";
  }
  return "'" . date_format($date, 'Y-m-d') . "'";
}

function otpDateDt(string $dateString, bool $isMySql): string {
  $date = otpDateString($dateString);
  return $isMySql ? "str_to_date($date, '%Y-%m-%d')" : "to_date($date, 'YYYY-MM-DD')";
}

function otpParams(): void {
  global $defaultYear, $defaultStart, $defaultEnd, $defaultWeeks;
  global $defaultCategory, $defaultSystem, $defaultActivity;
  global $defaultType, $defaultRecognition;
  global $defaultFunding, $defaultInstitution;
  global $defaultPerson;
  global $defaultTask, $defaultShadowTask, $defaultTrainingTask;
  global $defaultMinShadows, $defaultMinTrainings;
  global $defaultRequirement;
  global $defaultName;

  global $year, $start, $end, $weeks, $now;
  global $startDt, $endDt, $nowDt;
  global $startId, $endId, $nowId;
  global $category, $system, $activity;
  global $type, $recognition;
  global $funding, $institution;
  global $person;
  global $task, $shadowTask, $trainingTask;
  global $minShadows, $minTrainings;
  global $requirement;
  global $count, $not_ok;

  global $sql_details, $isMySql;

  $isMySql = $sql_details['type'] == 'Mysql';

  // December
  global $DEC;
  $DEC = $isMySql ? '`DEC`' : '"DEC"';
  // NVL
  global $NVL;
  $NVL = $isMySql ? 'IFNULL' : 'NVL';
  // LOAD
  global $LOAD;
  $LOAD = $isMySql ? '`LOAD`' : '"LOAD"';
  // SYSTEM
  global $SYSTEM;
  $SYSTEM = $isMySql ? '`SYSTEM`' : '"SYSTEM"';
  // DESCRIPTION
  global $DESCRIPTION;
  $DESCRIPTION = $isMySql ? '`DESCRIPTION`' : '"DESCRIPTION"';
  // LISTAGG
  global $LISTAGG;
  $LISTAGG = $isMySql ? 'GROUP_CONCAT' : 'LISTAGG';

  $dateFormat = 'd-M-Y';
  $now = date($dateFormat);
  $weekInSeconds = 60 * 60 * 24 * 7;

  $year = otpGetRequestValue('year', $defaultYear);
  $defaultStart = "01-Jan-" . $year; // start of current year
  $defaultEnd = "01-Jan-" . (((int)$year)+1); // start of next year
  $start = otpGetRequestValue('start', $defaultStart);
  $end = otpGetRequestValue('end', $defaultEnd);
  $weeks = otpGetRequestValue('weeks', $defaultWeeks);
  $category = otpGetRequestValue('category', $defaultCategory);
  $funding = otpGetRequestValue('funding', $defaultFunding);
  $institution = otpGetRequestValue('institution', $defaultInstitution);
  $system = otpGetRequestValue('system', $defaultSystem);
  $activity = otpGetRequestValue('activity', $defaultActivity);
  $type = otpGetRequestValue('type', $defaultType);
  $recognition = otpGetRequestValue('recognition', $defaultRecognition);
  $person = otpGetRequestValue('person', $defaultPerson);
  $task = otpGetRequestValue('task', $defaultTask);
  $shadowTask = otpGetRequestValue('shadowTask', $defaultShadowTask);
  $minShadows = otpGetRequestValue('minShadows', $defaultMinShadows);
  $trainingTask = otpGetRequestValue('trainingTask', $defaultTrainingTask);
  $minTrainings = otpGetRequestValue('minTrainings', $defaultMinTrainings);
  $requirement = otpGetRequestValue('requirement', $defaultRequirement);
  $name = otpGetRequestValue('name', $defaultName);

  // FLAG
  if (isset($_REQUEST['commingWeeks'])) {
    $start = $now;
    $end = date($dateFormat, strtotime("now") + (((int)$weeks) * $weekInSeconds));
  }

  // date_id
  $startId = otpDateId($start);
  $endId = otpDateId($end);
  $nowId = otpDateId($now);

  // convert to DB
  $startDt = otpDateDt($start, $isMySql);
  $endDt = otpDateDt($end, $isMySql);
  $nowDt = otpDateDt($now, $isMySql);
}

include( "config.php" );
include ("editor/lib/DataTables.php");

use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate,
    DataTables\Editor\ValidateOptions,
    DataTables\Database,
       DataTables\Database\Query,
       DataTables\Database\Result;

function otpConnect(): void {
  global $sql_details, $db, $person;

  // Get logged in Person
  if ($person == 0) {
    if (in_array($_SERVER['SERVER_NAME'], ['localhost', 'otp-next.localhost', 'otp-www.localhost'])) {
      $user = "DUNS";
    } else {
      // Old SSO
      $user = getenv("OIDC_CLAIM_cern_upn");
      if (!$user) {
          // compatible with OKD4 SSO
          $user = $_SERVER['HTTP_X_FORWARDED_USER'];
      }

      if ($user) {
          $user = strtoupper($user);
      } else {
          $user = "";
      }
    }

    $sql = "select ID  from PUB_PERSON where USERNAME = '" . $user . "'";

    $result = $db->sql( $sql );

    if ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
      $person = $row['ID'];
    }
  }

}

function otpSqlStart(): void {
  global $otpSql, $db, $otpResult;

  $otpResult = $db->sql( $otpSql );
}

function otpToJson(): void {
  global $otpResult, $db, $otpSql;
  global $isMySql;

  global $year, $start, $end, $weeks, $now;
  global $startDt, $endDt, $nowDt;
  global $startId, $endId, $nowId;
  global $category, $system, $activity;
  global $type, $recognition;
  global $funding, $institution;
  global $person;
  global $task, $shadowTask, $trainingTask;
  global $minShadows, $minTrainings;
  global $requirement;
  global $name;
  global $count, $not_ok;

  // NOTE: php will have undefined values as not all times these are available
  echo "  \"year\": ", $year, ",\n";
  echo "  \"start\": \"", $start, "\",\n";
  echo "  \"start_dt\": \"", $startDt, "\",\n";
  echo "  \"start_id\": \"", $startId, "\",\n";
  echo "  \"end\": \"", $end, "\",\n";
  echo "  \"end_dt\": \"", $endDt, "\",\n";
  echo "  \"end_id\": \"", $endId, "\",\n";
  echo "  \"weeks\": ", $weeks, ",\n";
  echo "  \"now\": \"", $now, "\",\n";
  echo "  \"now_dt\": \"", $nowDt, "\",\n";
  echo "  \"now_id\": \"", $nowId, "\",\n";
  echo "  \"category\": \"", $category, "\",\n";
  echo "  \"system\": \"", $system, "\",\n";
  echo "  \"activity\": \"", $activity, "\",\n";
  echo "  \"type\": \"", $type, "\",\n";
  echo "  \"recognition\": \"", $recognition, "\",\n";
  echo "  \"funding\": \"", $funding, "\",\n";
  echo "  \"institution\": \"", $institution, "\",\n";
  echo "  \"task\": \"", $task, "\",\n";
  echo "  \"person\": \"", $person, "\",\n";
  echo "  \"shadowTask\": \"", $shadowTask, "\",\n";
  echo "  \"minShadows\": \"", $minShadows, "\",\n";
  echo "  \"trainingTask\": \"", $trainingTask, "\",\n";
  echo "  \"minTrainings\": \"", $minTrainings, "\",\n";
  echo "  \"requirement\": \"", $requirement, "\",\n";
  echo "  \"name\": \"", $name, "\",\n";
  echo "  \"is_mysql\": \"", $isMySql, "\",\n";
  // echo "  \"version\": \"", exec('git describe --tags --always'), "\",\n";
  echo "  \"data\": [\n";

  $first = true;

  $count = 0;
  $not_ok = 0;

  while ($row = $otpResult->fetch($fetchType=\PDO::FETCH_ASSOC)) {
    // see http://php.net/manual/en/function.json-encode.php, UTF-8-encode, otherwise we will loose names with accents
    array_walk_recursive($row, function(&$val) {
      $val = utf8_encode($val);
    });

    echo ($first ? "    " : ",\n    ").json_encode($row);
    $first = false;
    $count++;
    if (array_key_exists('OK', $row) && ($row['OK'] != 'Yes')) {
      $not_ok++;
    }
  }

  echo "\n  ],\n";
  echo "  \"count\": ", $count, ",\n";
  echo "  \"not_ok\": ", $not_ok, "\n";
}

function otpSqlEnd(): void {
}

function otpClose(): void {
  global $count, $not_ok;

  if (isset($_REQUEST['count'])) {
    exit($count);
  }
  if (isset($_REQUEST['not_ok'])) {
    exit($not_ok);
  }
}

function otpHeader(): void {
  global $otpDatabase, $otpShowSql;

  if ($otpShowSql) {
    header("Content-Type: text/plain");
  } else {
    header("Content-Type: application/json");
    header("Oracle-DB: ".$otpDatabase);
  }
}

function otpShowSql(): void {
  global $otpSql;

  echo $otpSql.";\n";
}

function otpQuote(string $string): string {
  // split by comma (separator) except if followed by whitespace (e.g. Yerevan,NRC KI, Protvino,NIKHEF => Yerevan; NRC KI, Protvino; NIKHEF), issue #118
  $string = "'" . preg_replace('/,(?!\s)/m', "','", $string) . "'";
  return $string;
}

function getLabel(string $name): string {
  if ($name == 'funding') return "Funding Agency";
  if ($name == 'name') return "Display Name";

  return ucwords($name);
}

function getOptions(string $name, string $sql = ''): void {
  global $db;

  if ($name == 'category') {
    echo "<option>Class 1</option>\n";
    echo "<option>Class 2</option>\n";
    echo "<option>Class 3</option>\n";
    echo "<option>Class 4</option>\n";
    echo "<option>Upgrade Construction</option>\n";
    return;
  }

  if ($name == 'year') {
    for ($year = 2025; $year >= 2010; $year--) {
      echo "<option>$year</option>\n";
    }
    return;
  }

  if ($name == 'type') {
    echo "<option>Expert</option>\n";
    echo "<option>Shifter</option>\n";
    return;
  }

  if ($name == 'recognition') {
    echo "<option>Duty</option>\n";
    echo "<option>Personal Duty</option>\n";
    echo "<option>Contract</option>\n";
    echo "<option>Not Operational Task</option>\n";
    return;
  }

  if ($name == 'unit') {
    echo "<option>Auto Units</option>\n";
    echo "<option>Shifts</option>\n";
    echo "<option>FTEs</option>\n";
    return;
  }

  if ($name == 'name') {
    echo "<option>Full Name</option>\n";
    echo "<option>F. Last</option>\n";
    echo "<option>First L.</option>\n";
    return;
  }

  $result = $db->sql( $sql );

  echo '  "'.$name.'": [';
  $oldGroup = "";
  $group = "";
  while ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
    $group = array_key_exists('GRP', $row) ? trim($row['GRP']) : "";
    if ($group != $oldGroup) {
      if ($oldGroup != "") {
        echo "</optgroup>\n";
      }
      echo "<optgroup label=\"".$group."\">\n";
      $oldGroup = $group;
    }
    // Inlined commas will have a space behind them, so they can be distinguished from comma separators
    $value = $row['NAME']; // str_replace(',','|',$row['NAME']);
    if (array_key_exists('ID', $row)) {
      echo "<option data-institution-id=\"".trim($row['ID'])."\">".trim($value)."</option>\n";
    } else {
      echo "<option>".trim($value)."</option>\n";
    }
  }
  if ($oldGroup != "") {
    echo "</optgroup>\n";
  }
}

function getButton(string $name): void {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
  echo "<button type=\"button\" class=\"btn btn-primary mb-2 mr-sm-2\" id=\"$name\">$label</button>\n";
  echo "</div>\n";
}

function getSingleSelect(string $name, string $sql = ''): void {
  getSelect($name, $sql, false);
}

function getSelect(string $name, string $sql = '', bool $multiple=true, bool $all=false): void {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\">$label</span>\n";
      echo "</div>\n";
      echo "<select ".($multiple ? "multiple=\"multiple\"" : "")." class=\"selectpicker\" id=\"$name\" data-width=\"fit\" title=\"Choose...\">\n";
        if ($all) {
          echo "<option disabled=\"disabled\">All</option>\n";
        }
        getOptions($name, $sql);
      echo "</select>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function getInput(string $name): void {
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\" id=\"$name-label\">$label</span>\n";
      echo "</div>\n";
      echo "<input class=\"form-control\" id=\"$name\" placeholder=\"$label\"/>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function getCheck(string $name, bool $state): void {
  $label = getLabel($name);
  echo "<div class=\"form-check\">\n";
    echo "<input class=\"form-check-input\" type=\"checkbox\" value=\"" . ($state ? "true" : "false") . "\" id=\"$name\"" . ($state ? " checked=\"checked\"" : "") . ">\n";
    echo "<label class=\"form-check-label\" for=\"$name\">$label</label>\n";
  echo "</div>\n";
}

function otpReset(): void {
  getButton("Reset");
}

function otpSelectDateRange(): void {
  global $start, $end;
  $name = "dates";
  $label = getLabel($name);
  echo "<div class=\"form-group\">\n";
    echo "<label class=\"sr-only\" for=\"$name\">$label</label>\n";
    echo "<div class=\"input-group mb-2 mr-sm-2\">\n";
      echo "<div class=\"input-group-prepend\">\n";
        echo "<span class=\"input-group-text\" id=\"$name-label\">$label</span>\n";
      echo "</div>\n";
      echo "<input class=\"form-control\" id=\"$name\" type=\"text\" name=\"daterange\" value=\"$start - $end\"/>\n";
    echo "</div>\n";
  echo "</div>\n";
}

function otpSelectTask(): void {
  getInput("task");
}

function otpSelectPerson(): void {
  getInput("person");
}

function otpSelectShadowTask(): void {
  getInput("shadowTask");
}

function otpSelectMinShadows(): void {
  getInput("minShadows");
}

function otpSelectTrainingTask(): void {
  getInput("trainingTask");
}

function otpSelectMinTrainings(): void {
  getInput("minTrainings");
}

function otpSelectRequirement(): void {
  getInput("requirement");
}


function otpSelectCategory(bool $single = false, bool $all = false): void {
  // select CODE as NAME from DOMAIN_LISTS where TYP_CODE='Category' and ALLOW_SELECT='Y' order by ORDER_IN_TYPE
  getSelect('category', '', !$single, $all);
}

function otpSelectYear(bool $single = false, bool $all = false): void {
  getSelect('year', '', !$single, $all);
}

function otpSelectSystem(bool $single = false, bool $all = false): void {
  getSelect('system', "select TITLE as NAME from PUB_SYSTEM_NODE order by TITLE", !$single, $all);
}

function otpSelectActivity(bool $single = false, bool $all = false): void {
  getSelect('activity', "select TITLE as NAME from PUB_WBS_NODE where PARENT_ID is null order by TITLE", !$single, $all);
}

function otpSelectType(bool $single = false, bool $all = false): void {
  getSelect('type', '', !$single, $all);
}

function otpSelectRecognition(bool $single = false, bool $all = false): void {
  getSelect('recognition', '', !$single, $all);
}

function otpSelectUnit(bool $single = false, bool $all = false): void {
  getSelect('unit', '', !$single, $all);
}

function otpSelectFunding(bool $single = false, bool $all = false): void {
  getSelect('funding', "select distinct F.NAME as NAME from PUB_FUNDING_AGENCY F join PUB_INSTITUTE I on I.FUNDA_ID = F.ID order by F.NAME", !$single, $all);
}

function otpSelectInstitution(bool $single = false, bool $all = false): void {
  getSelect('institution', "select F.NAME as GRP, ONAME as NAME, I.ID as ID from PUB_INSTITUTE I join PUB_FUNDING_AGENCY F on F.ID = I.FUNDA_ID  where I.ID < 2000 or I.ID >= 3000 order by F.NAME, ONAME", !$single, $all);
}

function otpSelectInstitutionAndICs(bool $single = false, bool $all = false): void {
  getSelect('institution', "select F.NAME as GRP, ONAME as NAME, I.ID as ID from PUB_INSTITUTE I join PUB_FUNDING_AGENCY F on F.ID = I.FUNDA_ID order by F.NAME, ONAME", !$single, $all);
}

function otpSelectName(): void {
  getSelect("name", '', false, false);
}

function otpShowMonths(bool $state = false): void {
  getCheck("show_months", $state);
}


global $otpResult, $db, $otpSql;
global $isMySql;

global $year, $start, $end, $weeks, $now;
global $startDt, $endDt, $nowDt;
global $startId, $endId, $nowId;
global $category, $system, $activity;
global $type, $recognition;
global $funding, $institution;
global $person;
global $task, $shadowTask, $trainingTask;
global $minShadows, $minTrainings;
global $requirement;

?>


<?php
  global $otpDebug, $otpShowSql;

  otpSetup();

  if ($otpDebug) {
      header("Content-Type: application/json");
      echo file_get_contents("CommitmentDataExample.json");
      exit();
  }

  otpParams();
  otpConnect();
  otpHeader();
  if (!$otpShowSql) { echo "{\n"; }
?>

  <?php

$category = otpQuote($category);
$activity = otpQuote($activity);
$funding = otpQuote($funding);
$institution = otpQuote($institution);
$system = otpQuote($system);
$recognition = otpQuote($recognition);

$dateid_this_year = $year*100000;
$dateid_next_year = ($year+1)*100000;

// Institutes
$result = $db->sql('select ID, ONAME from PUB_INSTITUTE order by ID');

echo "  \"instituteData\": {\n";

$first = true;

while ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
  $id=$row['ID'];
  $oname=$row['ONAME'];

  echo ($first ? "    " : ",\n    ").json_encode($id).": ".json_encode($oname);
  $first = false;
}

echo "\n  },\n";


// FTEs
$result = $db->sql(
"select R.ID rid, I.ID institute_id, sum(SA.allocated_fte) fte
from PUB_SUM_ALLOCATION_V SA
join PUB_RES_REQUIREMENT R on SA.RES_REQUIREMENT_ID = R.ID
join PUB_TASK T on R.TASK_ID = T.ID
join PUB_SYSTEM_NODE S on T.SYSTEM_ID = S.ID
join PUB_WBS_NODE WBS on T.WBS_ID = WBS.ID
join PUB_WBS_NODE A on A.ID = WBS.PARENT_ID
join PUB_INSTITUTE I on I.ID = SA.ALLOCATED_PERSON_INSTITUTE_ID
join PUB_FUNDING_AGENCY F on F.ID = I.FUNDA_ID
join PUB_COMMITMENTS C on C.YEAR = $year and C.RES_REQUIREMENT_ID = R.ID and (C.INSTITUTE_ID = I.ID
-- Clause for Institution
  or I.ID in (
    -- csv to rows
    select TO_NUMBER (column_value)
    from PUB_INSTITUTE PI,
        xmltable('tokenize(., \",\")'
              passing PI.NAME
              columns
                -- n for ordinality,
                column_value varchar2(10) path '.'
              ) x
    where 2000 <= C.INSTITUTE_ID and C.INSTITUTE_ID < 3000 and PI.ID = C.INSTITUTE_ID
  )
)
where extract(year from sa.dt) = $year
and (('All' in ($recognition)) or (SA.RECOGNITION_CODE in ($recognition)))
and (('All' in ($category)) or (CATEGORY_CODE in ($category)))
-- no funding agency or institute selection as clusters may be different
and (('All' in ($system)) or (trim(S.TITLE) in ($system)))
and (('All' in ($activity)) or (trim(A.TITLE) in ($activity)))
group by R.ID, I.ID
order by R.ID, I.ID
");

echo "  \"fteData\": {\n";

$first = true;

while ($row = $result->fetch($fetchType=\PDO::FETCH_ASSOC)) {
  $rid=$row['RID'];
  $institute_id=$row['INSTITUTE_ID'];
  $fte=$row['FTE'];

  echo ($first ? "    " : ",\n    ").json_encode($rid."-".$institute_id).": ".json_encode($fte);
  $first = false;
}

echo "\n  },\n";

// Missing fteTaskData !!! FIXME

$otpSql =
"SELECT funding, inst_id, institution, iname, activity, system, task_id, task,
        description,
        committed_fte, 0 as allocated_fte, required_fte
-- fte filled by javascript with the fteData table
from (
  select
  -- coalesce(THIS_YEAR.funding) as funding,
  THIS_YEAR.funding as funding,
  THIS_YEAR.inst_id as inst_id,
  THIS_YEAR.institution as institution,
  THIS_YEAR.iname as iname,
  THIS_YEAR.description as description,
  THIS_YEAR.activity as activity,
  THIS_YEAR.system as system,
  THIS_YEAR.task_id as task_id,
  THIS_YEAR.task as task,
  THIS_YEAR.committed_fte as committed_fte
  from (
    select f.name funding, i.id inst_id, i.oname institution, i.name iname, a.title activity, s.title system, t.id task_id, t.short_title task, r.id requirement_id, r.title requirement, c.value/100 committed_fte, c.description description
    from PUB_COMMITMENTS C
    join PUB_INSTITUTE I on C.INSTITUTE_ID = I.ID
    join PUB_RES_REQUIREMENT R on C.RES_REQUIREMENT_ID = R.ID
    join PUB_TASK T on R.TASK_ID = T.ID
    join PUB_SYSTEM_NODE S on T.SYSTEM_ID = S.ID
    join PUB_WBS_NODE WBS on T.WBS_ID = WBS.ID
    join PUB_WBS_NODE A on A.ID = WBS.PARENT_ID
    join PUB_FUNDING_AGENCY F on F.ID = I.FUNDA_ID
    where C.YEAR = $year
    and (('All' in ($category)) or (CATEGORY_CODE in ($category)))
    -- avoid Upgrade Construction
    and CATEGORY_CODE not in ('Upgrade Construction')
    and (('All' in ($funding)) or (trim(F.NAME) in ($funding)))
    and (('All' in ($institution)) or (trim(I.ONAME) in ($institution)))
    and (('All' in ($system)) or (trim(S.TITLE) in ($system)))
    and (('All' in ($activity)) or (trim(A.TITLE) in ($activity)))
    and c.value > 0
  ) THIS_YEAR
) left join (
  -- Select Required FTE for every requirement
  select res_requirement_id rid, sum(sr.required_fte) required_fte
  from PUB_SUM_REQUIREMENT_V SR
  join PUB_TASK T on SR.TASK_ID = T.ID
  where SR.DATE_ID >= $dateid_this_year
  and SR.DATE_ID < $dateid_next_year
  and (('All' in ($category)) or (T.CATEGORY_CODE in ($category)))
  and (('All' in ($recognition)) or (SR.RECOGNITION_CODE in ($recognition)))
  group by RES_REQUIREMENT_ID
  order by RES_REQUIREMENT_ID
) on rid = requirement_id
"
;

?>


<?php
  if ($otpShowSql) {
    otpShowSql();
    otpClose();
    return;
  }
  otpSqlStart();
  otpToJson();
  otpSqlEnd();
  echo "}\n";
  otpClose();
?>
