$(document).ready(function() {
  "use strict";
  otpInitBegin("Commitment");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('funding', 'Funding Agencies');
  otpInitSelect('institution', 'Institutions');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var institutes;
  var fteData;
  var fteTaskData;

  var colSystem = 0;
  var colActivity = 1;
  var colTaskId = 2;
  var colTask = 3;
  var colRequirementId = 4;
  var colRequirement = 5;
  var colFunding = 6;
  var colInstitutionId = 7;
  var colInstitution = 8;
  var colIName = 9;
  var colDescription = 10;
  var colIc = 11;
  var colAlloc = 12;
  var colReq = 13;
  var colPerc = 14;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        if ("instituteData" in json) {
          // Get ID -> Institution Table
          institutes = json.instituteData;
          // console.log(institutes);
        }

        if ("fteData" in json) {
          fteData = json.fteData;
        }

        if ("fteTaskData" in json) {
          fteTaskData = json.fteTaskData;
        }

        // run over all data and fill in allocatedFTE, summed for ICs
        for (var r2=0; r2 < json.data.length; r2++) {
          var requirementId = json.data[r2].REQUIREMENT_ID.trim();
          var taskId = json.data[r2].TASK_ID.trim();
          var instituteId = json.data[r2].INST_ID.trim();

          // IC Clusters only, sum all allocations from different Institutions
          if ((2000 <= json.data[r2].INST_ID) && (json.data[r2].INST_ID < 3000)) {
            var instituteIds = json.data[r2].INAME.split(',');
            var total = 0;
            for (var index = 0; index < instituteIds.length; index++) {
              instituteId = instituteIds[index].trim();
              var a = fteData[requirementId + '-' + instituteId];
              var b = fteTaskData[taskId + '-' + instituteId];
              if (a !== undefined) {
                total += Number(a);
              } else if (b !== undefined) {
                total += Number(b);
              }
            }
            json.data[r2].ALLOCATED_FTE = total;
          } else {
            // normal case
            var c = fteData[requirementId + '-' + instituteId];
            var d = fteTaskData[taskId + '-' + instituteId];
            if (c !== undefined) {
              json.data[r2].ALLOCATED_FTE = Number(c);
            }else if (d !== undefined) {
              json.data[r2].ALLOCATED_FTE = Number(d);
            }
          }
        }

        return json.data;
      }
    },
    language: otpGetLanguage(30),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
    },
    order: [[ colRequirement, "asc" ], [ colIc, "desc"], [ colAlloc, "desc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columnDefs: [
      {
        targets: colTaskId,
        render: function ( data, type, row, meta ) {
            return type === 'display' ? '<a href="https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#Ma0_Task_panel(N'+row.TASK_ID+')">'+data+'</a>' : data;
        }
      },
      {
        targets: colInstitution,
        render: function ( data, type, row, meta ) {
            if (type === 'sort') return data;
            // filter and display
            if ((row.INST_ID < 2000) || (3000 <= row.INST_ID)) return data;
            var instituteIds = row.INAME.split(',');
            for (var index = 0; index < instituteIds.length; index++) {
              var instituteId = instituteIds[index].trim();
              var key = row.REQUIREMENT_ID + '-' + instituteId;
              var taskKey = row.TASK_ID + '-' + instituteId;
              var fte = fteData[key] !== undefined ?  fteData[key] : fteTaskData[taskKey] !== undefined ? fteTaskData[taskKey] : 0;
              data += '</br>' + institutes[instituteId] + ':&nbsp;<span>' + (fte*otpFactor).toFixed(2) + '</span>';
            }
            return  data;
        },
        className: "text-right"
      },
      { targets: [ colIc, colAlloc, colReq ],
        render: function ( data, type, row, meta ) {
          return (data*otpFactor).toFixed(2);
        },
        className: "text-right",
        type: "num"
      },
      { targets: [ colPerc ],
        render: function (data, type, row, meta) {
          var result = otpPercentage(row.COMMITTED_FTE, row.REQUIRED_FTE);
          return (type == "display") ? result < 0 ? "N/A" : result.toFixed(otpPercentagePrecision) : result;
        },
        className: "text-right",
        type: "num"
      },
    ],
    columns: [
      { data: "SYSTEM" },
      { data: "ACTIVITY" },
      { data: "TASK_ID" },
      { data: "TASK" },
      { data: "REQUIREMENT_ID" },
      { data: "REQUIREMENT"},

      { data: "FUNDING" },
      { data: "INST_ID" },
      { data: "INSTITUTION" },
      { data: "INAME", visible: false },
      { data: "DESCRIPTION" },

      { data: "COMMITTED_FTE" },
      { data: "ALLOCATED_FTE" },
      { data: "REQUIRED_FTE" },
      { data: null },
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      // Remove the formatting to get integer data for summation
      var intVal = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function( a, b) {
        return intVal(a) + intVal(b);
      };

      // create sums of all numeric columns
      var columns = [ colIc, colAlloc, colPerc ];
      var total_commited = api
          .column( colIc, { search:'applied' } )
          .data()
          .reduce( sum, 0 );
      $( api.column( colIc ).footer() ).html(Number(total_commited*otpFactor).toFixed(2));

      // Sum Allocations
      var total_allocated = api
          .cells( function ( index, data, node ) {
              return true;
          }, colAlloc, { search:'applied' } )
          .data()
          .reduce( sum, 0 );
      $( api.column( colAlloc ).footer() ).html(Number(total_allocated*otpFactor).toFixed(2));

      // Sum requirements
      var total_required = api
          .cells( function ( index, data, node ) {
              return true;
          }, colReq, { search:'applied' } )
          .data()
          .reduce( sum, 0 );
      $( api.column( colReq ).footer() ).html(Number(total_required*otpFactor).toFixed(2));

      // percentage
      $( api.column( colPerc ).footer() ).html(otpPercentage(total_commited, total_required).toFixed(otpPercentagePrecision));

      // count number of tasks
      var c = 0;
      api.column( colActivity, { search:'applied' }  )
         .data()
         .reduce( function (a, b) {
              return c++;
         }, 0 );

      // Update footer
      $( api.column( colActivity ).footer() ).html(c);
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  otpTable.push(table);
} );
