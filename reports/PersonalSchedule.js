$(document).ready(function() {
  "use strict";
  otpInitBegin("PersonalSchedule");

  otpInitDateRange();
  otpInitInput("person");
  otpInitSelect('category', 'Classes');
  otpInitSelect('type', 'Types');
  otpInitReset();

  otpInitEnd();

  function dateFormat(date) {
    var month;
    // FIXME to be done with moment
    switch(date.getMonth()) {
      case 0: month="Jan"; break;
      case 1: month="Feb"; break;
      case 2: month="Mar"; break;
      case 3: month="Apr"; break;
      case 4: month="May"; break;
      case 5: month="Jun"; break;
      case 6: month="Jul"; break;
      case 7: month="Aug"; break;
      case 8: month="Sep"; break;
      case 9: month="Oct"; break;
      case 10: month="Nov"; break;
      case 11: month="Dec"; break;
      default:
        month = "none"; break;
    }
    return date.getDate()+'-'+month+'-'+date.getFullYear();
  }

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        $("#person").val(json.id);
        $("#person-label").html('<b>' + json.first_name + " " + json.last_name + '</b>');
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function () {
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
    },
    orderFixed: [[ 0, "asc" ]],
    order: [[ 1, "asc" ]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columnDefs: [
      {
        targets: [ 3, 6,7, 9 ],
        sortable: false,
        searchable: false,
        visible: false
      },
      {
        targets: [ 10,11,12,13,14,15,16 ],
        render: function ( data, type, row, meta ) {
            var MONDAY = 0;
            var TUESDAY = 1;
            var WEDNESDAY = 2;
            var THURSDAY = 3;
            var FRIDAY = 4;
            var SATURDAY = 5;
            var SUNDAY = 6;

            var CLASS1 = 'Class 1';
            var FACTOR_UP = 1.31;
            var FACTOR_DOWN = 0.66;

            var days = [];
            days[MONDAY] = "MON";
            days[TUESDAY] = "TUE";
            days[WEDNESDAY] = "WED";
            days[THURSDAY] = "THU";
            days[FRIDAY] = "FRI";
            days[SATURDAY] = "SAT";
            days[SUNDAY] = "SUN";

            var day_index = meta.col-10;
            var alloc = row[days[day_index]+"_A"];

            var category = row.CLS;
            var hour_from = row.HOUR_FROM;
            if (category == CLASS1) {
              if ((day_index == SATURDAY) || (day_index == SUNDAY) || (hour_from == 23)) {
                alloc /= FACTOR_UP;
              } else {
                alloc /= FACTOR_DOWN;
              }
            }

            var clz = "badge badge-success percentage";
            var html = alloc == 0 ? "" : '<span class="'+clz+'">' + Number(alloc).toFixed(0) +' %</span>';
            return type === 'display' ? html : data;
        },
        sortable: false, searchable: false
      },
    ],
    columns: [
      { data: "YEAR_WEEK_NUMBER",
        className: "title",
        sortable: false,
        render: function ( data, type, row, meta ) {
          return type === 'display' ? parseInt(data.toString().slice(-2)) : data;
        }
      },
      { data: "TASK",
        render: function ( data, type, row, meta ) {
          return type === 'display' ? '<a href="https://atlas-otp.cern.ch/mao/client/cern.ppt.mao.app.gwt.MaoClient/MaoClient.html#Ma0_Task_panel(N'+row.TID+')">'+data+'</a>' : data;
        }
      },
      { data: "TID",
        visible: false },
      { data: "CLS" },
      { data: "REQ",
        visible: false,
        render: function ( data, type, row, meta ) {
          return type === 'display' ? '<span title="'+row.RID+'">'+data+'</span>' : data;
        }
      },
      { data: "RID",
        visible: false },
      { data: "HOUR_FROM" },
      { data: "HOUR_TO" },
      { data: "DESCRIPTION",
        render: function ( data, type, row, meta ) {
          return type === 'display' ? row.HOUR_FROM+'-'+row.HOUR_TO+' '+data : data;
        }
      },
      { data: "BEGIN_OF_WEEK",
        type: "nullable" },

      { data: "MON_A" },
      { data: "TUE_A" },
      { data: "WED_A" },
      { data: "THU_A" },
      { data: "FRI_A" },
      { data: "SAT_A" },
      { data: "SUN_A" },
    ],
    rowGroup: {
      startRender: function ( rows, group ) {
        var data = rows.data();

        // once rowspan works, should disable the rendering of the weeknumber cells, can do the same for Task
        //  rowspan="' + (data.length + 1) + '"
        group = parseInt(group.toString().slice(-2));
        var html = '<td class="title" style="color: black;">' + group + '</td>';
        html += '<td colspan="2"></td>';
        var begin_of_week = new Date(data[0].BEGIN_OF_WEEK);
        for (var i = 0; i < 7; i++) {
          html += '<td><span class="date">'+dateFormat(begin_of_week)+'</span></td>';
          begin_of_week.setDate(begin_of_week.getDate() + 1);
        }
        return $('<tr/>').append(html);
      },
      endRender: function ( rows, group ) {
      },
      dataSrc: 'YEAR_WEEK_NUMBER'
    }
  });


  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  otpTable.push(table);
});
