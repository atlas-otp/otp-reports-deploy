$(document).ready(function() {
  "use strict";
  otpInitBegin("TaskYear");

  otpInitSelect('category', 'Classes');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);

  otpInitEnd();

  var colActivity = 0;
  var colWBS = 1
  var colSystem = 2;
  var colTask = 3;
  var colTaskID = 4;
  var colReq = 5;
  var colReqID = 6;

  var table = $('#table').DataTable( {
    ajax: {
      url: otpGetUrl(table),
      type: "POST",
      dataSrc: function ( json ) {
        return json.data;
      }
    },
    language: otpGetLanguage(),
    lengthChange: false,
    buttons: otpGetExport(),
    initComplete : function (settings, json) {
      // console.log("Complete");
      table.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
//        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
/*
      var chart = $('#graph').highcharts();
      chart.title.update({ text: 'Allocations '+otpGetValue('funding') });
      chart.yAxis[0].setTitle({ text: 'Allocations ['+otpUnit+']' });
      chart.redraw();
*/
    },
    columnDefs: [
      {
        targets: [7,8,9,10,11,12,13],
        render: function ( data, type, row, meta ) {
          var d = (data*otpFactor).toFixed(otpDetailPrecision);
          return d !== 0 ? d : "";
        },
        className: "text-right",
        type: "num"
      },
    ],
    order: [[ colActivity, "asc"], [ colWBS, "asc"], [ colSystem, "asc"], [ colTask, "asc"]],
    paging: false,
    info: false,
    scrollX: true,
    scrollY: 100,
    scrollCollapse: true,
    scrollResize: true,
    // deferRender: true,
    // scroller: true,
    columns: [
      { data: "ACTIVITY",
        title: 'Activity',
        className: "title",
      },
      { data: "WBS",
        title: 'WBS',
        className: "title",
      },
      { data: "SYSTEM",
        title: 'System',
        className: "title",
      },
      { data: "TASK",
        title: 'Task',
        className: "title",
      },
      { data: "TASK_ID",
        title: 'Task ID',
        className: "title",
      },
      { data: "REQUIREMENT",
        title: 'Requirement',
        className: "title",
      },
      { data: "REQUIREMENT_ID",
        title: 'Req ID',
        className: "title",
      },

      { data: "Y2019",
        title: "2019",
      },
      { data: "Y2020",
        title: "2020",
      },
      { data: "Y2021",
        title: "2021",
      },
      { data: "Y2022",
        title: "2022",
      },
      { data: "Y2023",
        title: "2023",
      },
      { data: "Y2024",
        title: "2024",
      },
      { data: "Y2025",
        title: "2025",
      }
    ],
    footerCallback: function ( row, data, start, end, display ) {
      // console.log('Footer');
      // Show buttons when we have Data, otherwise remove
      if (end - start > 0) {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
      } else {
        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
      }

      var api = this.api();

      otpStore("Search", api.search());

      var doubleValue = function ( i ) {
          return typeof i === 'string' ?
              i.replace(/[\$,]/g, '')*1 :
              typeof i === 'number' ?
                  i : 0;
      };

      var sum = function (a, b) {
        return doubleValue(a) + doubleValue(b);
      };

      // create sums of all numeric month columns
      var points = [];
      var columns = [ 7,8,9,10,11,12,13 ];
      for (var i=0; i<columns.length; i++) {
        // Total over all pages
        var total = api
            .column( columns[i], { search:'applied' } )
            .data()
            .reduce( sum, 0 );

        // Update footer
        total = (total*otpFactor).toFixed(otpSumPrecision);
        points[i] = Number(total);
        $( api.column( columns[i] ).footer() ).html(total);
      }

      // count number of Activities, WBS, Systems, Tasks, Requirements
      var na = api
          .column( colActivity, { search:'applied' }  )
          .data()
          .reduce( function (a, b) {
            if (!a) {
              a = new Set();
            }
            a.add(b);
            return a;
          }, 0 );
      $( api.column( colActivity ).footer() ).html(na ? na.size : 0);

      var nwbs = api
      .column( colWBS, { search:'applied' }  )
      .data()
      .reduce( function (a, b) {
        if (!a) {
          a = new Set();
        }
        a.add(b);
        return a;
      }, 0 );
      $( api.column( colWBS ).footer() ).html(nwbs ? nwbs.size : 0);

      var nsystem = api
      .column( colSystem, { search:'applied' }  )
      .data()
      .reduce( function (a, b) {
        if (!a) {
          a = new Set();
        }
        a.add(b);
        return a;
      }, 0 );
      $( api.column( colSystem ).footer() ).html(nsystem ? nsystem.size : 0);

      var ntask = api
      .column( colTask, { search:'applied' }  )
      .data()
      .reduce( function (a, b) {
        if (!a) {
          a = new Set();
        }
        a.add(b);
        return a;
      }, 0 );
      $( api.column( colTask ).footer() ).html(ntask ? ntask.size : 0);

      var nreq = api
      .column( colReq, { search:'applied' }  )
      .data()
      .reduce( function (a, b) {
        if (!a) {
          a = new Set();
        }
        a.add(b);
        return a;
      }, 0 );
      $( api.column( colReq ).footer() ).html(nreq ? nreq.size : 0);
    }
  } );

  // fix to make sure "Loading..." message re-apears when loading a new URL
  table.on('preXhr.dt', function(e, settings, data) {
    $(this).dataTable().api().clear();
    settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
    $(this).dataTable().api().draw();
  });

  var DataTable = $.fn.dataTable;
  DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
    var alignmentForCols = $.map( headers, function ( header ) {
        for(var i = 0; i < dt.settings()[0].aoColumns.length; i++){
            var column = dt.settings()[0].aoColumns[i];
            if(column.title == header) {
                if(column.type == 'num' || column.type == 'num-fmt')
                    return 'right';
                return 'left';
            }
        }
    });
    return  alignmentForCols;
  });

  otpTable.push(table);
} );
