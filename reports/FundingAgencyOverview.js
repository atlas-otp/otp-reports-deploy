$(document).ready(function() {
  "use strict";
  otpInitBegin("FundingAgencyOverview");

  otpInitSelect('year', 'Years');
  otpInitSelect('category', 'Classes');
  otpInitSelect('funding', 'Funding Agencies');
  otpInitSelect('system', 'Systems');
  otpInitSelect('activity', 'Activities');
  otpInitSelect('type', 'Types');
  otpInitSelect('recognition', 'Recognitions');
  otpInitSelect('unit', 'Units', true);
  // otpInitCheck('show_months', true);

  otpInitEnd();

  let table_overview = null;
  let table_overview_year = null;

  // Colors from: http://graphicdesign.stackexchange.com/questions/3682/where-can-i-find-a-large-palette-set-of-contrasting-colors-for-coloring-many-d
  /*
  let chart = $('#graph').highcharts({
      chart: {
          colors: [
            '#ff0000', '#e4e400', '#00ff00', '#00ffff', '#b0b0ff', '#ff00ff',
            '#870000', '#878700', '#008700', '#008787', '#4949ff', '#870087',
            '#550000', '#545400', '#005500', '#005555', '#0000ff', '#550055',
            '#b00000', '#baba00', '#00b000', '#00b0b0', '#8484ff', '#b000b0',
          ],
          type: 'column'
      },
      xAxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
              'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
          plotLines: [{
              value: 0,
              width: 1,
              color: '#808080'
          }]
      },
      plotOptions: {
          column: {
              stacking: 'normal',
              lineColor: '#666666',
              lineWidth: 1,
              marker: {
                  lineWidth: 1,
                  lineColor: '#666666'
              }
          }
      },
      series: [
      ]
  });
*/
  {
    let colFirstName = 0;
    let colLastName = 1;
    let colActivity = 2;
    let colSystem = 3;
    let colTask = 4;
    let colTaskId = 5;
    let colAlloc = 18;

    table_overview = $('#table_overview').DataTable( {
      ajax: {
        url: otpGetUrl(table_overview, 'FundingAgencyOverviewData.php'),
        type: "POST",
        dataSrc: function ( json ) {
          return json.data;
        }
      },
      language: otpGetLanguage(),
      lengthChange: false,
      buttons: otpGetExport(),
      initComplete : function (settings, json) {
        // console.log("Complete");
        table_overview.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
  //        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
  /*
        let chart = $('#graph').highcharts();
        chart.title.update({ text: 'Allocations '+otpGetValue('funding') });
        chart.yAxis[0].setTitle({ text: 'Allocations ['+otpUnit+']' });
        chart.redraw();
  */
      },
      columnDefs: [
        {
          targets: [6,7,8,9,10,11,12,13,14,15,16,17],
          render: function ( data, _type, _row, _meta ) {
            let d = (data*otpMonthFactor).toFixed(otpDetailPrecision);
            return d !== 0 ? d : "";
          },
          className: "text-right",
          type: "num"
        },
        {
          targets: [colAlloc],
          render: function ( data, _type, _row, _meta ) {
            return (data*otpFactor).toFixed(otpSumPrecision);
          },
          className: "text-right",
          type: "num"
        },
      ],
      // orderFixed: [[ colLastName, "asc" ]],
      order: [[ colLastName, "asc"], [ colActivity, "asc"], [ colSystem, "asc"], [ colTask, "asc"]],
      paging: false,
      info: false,
      scrollX: true,
      scrollY: 100,
      scrollCollapse: true,
      scrollResize: true,
      // deferRender: true,
      // scroller: true,
      columns: [
        { data: "FNAME",
          title: 'First Name',
          className: "title",
        },
        { data: "LNAME",
          title: 'Last Name',
          className: "title",
        },
        { data: "ACTIVITY",
          title: 'Activity',
          className: "title",
        },
        { data: "SYSTEM",
          title: 'System',
          className: "title",
        },
        { data: "TASK",
          title: 'Task',
          className: "title",
        },
        { data: "TASK_ID",
          title: 'Task ID',
          className: "title",
        },

        { data: "JAN",
          title: "Jan",
          sortable: false,
        },
        { data: "FEB",
          title: "Feb",
          sortable: false,
        },
        { data: "MAR",
          title: "Mar",
          sortable: false,
        },
        { data: "APR",
          title: "Apr",
          sortable: false,
        },
        { data: "MAY",
          title: "May",
          sortable: false,
        },
        { data: "JUN",
          title: "Jun",
          sortable: false,
        },
        { data: "JUL",
          title: "Jul",
          sortable: false,
        },
        { data: "AUG",
          title: "Aug",
          sortable: false,
        },
        { data: "SEP",
          title: "Sep",
          sortable: false,
        },
        { data: "OCT",
          title: "Oct",
          sortable: false,
        },
        { data: "NOV",
          title: "Nov",
          sortable: false,
        },
        { data: "DEC",
          title: "Dec",
          sortable: false,
        },
        { data: "SUMA",
          title: "Alloc",
          sortable: false,
        },
      ],
  //     rowGroup: {
  //       endRender: function ( rows, group ) {
  //         // console.log('EndRender ' + group);
  //         // FIXME should move
  //         let doubleValue = function ( i ) {
  //             return typeof i === 'string' ?
  //                 i.replace(/[\$,]/g, '')*1 :
  //                 typeof i === 'number' ?
  //                     i : 0;
  //         };

  //         // FIXME should move
  //         let sum = function (a, b) {
  //           return doubleValue(a) + doubleValue(b);
  //         };

  //         let html = '';
  //         let colspan = 5;
  //         html += '<td colspan="' + colspan + '">' + group + '<span class="rowCount-grid"><b>' + ' (' + rows.count() + ')' + '</b></span>' + '</td>';
  //         html += '<td>Sum:</td>';
  //         let columns = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC','SUMA'];
  //         let points = [];
  //         for (let i = 0; i < columns.length; i++) {
  //           let total = rows.data().pluck(columns[i]).reduce(sum, 0);

  //           let factor = i < 12 ? otpMonthFactor : otpFactor;
  //           html += '<td class="text-right">' + (total*factor).toFixed(otpSumPrecision) + '</td>';

  //           if (i < 12) {
  //             points[i] = Number(total);
  //           }
  //         }
  // /*
  //         let chart = $('#graph').highcharts();
  //         let series = chart.addSeries({ name: group, id: group }, false);
  //         series.setData(points, false);
  //         series.update({ 'tooltip': { valueSuffix: ' '+otpUnit }}, false);
  // */
  //         return $('<tr/>').append(html);
  //       },
  //       dataSrc: 'INAME'
  //     },
      headerCallback: function ( row, data, start, end, display ) {
        // console.log('Header');
  /*
        let chart = $('#graph').highcharts();
        while(chart.series.length > 0) {
          chart.series[0].remove(false);
        }
  */
      },
      footerCallback: function ( row, data, start, end, display ) {
        // console.log('Footer');
        // Show buttons when we have Data, otherwise remove
        if (end - start > 0) {
          $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
        } else {
          $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
        }

        let api = this.api();

        otpStore("Search", api.search());

        let doubleValue = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        let sum = function (a, b) {
          return doubleValue(a) + doubleValue(b);
        };

        // create sums of all numeric month columns
        let points = [];
        let columns = [ 6,7,8,9,10,11,12,13,14,15,16,17 ];
        for (let i=0; i<columns.length; i++) {
          // Total over all pages
          let total = api
              .column( columns[i], { search:'applied' } )
              .data()
              .reduce( sum, 0 );

          // Update footer
          total = (total*otpMonthFactor).toFixed(otpSumPrecision);
          points[i] = Number(total);
          $( api.column( columns[i] ).footer() ).html(total);
        }

        let suma = api
            .column( colAlloc, { search:'applied' } )
            .data()
            .reduce( function (a, b) {
                return doubleValue(a) + doubleValue(b);
            }, 0 );
        $( api.column( colAlloc ).footer() ).html((suma*otpFactor).toFixed(otpSumPrecision));
      }
    } );

    // fix to make sure "Loading..." message re-apears when loading a new URL
    table_overview.on('preXhr.dt', function(e, settings, data) {
      $(this).dataTable().api().clear();
      settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
      $(this).dataTable().api().draw();
    });

    let DataTable = $.fn.dataTable;
    DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
      let alignmentForCols = $.map( headers, function ( header ) {
          for(let i = 0; i < dt.settings()[0].aoColumns.length; i++){
              let column = dt.settings()[0].aoColumns[i];
              if(column.title == header) {
                  if(column.type == 'num' || column.type == 'num-fmt')
                      return 'right';
                  return 'left';
              }
          }
      });
      return  alignmentForCols;
    });
  }
  otpTable.push(table_overview);

  {
    let colYear = 0;
    let colOtClass12 = 1;
    let colOtClass3 = 2;
    let colReqClass1 = 3;
    let colReqClass2 = 4;
    let colReqClass3 = 5;
    let colAllocClass1 = 6;
    let colAllocClass2 = 7;
    let colAllocClass3 = 8;
    let colAllocClass4 = 9;
    let colAllocUpgradeConstruction = 10;

    table_overview_year = $('#table_overview_year').DataTable( {
      ajax: {
        url: otpGetUrl(table_overview_year, 'FundingAgencyOverviewYearData.php'),
        type: "POST",
        dataSrc: function ( json ) {
          return json.data;
        }
      },
      language: otpGetLanguage(),
      lengthChange: false,
      buttons: otpGetExport(),
      initComplete : function (settings, json) {
        // console.log("Complete");
        table_overview_year.buttons().container().appendTo( $('#table_wrapper .col-md-6:eq(0)'));
  //        $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
  /*
        let chart = $('#graph').highcharts();
        chart.title.update({ text: 'Allocations '+otpGetValue('funding') });
        chart.yAxis[0].setTitle({ text: 'Allocations ['+otpUnit+']' });
        chart.redraw();
  */
      },
      columnDefs: [
        {
          targets: [colOtClass12, colOtClass3],
          render: function ( data, _type, _row, _meta ) {
            let d = (data*1).toFixed(otpDetailPrecision);
            return d !== 0 ? d : "";
          },
          className: "text-right",
          type: "num"
        },
        {
          targets: [colAllocClass1, colAllocClass2, colReqClass1, colReqClass2],
          render: function ( data, _type, _row, _meta ) {
            let d = (data*365).toFixed(otpDetailPrecision);
            return d !== 0 ? d : "";
          },
          className: "text-right",
          type: "num"
        },
        {
          targets: [colAllocClass3, colAllocClass4, colAllocUpgradeConstruction, colReqClass3],
          render: function ( data, _type, _row, _meta ) {
            let d = (data*1).toFixed(otpDetailPrecision);
            return d !== 0 ? d : "";
          },
          className: "text-right",
          type: "num"
        },
      ],
      orderFixed: [[ colYear, "desc" ]],
      order: [[ colYear, "desc"]],
      paging: false,
      info: false,
      scrollX: true,
      scrollY: 100,
      scrollCollapse: true,
      scrollResize: true,
      // deferRender: true,
      // scroller: true,
      columns: [
        { data: "YEAR" },

        { data: "OT1" },
        { data: "OT3" },

        { data: "C1R" },
        { data: "C2R" },
        { data: "C3R" },

        { data: "C1A" },
        { data: "C2A" },
        { data: "C3A" },
        { data: "C4A" },
        { data: "UCA" },
      ],
      footerCallback: function ( row, data, start, end, display ) {
        // console.log('Footer');
        // Show buttons when we have Data, otherwise remove
        if (end - start > 0) {
          $('#table_wrapper .col-md-6:eq(0) .dt-buttons').show();
        } else {
          $('#table_wrapper .col-md-6:eq(0) .dt-buttons').hide();
        }

        let api = this.api();

        otpStore("Search", api.search());

        let doubleValue = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        let sum = function (a, b) {
          return doubleValue(a) + doubleValue(b);
        };

        // create sums of all numeric month columns
        let ot_columns = [colOtClass12, colOtClass3];
        for (let i=0; i<ot_columns.length; i++) {
          // Total over all pages
          let total = api
              .column( ot_columns[i], { search:'applied' } )
              .data()
              .reduce( sum, 0 );
          let count = api
              .column( ot_columns[i], { search:'applied' } )
              .data()
              .count();

          // Update footer
          let avg = (total / count).toFixed(otpSumPrecision);
          $( api.column( ot_columns[i] ).footer() ).html(avg);
        }

        let shift_columns = [colAllocClass1, colAllocClass2, colReqClass1, colReqClass2];
        for (let i=0; i<shift_columns.length; i++) {
          // Total over all pages
          let total = api
              .column( shift_columns[i], { search:'applied' } )
              .data()
              .reduce( sum, 0 );
          let count = api
              .column( shift_columns[i], { search:'applied' } )
              .data()
              .count();

          // Update footer
          let avg = ((total*365) / count).toFixed(otpSumPrecision);
          $( api.column( shift_columns[i] ).footer() ).html(avg);
        }

        let fte_columns = [colAllocClass3, colAllocClass4, colAllocUpgradeConstruction, colReqClass3];
        for (let i=0; i<fte_columns.length; i++) {
          // Total over all pages
          let total = api
              .column( fte_columns[i], { search:'applied' } )
              .data()
              .reduce( sum, 0 );
          let count = api
              .column( fte_columns[i], { search:'applied' } )
              .data()
              .count();

          // Update footer
          let avg = ((total*1) / count).toFixed(otpSumPrecision);
          $( api.column( fte_columns[i] ).footer() ).html(avg);
        }
        $( api.column( colYear ).footer() ).html("Average");
      }
    } );

    // fix to make sure "Loading..." message re-apears when loading a new URL
    table_overview_year.on('preXhr.dt', function(e, settings, data) {
      $(this).dataTable().api().clear();
      settings.iDraw = 0;   //set to 0, which means "initial draw" which with a clear table will show "loading..." again.
      $(this).dataTable().api().draw();
    });

    let DataTable = $.fn.dataTable;
    DataTable.Api.register( 'buttons.getAlignmentForColumns()', function ( dt, headers ) {
      let alignmentForCols = $.map( headers, function ( header ) {
          for(let i = 0; i < dt.settings()[0].aoColumns.length; i++){
              let column = dt.settings()[0].aoColumns[i];
              if(column.title == header) {
                  if(column.type == 'num' || column.type == 'num-fmt')
                      return 'right';
                  return 'left';
              }
          }
      });
      return  alignmentForCols;
    });
  }
  otpTable.push(table_overview_year);

} );
